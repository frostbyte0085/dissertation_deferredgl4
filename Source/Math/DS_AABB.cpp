#include "DS_AABB.h"
#include "DS_Math.h"

using namespace DS;

AABB::AABB(const vec3& bbMin, const vec3& bbMax)
{
	SetAABB(bbMin, bbMax);
}

AABB::AABB(const vector<vec3> &vertices)
{
	m_Min = vertices[0];
	m_Max = vertices[0];

	for (int i=0; i<(int)vertices.size(); i++)
	{
		m_Min = glm::min(m_Min, vertices[i]);
		m_Max = glm::max(m_Max, vertices[i]);
	}
}

AABB::AABB(const AABB& other)
{
	SetAABB(other.GetMin(), other.GetMax());
}

AABB::~AABB()
{
}

void AABB::SetMin(const vec3& bbMin)
{
	m_Min = bbMin;
}

void AABB::SetMax(const vec3& bbMax)
{
	m_Max = bbMax;
}

void AABB::SetAABB(const vec3& bbMin, const vec3& bbMax)
{
	m_Min = bbMin;
	m_Max = bbMax;
}

void AABB::Transform (const mat4 & world)
{
	vec3 translation = Math::ExtractTranslation(world);
	m_Min += translation;
	m_Max += translation;
}
