#pragma once

#include <DS_Include.h>
#include "DS_Plane.h"

// missing from the windows implementation of cmath!
#ifndef roundf
#define roundf(x) (int) (x + 0.5f)
#endif

#   define EPS 0.00001f
#   define PI 3.14159265358979323846f
#   define PI_OVER_180	 0.017453292519943295769236907684886f
#   define PI_OVER_360	 0.0087266462599716478846184538424431f
#   define DEG2RAD(x) ((x) * ((PI) / (180.0f)))
#   define RAD2DEG(x) ((x) * ((180.0f) / (PI)))

namespace DS
{
	class AABB;
	class Sphere;

	class Math
	{
	public:
		static int Random(int from, int to);
		static float RandomF(float from, float to);
		static double RandomD(double low, double high);

		static float Clamp(float value, float minimum, float maximum);
		static int Clamp(int value, int minimum, int maximum);
    
		static mat4 ComposeMatrix(const vec3& t, const vec3& s, const quat& r);
		static void DecomposeMatrix (const mat4 &input, vec3& t, quat& r, vec3& s);
		static vector<Plane> CalculateViewFrustum (const mat4 &view, const mat4 &projection);
		static vector<vec3> CalculateFrustumCorners(const mat4& view, const mat4& projection);
    
		static vec3 ExtractTranslation (const mat4 &input);
		static vec3 ExtractScaling (const mat4 &input);
		static quat ExtractRotation (const mat4 &input);

		static bool FloatEquals(float f1, float f2);
		static bool IsZero(float x);

		static vec3 TransformVectorByQuaternion (const quat &q, const vec3 &v);
		static vec3 TransformVector3ByMatrix (const vec3 &v, const mat4 &m);
		static vec4 TransformVector4ByMatrix (const vec4 &v, const mat4 &m);

		static bool AABBInFrustum(const AABB& aabb, const vector<Plane> &frustum);
		static bool SphereInFrustum(const Sphere& sphere, const vector<Plane> &frustum);

		static bool Intersects (const AABB &aabb, const Sphere &sphere);
	};
}