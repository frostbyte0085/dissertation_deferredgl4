#pragma once

#include <DS_Include.h>

namespace DS
{
	class Sphere
	{
	public:
		Sphere();
		Sphere(const vector<vec3> &points);
		Sphere(const vec3& center, float radius);
		Sphere(const Sphere& other);
		~Sphere();

		void SetRadius(float radius) { m_Radius = radius; }
		float GetRadius() const { return m_Radius;}

		void SetCenter(const vec3 &center) { m_Center = center; }
		const vec3& GetCenter() const { return m_Center; }

		void Transform(const mat4& world);

	private:
		vec3 m_Center;
		float m_Radius;
	};
}
