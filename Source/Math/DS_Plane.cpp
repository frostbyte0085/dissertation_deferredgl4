#include "DS_Plane.h"

using namespace DS;

Plane::Plane() {
    position = vec3(0,0,0);
    normal = vec3(0,1,0);
}

Plane::Plane(float a, float b, float c, float d) {

    normal = vec3(a,b,c);
    normalLength = length (normal);
    normal = normalize(normal);
    
    a /= normalLength;
    b /= normalLength;
    c /= normalLength;
    d /= normalLength;
    
    position = d * normalLength * normal;
    
    equation = PlaneEquation (a,b,c,d);
}

Plane::Plane(const vec4 &p) {
    *this = Plane (p.x, p.y, p.z, p.w);
}

Plane::Plane (const PlaneEquation &equation) {
    *this = Plane(equation.a, equation.b, equation.c, equation.d);
}

void Plane::recalcEquation() {
    normal = glm::normalize (normal);
    
    equation.a = normal.x * normalLength;
    equation.b = normal.y * normalLength;
    equation.c = normal.z * normalLength;
    
    equation.d = glm::dot (normal, position) / normalLength;
}

Plane::Plane (const vec3 &normal, const vec3 &p) {
    this->normalLength = length(normal);
    this->normal = normalize( normal );
    this->position = p;
    
    recalcEquation();
}

Plane::~Plane() {
    
}

eClassifyPoint Plane::ClassifyPoint(const vec3 &point) {
    vec3 direction = position - point;
    float result = dot(normal, direction);
    
    if (result < -0.01f)
        return CP_FRONT;
    
    if (result > 0.01f)
        return CP_BACK;
    
    return CP_ONPLANE;
}