#include "DS_Math.h"
#include "DS_Plane.h"
#include "DS_AABB.h"
#include "DS_Sphere.h"

using namespace DS;

vec3 Math::TransformVectorByQuaternion(const quat &q, const vec3 &v)
{

    float x2 = q.x + q.x; 
    float y2 = q.y + q.y; 
    float z2 = q.z + q.z;    
    float xx2 = q.x * x2; 
    float yy2 = q.y * y2; 
    float zz2 = q.z * z2; 
    float xy2 = q.x * y2; 
    float yz2 = q.y * z2; 
    float zx2 = q.z * x2; 
    float xw2 = q.w * x2; 
    float yw2 = q.w * y2; 
    float zw2 = q.w * z2; 
    
    return vec3( 
                    (1.f - yy2 - zz2) * v[0] + (xy2 - zw2) * v[1] + (zx2 + yw2) * v[2], 
                    (xy2 + zw2) * v[0] + (1.f - zz2 - xx2) * v[1] + (yz2 - xw2) * v[2], 
                    (zx2 - yw2) * v[0] + (yz2 + xw2) * v[1] + (1.f - xx2 - yy2) * v[2] 
                    ); 
}

int Math::Random(int from, int to)
{
	return rand() % (to - from +1) + from;
}

float Math::RandomF(float low, float high)
{
	float temp;

	/* swap low & high around if the user makes no sense */
	if (low > high)
	{
		temp = low;
		low = high;
		high = temp;
	}

	/* calculate the random number & return it */
	temp = ((float)rand() / (static_cast<float>(RAND_MAX) + 1.0f))
	* (high - low) + low;
	return temp;
}

double Math::RandomD(double low, double high)
{
	double temp;

	/* swap low & high around if the user makes no sense */
	if (low > high)
	{
		temp = low;
		low = high;
		high = temp;
	}

	/* calculate the random number & return it */
	temp = (rand() / (static_cast<double>(RAND_MAX) + 1.0))
	* (high - low) + low;
	return temp;
}

bool Math::FloatEquals(float f1, float f2)
{
	return fabs(f2 - f1) < EPS;
}

bool Math::IsZero (float x)
{
    return fabs(x) < EPS;
}

float Math::Clamp(float value, float minimum, float maximum)
{
    if (value <= minimum)
        value = minimum;
    else if (value >= maximum)
        value = maximum;
    
    return value;
}

int Math::Clamp(int value, int minimum, int maximum)
{
    if (value <= minimum)
        value = minimum;
    else if (value >= maximum)
        value = maximum;
    
    return value;    
}

vec3 Math::TransformVector3ByMatrix (const vec3 &v, const mat4 &m)
{
	vec3 ret;
        
	vec4 v4 = vec4(v.x, v.y, v.z, 1.0f);
	vec4 vt4 = m * v4;
	ret = vec3(vt4.x, vt4.y, vt4.z);
        
	return ret;
}
    
vec4 Math::TransformVector4ByMatrix (const vec4 &v, const mat4 &m)
{
	return m * v;
}

mat4 Math::ComposeMatrix(const vec3& t, const vec3& s, const quat& r)
{
	mat4 ret = mat4(1.0f);

	// rotation matrix
	mat4 rotationMatrix = mat4_cast(r);
    
    // translation matrix
    mat4 translationMatrix = translate (t);
    
    // scale matrix
    mat4 scaleMatrix = scale(s);
    
    ret = translationMatrix * rotationMatrix * scaleMatrix;

	return ret;
}

void Math::DecomposeMatrix(const mat4 &input, vec3& t, quat& r, vec3& s)
{
    r = ExtractRotation(input);
    s = ExtractScaling(input);
    t = ExtractTranslation(input);
}

vec3 Math::ExtractTranslation (const mat4& input)
{
    float *m = value_ptr(const_cast<mat4&>(input));
    return vec3 (m[12], m[13], m[14]);
}

vec3 Math::ExtractScaling (const mat4 &input)
{
    float *m = value_ptr(const_cast<mat4&>(input));
    vec3 v1 = vec3 (m[0], m[1], m[2]);
    vec3 v2 = vec3 (m[4], m[5], m[6]);
    vec3 v3 = vec3 (m[8], m[9], m[10]);
    
    return vec3(glm::length(v1), glm::length(v2), glm::length(v3));
}

quat Math::ExtractRotation (const mat4 &input)
{
    float *m = glm::value_ptr(const_cast<mat4&>(input));
    
    vec3 v1 = vec3 (m[0], m[1], m[2]);
    vec3 v2 = vec3 (m[4], m[5], m[6]);
    vec3 v3 = vec3 (m[8], m[9], m[10]);
    
    v1 = glm::normalize(v1);
    v2 = glm::normalize(v2);
    v3 = glm::normalize(v3);
    
    mat3x3 m1 (v1[0], v1[1], v1[2],
                v2[0], v2[1], v2[2],
                v3[0], v3[1], v3[2]);
    
    m1 = glm::transpose(m1);
    
    return quat_cast(m1);
}

vector<vec3> Math::CalculateFrustumCorners(const mat4& view, const mat4& projection)
{
	vector<vec3> corners;
	corners.resize(8);

	mat4 mvp = projection * view;
	mat4 invMvp = inverse(mvp);

	vec4 corner = invMvp * vec4(1, 1, -1, 1);
	corners[0] = vec3(corner.x, corner.y, corner.z) / corner.w;

	corner = invMvp * vec4(-1, 1, -1, 1);
	corners[1] = vec3(corner.x, corner.y, corner.z) / corner.w;

	corner = invMvp * vec4(1, -1, -1, 1);
	corners[2] = vec3(corner.x, corner.y, corner.z) / corner.w;

	corner = invMvp * vec4(-1, -1, -1, 1);
	corners[3] = vec3(corner.x, corner.y, corner.z) / corner.w;


	corner = invMvp * vec4(1, 1, 1, 1);
	corners[4] = vec3(corner.x, corner.y, corner.z) / corner.w;

	corner = invMvp * vec4(-1, 1, 1, 1);
	corners[5] = vec3(corner.x, corner.y, corner.z) / corner.w;

	corner = invMvp * vec4(1, -1, 1, 1);
	corners[6] = vec3(corner.x, corner.y, corner.z) / corner.w;

	corner = invMvp * vec4(-1, -1, 1, 1);
	corners[7] = vec3(corner.x, corner.y, corner.z) / corner.w;

	return corners;
}

// http://www.crownandcutlass.com/features/technicaldetails/frustum.html
vector<Plane> Math::CalculateViewFrustum (const mat4 &view, const mat4 &projection)
{
    vector<Plane> frustum;
    
    const float   *proj = glm::value_ptr(projection);
    const float   *modl = glm::value_ptr(view);
    float   clip[16];
        
    //Combine the two matrices (multiply projection by modelview)
    clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
    clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
    clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
    clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];
    
    clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
    clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
    clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
    clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];
    
    clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
    clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
    clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
    clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];
    
    clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
    clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
    clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
    clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];
    
    //Extract the numbers for the RIGHT plane
    Plane right (clip[ 3] - clip[ 0], clip[ 7] - clip[ 4], clip[11] - clip[ 8], clip[15] - clip[12]);
    frustum.push_back(right);
    
    //Extract the numbers for the LEFT plane
    Plane left (clip[ 3] + clip[ 0], clip[ 7] + clip[ 4], clip[11] + clip[ 8], clip[15] + clip[12]);
    frustum.push_back(left);
    
    //Extract the numbers for the BOTTOM plane
    Plane bottom (clip[ 3] + clip[ 1], clip[ 7] + clip[ 5], clip[11] + clip[ 9], clip[15] + clip[13]);
    frustum.push_back(bottom);
    
    //Extract the numbers for the TOP plane
    Plane top (clip[ 3] - clip[ 1], clip[ 7] - clip[ 5], clip[11] - clip[ 9], clip[15] - clip[13]);
    frustum.push_back(top);
    
    //Extract the numbers for the FAR plan
    Plane farp (clip[ 3] - clip[ 2], clip[ 7] - clip[ 6], clip[11] - clip[10], clip[15] - clip[14]);
    frustum.push_back(farp);
    
    //Extract the numbers for the NEAR plane
    Plane nearp (clip[ 3] + clip[ 2], clip[ 7] + clip[ 6], clip[11] + clip[10], clip[15] + clip[14]);
    frustum.push_back(nearp);
    return frustum;
}

bool Math::AABBInFrustum(const AABB& aabb, const vector<Plane>& frustum)
{
	for (int i=0; i<(int)frustum.size(); i++)
	{
		PlaneEquation e = frustum[i].GetEquation();

		float d = std::max(aabb.GetMin().x * e.a, aabb.GetMax().x * e.a)
			+ std::max(aabb.GetMin().y * e.b, aabb.GetMax().y * e.b)
			+ std::max(aabb.GetMin().z * e.c, aabb.GetMax().z * e.c)
			+ e.d;

		if (d <= 0)
			return false;
	}
	return true;
}

bool Math::SphereInFrustum(const Sphere& sphere, const vector<Plane>& frustum)
{
	vec3 center = sphere.GetCenter();
	float radius = sphere.GetRadius();

	for(int i=0; i<(int)frustum.size(); i++) {
        const Plane &p = frustum[i];
		
		PlaneEquation eq = p.GetEquation();
		if (eq.a * center.x + eq.b * center.y + eq.c * center.z + eq.d <= -radius)
            return false;
    }
    return true;
}

bool Math::Intersects(const AABB &aabb, const Sphere &sphere)
{
	vec3 closestToAABB = glm::min (glm::max(sphere.GetCenter(), aabb.GetMin()), aabb.GetMax());
	float distanceSquared = length2(closestToAABB - sphere.GetCenter());

	return distanceSquared < (sphere.GetRadius() * sphere.GetRadius());
}