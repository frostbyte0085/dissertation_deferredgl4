#include "DS_Sphere.h"
#include "DS_Math.h"

using namespace DS;

Sphere::Sphere()
{
	m_Center = vec3(0);
	m_Radius = 0.0f;
}

Sphere::Sphere(const vec3& center, float radius)
{
	m_Center = center;
	m_Radius = radius;
}

Sphere::Sphere(const Sphere& other)
{
	m_Center = other.m_Center;
	m_Radius = other.m_Radius;
}

Sphere::Sphere(const vector<vec3>& points)
{
    vec3 center(0,0,0);
    
    for (int i=0; i<(int)points.size(); i++)
	{
        center += points[i];
    }
    center /= points.size();
    
    float radius=0.0f;
    for (int i=0; i<(int)points.size(); i++)
	{
        vec3 v = points[i] - center;
        float len2 = glm::length2(v);
        if (len2 > radius)
            radius = len2;
    }
    radius = sqrtf(radius);
    
    SetRadius(radius);
    SetCenter(center);
}

Sphere::~Sphere()
{
}

void Sphere::Transform(const mat4& world)
{
	m_Center += Math::ExtractTranslation(world);
	vec3 scaling = Math::ExtractScaling(world);

	float maxScaling = std::max(scaling.x, std::max(scaling.y, scaling.z));
	m_Radius *= maxScaling;
}