#pragma once

#include <DS_Include.h>

namespace DS
{
	enum eFrustumPlane
	{
		FP_RIGHT,
		FP_LEFT,
		FP_BOTTOM,
		FP_TOP,
		FP_FAR,
		FP_NEAR
	};

	enum eClassifyPoint {
		CP_FRONT = 1,
		CP_BACK = 2,
		CP_ONPLANE = 3,
		CP_SPLIT = 4
	};

	class PlaneEquation {
	public:
    
		PlaneEquation() {a=b=c=d=0;}
		PlaneEquation(float a, float b, float c, float d) {
			this->a = a;
			this->b = b;
			this->c = c;
			this->d = d;
		}
    
		float a,b,c,d;
	};

	class Plane {
	public:
		Plane ();
		Plane (float a, float b, float c, float d);
		Plane (const PlaneEquation& equation);
		Plane (const vec4 &p);
		Plane (const vec3 &normal, const vec3 &p);
		~Plane();
    
		void SetPosition (const vec3 &p) { this->position = position; recalcEquation();}
		const vec3& GetPosition() const { return position; }
    
		void SetNormal (const vec3 &normal) { 
			this->normal = normal;
			this->normalLength = glm::length(normal);
			this->normal = glm::normalize(this->normal);
        
			recalcEquation();
		}
    
		const vec3& GetNormal() const { return normal; }
    
		PlaneEquation GetEquation() const { return equation; }
    
		eClassifyPoint ClassifyPoint (const vec3 &point);
    
	private:
		void recalcEquation();
    
		vec3 position;
		vec3 normal;
		float normalLength;
    
		PlaneEquation equation;
	};

}