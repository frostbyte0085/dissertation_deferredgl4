#pragma once

#include <DS_Include.h>

namespace DS
{
	class AABB
	{
	public:
		AABB(const AABB& other);
		AABB(const vec3 &bbMin, const vec3 &bbMax);
		AABB(const vector<vec3> &vertices);
		~AABB();

		const vec3& GetMin() const { return m_Min; }
		const vec3& GetMax() const { return m_Max; }
		
		void SetMin(const vec3& bbMin);
		void SetMax(const vec3& bbMax);
		void SetAABB(const vec3& bbMin, const vec3& bbMax);

		const vec3& GetCenter() const { return m_Center; }

		void Transform(const mat4& world);
	private:
		

		vec3 m_Min;
		vec3 m_Max;
		vec3 m_Center;
		vec3 m_Extents;
	};
}