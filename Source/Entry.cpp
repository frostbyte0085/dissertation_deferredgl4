#include <Core/DS_Application.h>
#include <Tools/DS_Exception.h>

using namespace DS;

int main()
{
	try
	{
		Application app;
		app.Run();
	}
	catch(Exception &exception)
	{
		cout << kExceptionLog.c_str() << exception.GetDescription().c_str() << endl;
		return -1;
	}

	return 0;
}