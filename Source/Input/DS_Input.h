#pragma once

#include <DS_Include.h>

namespace DS
{
	// subtract this from char keys like 'a'
	const int kKeyOffset = 32;

	class Input
	{
	public:
		Input();
		~Input();

		void Initialise();
		void Update();

		bool IsKeyDown(int key);
		bool IsKeyUp(int key);
		bool IsKeyClicked(int key);

		bool IsButtonDown(int button);
		bool IsButtonUp(int button);
		bool IsButtonClicked(int button);	

		void GetMousePosition(int &x, int &y);
	private:
		
		bool m_KeyStates[GLFW_KEY_LAST];
		bool m_OldKeyStates[GLFW_KEY_LAST];

		bool m_ButtonStates[GLFW_MOUSE_BUTTON_LAST];
		bool m_OldButtonStates[GLFW_MOUSE_BUTTON_LAST];
	};
	
	extern Input *gInput;
}