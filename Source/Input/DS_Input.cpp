#include <Input/DS_Input.h>

namespace DS
{
	Input *gInput = NULL;
}

using namespace DS;


Input::Input()
{

}

Input::~Input()
{
}

void Input::Initialise()
{
	memset(m_KeyStates, 0, sizeof(m_KeyStates));
	memset(m_KeyStates, 0, sizeof(m_OldKeyStates));

	memset(m_ButtonStates, 0, sizeof(m_ButtonStates));
	memset(m_ButtonStates, 0, sizeof(m_OldButtonStates));
}

void Input::Update()
{
	// store the old keys
	memcpy (m_OldKeyStates, m_KeyStates, sizeof(m_KeyStates));
	
	for (int i=0; i<GLFW_KEY_LAST; i++)
	{
		m_KeyStates[i] = (glfwGetKey(i) == GLFW_PRESS);
	}

	// store old buttons
	memcpy(m_OldButtonStates, m_ButtonStates, sizeof(m_ButtonStates));
	for (int i=0; i<GLFW_MOUSE_BUTTON_LAST; i++)
	{
		m_ButtonStates[i] = (glfwGetMouseButton(i) == GLFW_PRESS);
	}
}

bool Input::IsKeyClicked(int key)
{
	return m_KeyStates[key] && !m_OldKeyStates[key];
}

bool Input::IsKeyDown(int key)
{
	return m_KeyStates[key];
}

bool Input::IsKeyUp(int key)
{
	return !m_KeyStates[key];
}

bool Input::IsButtonClicked(int button)
{
	return m_ButtonStates[button] && !m_OldButtonStates[button];
}

bool Input::IsButtonDown(int button)
{
	return m_ButtonStates[button];
}

bool Input::IsButtonUp(int button)
{
	return !m_ButtonStates[button];
}

void Input::GetMousePosition(int &x, int &y)
{
	glfwGetMousePos(&x, &y);
}