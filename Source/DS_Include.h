#pragma once

// standard c++ headers
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <direct.h>
#include <algorithm>
#include <windows.h>
#include <utility>
#include <sstream>

// our containers
#include <vector>
#include <string>
#include <list>
#include <queue>
#include <stack>
#include <unordered_map>
#include <map>

// third party libraries
// ---------------------
// glew
#include <glew/glew.h>

// GLFW
#include <GLFW/glfw.h>

// GLM
#include <glm/glm.hpp>
#include <glm/ext.hpp>

// DevIL
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

using namespace glm;
using namespace std;

extern "C"
{

	inline const GLubyte *BUFFER_OFFSET(size_t bytes)
	{
		return reinterpret_cast<const GLubyte *>(0) + bytes;
	}

}