#include "DS_Object.h"
#include <Math/DS_Math.h>
#include <Graphics/DS_Mesh.h>
#include <Graphics/DS_Camera.h>
#include <Graphics/DS_Light.h>

using namespace DS;

Object::Object(const string& name) : m_Translation(0,0,0), m_Scale(1,1,1), m_Orientation(1,0,0,0), m_WorldMatrix(1.0f),
		m_Mesh(NULL), m_Camera(NULL), m_Up(0,1,0), m_Right(1,0,0), m_Forward(0,0,-1), m_Light(NULL)
{
	m_Name = name;
}

Object::~Object()
{
	if (m_Camera)
		delete m_Camera;

	if (m_Light)
		delete m_Light;
}

void Object::SetTranslation(const vec3& t)
{
	m_Translation = t;
}

void Object::Translate(const vec3& v)
{
	m_Translation += v;
}

void Object::SetScale(const vec3& s)
{
	m_Scale = s;
}

void Object::SetOrientation(const quat& r)
{
	m_Orientation = r;
	// re-calculate the up, right and forward vectors
	m_Up = vec3(0,1,0);
	m_Up = Math::TransformVectorByQuaternion (m_Orientation, m_Up);
	m_Up = normalize(m_Up);

	m_Right = vec3(1,0,0);
	m_Right = Math::TransformVectorByQuaternion (m_Orientation, m_Right);
	m_Right = normalize(m_Right);

	m_Forward = vec3(0,0,-1);
	m_Forward = Math::TransformVectorByQuaternion (m_Orientation, m_Forward);
	m_Forward = normalize(m_Forward);
}

void Object::Rotate (float yaw, float pitch, float roll)
{
	
}

void Object::RotateX (float r)
{
	quat rotation = quat(vec3(r,0,0));
	SetOrientation (GetOrientation() * rotation);
}

void Object::RotateY (float r)
{
	quat rotation = quat(vec3(0,r,0));
	SetOrientation (GetOrientation() * rotation);
}

void Object::Update()
{
	m_WorldMatrix = Math::ComposeMatrix(m_Translation, m_Scale, m_Orientation);
}

void Object::SetMesh(const Mesh* mesh)
{
	m_Mesh = const_cast<Mesh*>(mesh);
	if (m_Mesh != NULL)
	{
		
	}
}

void Object::SetCamera( const Camera* camera)
{
	m_Camera = const_cast<Camera*>(camera);
	if (m_Camera != NULL)
	{
		
	}
}

void Object::SetLight( const Light* light)
{
	m_Light = const_cast<Light*>(light);
	if (m_Light != NULL)
	{
		
	}
}