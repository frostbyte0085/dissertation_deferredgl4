#include "DS_Scene.h"
#include "DS_Object.h"
#include <Graphics/DS_Camera.h>
#include <Tools/DS_StrOp.h>
#include <Tools/DS_Exception.h>
#include <Math/DS_Math.h>

namespace DS
{
	Scene* gScene = NULL;
}

using namespace DS;

Scene::Scene() : m_Camera(NULL)
{

}

Scene::~Scene()
{
	ObjectMap::iterator itObjects;
	for (itObjects = m_Objects.begin(); itObjects != m_Objects.end(); itObjects++)
	{
		if (itObjects->second != NULL)
			delete itObjects->second;
	}
	m_Objects.clear();
}

void Scene::Initialise()
{
	m_Camera = NULL;
}

void Scene::Update()
{
	if (m_Camera)
	{
		Camera* cam = m_Camera->GetCamera();

		cam->Update();
		mat4 projection = cam->GetProjectionMatrix();
		mat4 view = inverse(m_Camera->GetWorldMatrix());

		m_FrustumPlanes = Math::CalculateViewFrustum(view, projection);
		m_FrustumCorners = Math::CalculateFrustumCorners(view, projection);
	}

	updateTransforms();
}

void Scene::updateTransforms()
{
	ObjectMap::const_iterator it;
	for (it = m_Objects.begin(); it != m_Objects.end(); it++)
	{
		Object *obj = it->second;
		if (obj == NULL)
			continue;

		obj->Update();
		// also, if this is a light, let's update the light data as well
		Light *light = obj->GetLight();
		if (light)
			light->Update(obj);
	}
}

void Scene::SetCamera(Object* camera)
{
	m_Camera = camera;
}

Object* Scene::AddObject(const string& name)
{
	string loweredName = StrOp::ToLower(name);
	if (GetObject(loweredName))
		throw Exception ("Object with name " + name + " already exists!");

	Object *obj = new Object(loweredName);

	m_Objects[loweredName] = obj;

	return obj;
}

Object* Scene::GetObject(const string& name) const
{
	string loweredName = StrOp::ToLower(name);

	Object *ret = NULL;

	ObjectMap::const_iterator it = m_Objects.find(loweredName);
	if (it != m_Objects.end())
		ret = it->second;

	return ret;
}

Object* Scene::GetObjectWithMesh(const string& name) const
{
	Object *ret = GetObject(name);
	if (ret && ret->GetMesh())
		return ret;
	return NULL;
}

vector<Object*> Scene::GetObjectsWithMesh() const
{
	vector<Object*> objects;

	ObjectMap::const_iterator it;
	for (it = m_Objects.begin(); it != m_Objects.end(); it++)
	{
		Object *obj = it->second;
		if (!obj || !obj->GetMesh())
			continue;

		objects.push_back(obj);
	}

	return objects;
}

Object* Scene::GetObjectWithLight(const string& name) const
{
	Object *ret = GetObject(name);
	if (ret && ret->GetLight())
		return ret;
	return NULL;
}

vector<Object*> Scene::GetObjectsWithLight() const
{
	vector<Object*> objects;

	ObjectMap::const_iterator it;
	for (it = m_Objects.begin(); it != m_Objects.end(); it++)
	{
		Object *obj = it->second;
		if (!obj || !obj->GetLight())
			continue;

		objects.push_back(obj);
	}

	return objects;
}

vector<Object*> Scene::GetObjectsWithLight(eLightType type) const
{
	vector<Object*> objects;

	ObjectMap::const_iterator it;
	for (it = m_Objects.begin(); it != m_Objects.end(); it++)
	{
		Object *obj = it->second;
		if (!obj || !obj->GetLight())
			continue;

		if (obj->GetLight()->GetType() != type)
			continue;

		objects.push_back(obj);
	}

	return objects;
}

Object* Scene::GetObjectWithCamera(const string& name) const
{
	Object *ret = GetObject(name);
	if (ret && ret->GetCamera())
		return ret;
	return NULL;
}

vector<Object*> Scene::GetObjectsWithCamera() const
{
	vector<Object*> objects;

	ObjectMap::const_iterator it;
	for (it = m_Objects.begin(); it != m_Objects.end(); it++)
	{
		Object *obj = it->second;
		if (!obj || !obj->GetCamera())
			continue;

		objects.push_back(obj);
	}

	return objects;
}