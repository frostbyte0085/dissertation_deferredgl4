#pragma once

#include <DS_Include.h>

namespace DS
{
	class Scene;
	class Mesh;
	class Camera;
	class Light;

	class Object
	{
		friend class Scene;
	public:
		const string& GetName() const { return m_Name; }

		const vec3& GetUp() const { return m_Up; }
		const vec3& GetForward() const { return m_Forward; }
		const vec3& GetRight() const { return m_Right; }

		void SetTranslation(const vec3& t);
		void Translate(const vec3& v);
		const vec3& GetTranslation() const { return m_Translation; }

		void SetScale(const vec3& s);
		const vec3& GetScale() const { return m_Scale; }

		void SetOrientation(const quat& r);
		void Rotate(const quat& r);
		void Rotate(float yaw, float pitch, float roll);
		void Rotate(const vec3& axis, float angle);
		void RotateX(float r);
		void RotateY(float r);
		void RotateZ(float z);

		const quat& GetOrientation() const { return m_Orientation;}

		const mat4x4& GetWorldMatrix() const { return m_WorldMatrix; }

		void Update();

		// components
		void SetMesh(const Mesh* mesh);
		Mesh* GetMesh() const { return m_Mesh; }

		void SetCamera(const Camera* camera);
		Camera* GetCamera() const { return m_Camera; }

		void SetLight(const Light* light);
		Light* GetLight() const { return m_Light; }

	private:
		Object(const string& name);
		~Object();

		string m_Name;

		Mesh* m_Mesh;
		Camera* m_Camera;
		Light* m_Light;

		vec3 m_Translation;
		quat m_Orientation;
		vec3 m_Scale;
		
		vec3 m_Up;
		vec3 m_Forward;
		vec3 m_Right;

		mat4 m_WorldMatrix;
	};
}