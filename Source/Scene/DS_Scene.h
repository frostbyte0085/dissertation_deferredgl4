#pragma once

#include <DS_Include.h>
#include <Graphics/DS_Light.h>
#include <Math/DS_Plane.h>

namespace DS
{
	class Object;
	class Camera;

	class Scene
	{
		friend class Renderer;
	public:
		typedef map<string, Object*> ObjectMap;

		Scene();
		~Scene();

		void Initialise();

		// Object management
		void SetCamera(Object* camera);
		Object* GetCamera() const { return m_Camera; }

		Object* AddObject(const string& name);
		
		void Update();

		// querrying
		Object* GetObject(const string& name) const;
		// with meshes
		Object* GetObjectWithMesh(const string& name) const;
		vector<Object*> GetObjectsWithMesh() const;
		// with lights
		Object* GetObjectWithLight(const string& name) const;
		vector<Object*> GetObjectsWithLight() const;
		vector<Object*> GetObjectsWithLight(eLightType type) const;
		// with cameras
		Object* GetObjectWithCamera(const string& name) const;
		vector<Object*> GetObjectsWithCamera() const;

		const vector<Plane>& GetFrustumPlanes() const { return m_FrustumPlanes; }
		vector<vec3>& GetFrustumCorners() { return m_FrustumCorners; }

	private:
		ObjectMap m_Objects;

		vector<Plane> m_FrustumPlanes;
		vector<vec3> m_FrustumCorners;

		void updateTransforms();

		Object *m_Camera;
	};

	extern Scene *gScene;
}