#pragma once

#include <DS_Include.h>

namespace DS
{
	class Mesh;
	class Shader;
	class Texture;
	class TextureFont;

	class Resources
	{
	public:
		Resources();
		~Resources();

		void Initialise();
		
		// MESH MANAGEMENT
		//
		Mesh* LoadMesh(const string& filename);
		Mesh* GetMesh(const string& filename);
		void  RemoveMesh(const string& filename);
		//
		
		// SHADER MANAGEMENT
		Shader* LoadShader(const string& filename);
		Shader* GetShader(const string& filename);
		void	RemoveShader(const string& filename);
		void	ReloadShaders();
		//

		// TEXTURE MANAGEMENT
		Texture* LoadTexture(const string& filename, bool mipmaps=true, bool filter=true, bool clamp=false, GLenum format=0, GLenum internalFormat=0, int width=-1, int height=-1);
		Texture* GetTexture(const string& filename);
		void	RemoveTexture(const string& filename);
		//

		// TEXTURE FONT MANAGEMENT
		TextureFont* LoadFont(const string& filename, float fontSize);
		TextureFont* GetFont(const string& filename);
		void		 RemoveFont(const string& filename);
		//

	private:
		
		// meshes
		typedef unordered_map<string, Mesh*> MeshMap;
		MeshMap m_Meshes;

		// shaders
		typedef unordered_map<string, Shader*> ShaderMap;
		ShaderMap m_Shaders;

		// textures
		typedef unordered_map<string, Texture*> TextureMap;
		TextureMap m_Textures;

		// texture fonts
		typedef unordered_map<string, TextureFont*> TextureFontMap;
		TextureFontMap m_TextureFonts;

	};

	extern Resources *gResources;
}