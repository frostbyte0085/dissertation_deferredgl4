#pragma once

#include <DS_Include.h>
#include <Graphics/DS_Renderer.h>

#include <Math/DS_Math.h>

namespace DS
{
	const int kGLMajor = 4;
	const int kGLMinor = 2;

	const string kSettingsFile = "./settings.ini";

	const int kNumRingLights = 24;
	const float kRingLightRadius = 30;
	const string kRingLightPrefix = "ringLight";
	const int kRingLightRotateRadius = 120;

	const int kNumWallSpotLights = 2;
	const string kWallSpotLightPrefix = "wallSpot";
	const vec3 kWallSpotLightPos[kNumWallSpotLights] = {vec3(-600,300,-100), vec3(500, 300, -100)};
	const vec3 kWallSpotLightRot[kNumWallSpotLights] = {vec3(0,PI/2,0), vec3(0,-PI/2,0)};
	const float kWallSpotLightFOV[kNumWallSpotLights] = {45.0f, 75.0f};
	const vec3 kWallSpotLightColor[kNumWallSpotLights] = {vec3(0, 0.4f, 1.0f), vec3(1.0f, 0.2f, 0.05f)};

	const int kNumCorridorLights = 2;
	const string kCorridorLightPrefix = "corridorLight";
	const float kCorridorLightRadius[kNumCorridorLights] = {350, 220};
	const int kCorridorLightMovement[kNumCorridorLights] = {1, -1};
	const vec3 kCorridorLightColor[kNumCorridorLights] = {vec3(1,0,0), vec3(1,1,0.2f)};
	const vec3 kCorridorLightPos[kNumCorridorLights] = {vec3(-1000,150,100), vec3(0,150,-170)};
	const string kCorridorLightCookie[kNumCorridorLights] = { "", "" };
	const bool kCorridorLightRotation[kNumCorridorLights] = { false, false};

	const int kNumCurtainLights = 20;
	const float kCurtainLightRotateRadius = 15;
	const string kCurtainLightPrefix = "curtainLight";
	const float kCurtainLightRadius[kNumCurtainLights] = {80, 80, 80, 80, 80, 
														  80, 80, 80, 80, 80, 
														  80, 80, 80, 80, 80, 
														  80, 80, 80, 80, 80 };

	const vec3 kCurtainLightColor[kNumCurtainLights] = {// front side curtains
														vec3(1,0,0), vec3(0,1,0), vec3(0,0,1), vec3(1,0,0), vec3(0,1,0),
														vec3(1,0,0), vec3(0,0,1), vec3(0,1,0), vec3(1,0,0), vec3(0,0,1),

														// back side curtains
														vec3(1,0,0), vec3(0,1,0), vec3(0,0,1), vec3(1,0,0), vec3(0,1,0),
														vec3(1,0,0), vec3(0,0,1), vec3(0,1,0), vec3(1,0,0), vec3(0,0,1) };

	const vec3 kCurtainLightPos[kNumCurtainLights] = {// front side curtains
													vec3(665,171,183), vec3(306,171,183), vec3(-71,171,183), vec3(-439,171,183), vec3(-813,171,183),
													vec3(-820,171,-247), vec3(-426,171,-247), vec3(-80,171,-247), vec3(309,171,-247), vec3(658,171,-247),
													// back side curtains
													vec3(665,171,217), vec3(306,171,217), vec3(-71,171,217), vec3(-439,171,217), vec3(-813,171,217),
													vec3(-820,171,-311), vec3(-426,171,-311), vec3(-80,171,-311), vec3(309,171,-311), vec3(658,171,-311)};

	const int kNumCookieCorridorLights = 4;
	const string kCookieCorridorLightPrefix = "cookieCorridorLight";
	const float kCookieCorridorLightRadius[kNumCookieCorridorLights] = {200, 220, 185, 250};
	const vec3 kCookieCorridorLightColor[kNumCookieCorridorLights] = {vec3(1,0.5f,0), vec3(1,0,0.3f), vec3(0,0.5f,1), vec3(0.2f,0.7f,0.2f)};
	const vec3 kCookieCorridorLightPos[kNumCookieCorridorLights] = {vec3(-1271, 101, -549), vec3(-568, 108, -547), vec3(57, 107, -547), vec3(650,106,-547)};
	const string kCookieCorridorLightCookie[kNumCookieCorridorLights] = {"Data/Textures/PointCookie/pointCookie.cube", 
																		"Data/Textures/PointCookie/pointCookie.cube",
																		"Data/Textures/PointCookie/pointCookie.cube",
																		"Data/Textures/PointCookie/pointCookie.cube"};


	// helper struct with static methods to keep track of time
	// time is in seconds
	struct FrameTime
	{
		static float elapsed;
		static float total;
	};

	class FSQ;

	class Application
	{
	public:
		Application();
		~Application();

		void Run();

	private:
		void initialise();
		void initialiseGLState();
		void initialiseSubsystems();
		void loadData();
		void createLights();

		void findWindowedMode(GLFWvidmode *displayMode);
		void enumerateFullscreenModes(vector<GLFWvidmode> *supportedModes);
		bool fullscreenModeSupported(int width, int height);
		vector<GLFWvidmode> m_SupportedFullscreenModes;
		GLFWvidmode m_SelectedDisplayMode;

		FSQ *m_Quad;

		void debugInputUpdate();
		void cameraUpdate();
		void overlayRender();
		bool m_ShowOverlay;

		unsigned int m_NumFrames;
		float m_ElapsedFPS;
		int m_FPS;

		void displayBufferView(eDeferredCompositionStep target);
		eDeferredCompositionStep m_DeferredStep;

		bool m_Running;

		int m_OldMouseX, m_OldMouseY;
		int m_MouseX, m_MouseY;
		float m_CameraRotX, m_CameraRotY;
	};
}