#include <Core/DS_Application.h>
#include <Core/DS_Resources.h>
#include <Input/DS_Input.h>
#include <Tools/DS_Exception.h>

#include <Graphics/DS_Shader.h>
#include <Graphics/DS_Mesh.h>
#include <Graphics/DS_Camera.h>
#include <Graphics/DS_FSQ.h>
#include <Graphics/DS_Light.h>
#include <Graphics/DS_Texture.h>
#include <Graphics/DS_RenderTarget.h>
#include <Graphics/DS_Renderer.h>
#include <Graphics/DS_PostProcess.h>
#include <Graphics/DS_TextureFont.h>

#include <Graphics/PostProcessEffects/DS_DeferredCompositionEffect.h>
#include <Graphics/PostProcessEffects/DS_SMAAEffect.h>

#include <Scene/DS_Scene.h>
#include <Scene/DS_Object.h>

using namespace DS;

// FrameTime related
float FrameTime::elapsed = 0.0f;
float FrameTime::total = 0.0f;

Shader *shader;
Texture *texture;
Texture *texture2;
Mesh *sponza;

Application::Application() : m_Running(false), m_MouseX(0), m_MouseY(0), m_OldMouseX(0), m_OldMouseY(0), m_CameraRotX(0), m_CameraRotY(0), m_ShowOverlay(true), m_ElapsedFPS(0), m_NumFrames(0)
{
	memset(&m_SelectedDisplayMode, 0, sizeof(m_SelectedDisplayMode));
	srand(time(0));
}

Application::~Application()
{
	if (m_Quad)
		delete m_Quad;
	m_Quad = NULL;

	if (gInput)
		delete gInput;
	gInput = NULL;

	if (gResources)
		delete gResources;
	gResources = NULL;

	if (gScene)
		delete gScene;
	gScene = NULL;

	if (gRenderer)
		delete gRenderer;
	gRenderer = NULL;

	glfwCloseWindow();
	glfwTerminate();
}

void Application::cameraUpdate()
{
	const float kMoveFactor = 220.0f;
	const float kRunSpeed = 3.0f;
	const float kMouseSensitivity = 0.15f;
	const float kMoveSpeed = FrameTime::elapsed * kMoveFactor;

	Object* camera = gScene->GetCamera();

	// mouse look
	float xDiff = (float)(m_MouseX - m_OldMouseX) * kMouseSensitivity;
	float yDiff = (float)(m_MouseY - m_OldMouseY) * kMouseSensitivity;
	
	// exponential decay function to smooth mouse movement
	float d = 1.0f - expf(logf(0.5f) * 30.0f);

	m_CameraRotX += yDiff * FrameTime::elapsed * d;
	m_CameraRotY += xDiff * FrameTime::elapsed * d;
	
	// clamp our rotation
	m_CameraRotX = Math::Clamp (m_CameraRotX, -(float)PI/2, (float)PI/2);
	if (m_CameraRotY < -PI)
		m_CameraRotY += (float)PI*2;
	if (m_CameraRotY > PI)
		m_CameraRotY -= (float)PI*2;

	camera->SetOrientation (quat(vec3(-m_CameraRotX,-m_CameraRotY,0)));

	float runFactor = 1.0f;
	if (gInput->IsKeyDown(GLFW_KEY_LSHIFT))
	{
		runFactor = kRunSpeed;
	}

	if (gInput->IsKeyDown('w'-kKeyOffset))
	{
		camera->Translate (camera->GetForward() * kMoveSpeed * runFactor);
	}
	if (gInput->IsKeyDown('s'-kKeyOffset))
	{
		camera->Translate (-camera->GetForward() * kMoveSpeed * runFactor);
	}
	if (gInput->IsKeyDown('d'-kKeyOffset))
	{
		camera->Translate (camera->GetRight() * kMoveSpeed * runFactor);
	}
	if (gInput->IsKeyDown('a'-kKeyOffset))
	{
		camera->Translate (-camera->GetRight() * kMoveSpeed * runFactor);
	}

	if (gInput->IsButtonClicked(0))
	{
		vec3 orientation = eulerAngles(camera->GetOrientation());
		vec3 position = camera->GetTranslation();
		int i = 0;
	}
}

void Application::overlayRender()
{
	TextureFont *font = gResources->GetFont("Data/Textures/font1.bmp");

	float currentY = 20;

	//font->RenderString("FPS: " + StrOp::ToString<int>(m_FPS), 10, currentY+=20);
	//font->RenderString("1,2,3,4: Display deferred composition steps", 10, currentY+=20);
	//font->RenderString("w,s,a,d: Move camera around", 10, currentY+=20);
	//font->RenderString("mouse move: Camera free look", 10, currentY+=20);
	//font->RenderString("h: Show/hide this help", 10, currentY+=20);
}

void Application::debugInputUpdate()
{
	static bool ssaostep = false;

	// reload shaders key
	if (gInput->IsKeyClicked(GLFW_KEY_F1))
	{
		gResources->ReloadShaders();
	}

	// debug buffer view
	if (gInput->IsKeyClicked('1'))
	{
		m_DeferredStep = DEFERRED_ALBEDO;
		ssaostep = false;
	}
	else if (gInput->IsKeyClicked('2'))
	{
		m_DeferredStep = DEFERRED_NORMALS;
		ssaostep = false;
	}
	else if (gInput->IsKeyClicked('3'))
	{
		m_DeferredStep = DEFERRED_DEPTH;
		ssaostep = false;
	}
	else if (gInput->IsKeyClicked('4'))
	{
		m_DeferredStep = DEFERRED_LIGHTMAP;
		ssaostep = false;
	}
	else if (gInput->IsKeyClicked('6'))
	{
		m_DeferredStep = DEFERRED_INVALID;
		ssaostep = false;
	}
	else if (gInput->IsKeyClicked('5'))
	{
		m_DeferredStep = DEFERRED_INVALID;
		ssaostep = true;
	}

	if (gInput->IsKeyClicked('i' - kKeyOffset))
	{
		RenderSettings::ShadowsEnabled = !RenderSettings::ShadowsEnabled;
	}

	if (gInput->IsKeyClicked('o' - kKeyOffset))
	{
		RenderSettings::ShadowFilter = !RenderSettings::ShadowFilter;
	}

	if (gInput->IsKeyClicked('p' - kKeyOffset))
	{
		RenderSettings::SMAA = !RenderSettings::SMAA;
	}

	if (gInput->IsKeyClicked('u' - kKeyOffset))
	{
		RenderSettings::SSAO = !RenderSettings::SSAO;
	}

	PostProcessEffect *smaa = gPostProcess->GetEffect("smaa");
	if (smaa != NULL)
	{
		smaa->SetEnabled (RenderSettings::SMAA);
	}

	DeferredCompositionEffect *dcmp = (DeferredCompositionEffect*)gPostProcess->GetEffect("deferredComposition");
	if (dcmp != NULL)
	{
		dcmp->DoSSAO = RenderSettings::SSAO;
		dcmp->SSAOStep = ssaostep;
	}

	// adjust ambient color
	if (gInput->IsKeyDown(']'))
	{
		RenderSettings::AmbientColor += vec3(1.0f) * FrameTime::elapsed * 2.0f;
		if (RenderSettings::AmbientColor.r > 1.0f)
		{
			RenderSettings::AmbientColor = vec3(1.0f);
		}
	}
	else if (gInput->IsKeyDown('['))
	{
		RenderSettings::AmbientColor -= vec3(1.0f) * FrameTime::elapsed * 2.0f;
		if (RenderSettings::AmbientColor.r < 0.0f)
		{
			RenderSettings::AmbientColor = vec3(0.0f);
		}
	}
}

void Application::Run()
{
	initialise();
	initialiseGLState();
	initialiseSubsystems();
	loadData();

	while (glfwGetWindowParam( GLFW_OPENED ) && m_Running)
	{
		float timeStart = (float)glfwGetTime();

		gInput->Update();
		if (gInput->IsKeyClicked(GLFW_KEY_ESC))
			m_Running = false;

		// mouse update
		m_OldMouseX = m_MouseX;
		m_OldMouseY = m_MouseY;

		gInput->GetMousePosition(m_MouseX, m_MouseY);

		debugInputUpdate();

		// updating
		cameraUpdate();

		// light update
		for (int i=0; i<kNumRingLights; i++)
		{
			string lightName = kRingLightPrefix + StrOp::ToString<int>(i);

			Object *ringLight = gScene->GetObject(lightName);
			ringLight->SetTranslation(vec3(kRingLightRotateRadius * sinf(FrameTime::total + i), 10 + 3 * sinf(FrameTime::total), kRingLightRotateRadius * cosf(FrameTime::total + i)));
		}

		for (int i=0; i<kNumWallSpotLights; i++)
		{
			string lightName = kWallSpotLightPrefix + StrOp::ToString<int>(i);

			Object *wallLight = gScene->GetObject(lightName);
			wallLight->RotateY (sinf(FrameTime::total*0.5f) * FrameTime::elapsed * 0.1f);
		}

		for (int i=0; i<kNumCorridorLights; i++)
		{
			string lightName = kCorridorLightPrefix + StrOp::ToString<int>(i);

			Object *corridorLight = gScene->GetObject(lightName);
			float xmovement = 0.0f;
			if (kCorridorLightMovement[i] == 1)
				xmovement = sinf(FrameTime::total) * 600;
			else if (kCorridorLightMovement[i] == -1)
				xmovement = cosf(FrameTime::total) * 600;

			corridorLight->Translate(vec3(xmovement, 0, 0) * FrameTime::elapsed);

			if (kCorridorLightRotation[i])
			{
				corridorLight->SetOrientation(quat(vec3(FrameTime::total, FrameTime::total, FrameTime::total)));
			}
		}

		for (int i=0; i<kNumCookieCorridorLights; i++)
		{
			string lightName = kCookieCorridorLightPrefix + StrOp::ToString<int>(i);

			Object *cookieLight = gScene->GetObject(lightName);

			cookieLight->RotateY (sinf(FrameTime::total*0.5f) * FrameTime::elapsed);
			cookieLight->RotateX (cosf(FrameTime::total*0.5f) * FrameTime::elapsed);
		}

		for (int i=0; i<kNumCurtainLights; i++)
		{
			string lightName = kCurtainLightPrefix + StrOp::ToString<int>(i);

			Object *curtainLight = gScene->GetObject(lightName);

			curtainLight->Translate(FrameTime::elapsed * 
								vec3(kCurtainLightRotateRadius * sinf(FrameTime::total),
								kCurtainLightRotateRadius * cosf(FrameTime::total), 0));
		}

		// middle air spot light rotation
		Object *middleAirSpot1 = gScene->GetObject("middleAirSpot1");
		middleAirSpot1->RotateY(FrameTime::elapsed);

		Object *middleAirSpot2 = gScene->GetObject("middleAirSpot2");
		middleAirSpot2->RotateY(-FrameTime::elapsed);
		//
		//


		// scene update
		gScene->Update();
		
		// rendering
		glClearColor(0,0,0,0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		gRenderer->Render();
		
		gPostProcess->Begin();
		// put in here whatever to be rendered
		gPostProcess->Process();

		// render our tooltips
		//if (gInput->IsKeyClicked('h' - kKeyOffset))
		//	m_ShowOverlay = !m_ShowOverlay;

		//if (m_ShowOverlay)
		//	overlayRender();

		displayBufferView(m_DeferredStep);

		glfwSwapBuffers();

		// update our elapsed and total timers
		float timeEnd = (float)glfwGetTime();
		FrameTime::elapsed = (timeEnd - timeStart);
		FrameTime::total += FrameTime::elapsed;

		m_NumFrames++;
		m_ElapsedFPS += FrameTime::elapsed;
		

		if (m_ElapsedFPS >= 1.0f)
		{
			m_FPS = m_NumFrames;
			cout << m_FPS << endl;

			m_ElapsedFPS = 0.0f;
			m_NumFrames = 0;
		}

		//Sleep(1);
	}
}

bool Application::fullscreenModeSupported(int width, int height)
{
	vector<GLFWvidmode>::iterator itModes;
	for (itModes=m_SupportedFullscreenModes.begin(); itModes!=m_SupportedFullscreenModes.end(); itModes++)
	{
		GLFWvidmode &mode = *itModes;
		if (mode.Width == width && mode.Height == height)
			return true;
	}
	return false;
}

void Application::initialise()
{
	// GL & GLFW
	if (!glfwInit())
		throw Exception("GLFW initialisation failed!");

	glfwSwapInterval (0);

	// create a glfw window with the opengl version requested
	glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, GL_TRUE);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, kGLMajor);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, kGLMinor);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwEnable( GLFW_KEY_REPEAT );
	glfwDisable(GLFW_AUTO_POLL_EVENTS);

	// read from the settings ini file and setup a window
	int windowMode = GLFW_WINDOW;

	// we won't need to switch display mode for this application, just hack it
	// quick and dirty
	vector<GLFWvidmode>::iterator itModes;

	int readWindowMode = GetPrivateProfileInt("Engine", "Fullscreen", 0, kSettingsFile.c_str());
	if (readWindowMode == 1)
	{
		windowMode = GLFW_FULLSCREEN;
		// find a suitable display mode, we just need a nice HD one
		enumerateFullscreenModes(&m_SupportedFullscreenModes);
		/*
		vector<GLFWvidmode> hdModes;
		for (itModes=m_SupportedFullscreenModes.begin(); itModes!=m_SupportedFullscreenModes.end(); itModes++)
		{
			GLFWvidmode &mode = *itModes;
			if (mode.Height == 720 || mode.Height == 900 || mode.Height == 1080 || mode.Height == 1050)
			{
				hdModes.push_back (mode);
			}
		}*/


		// see if the width, height that we have in the ini is supported.
		int fsWidth = GetPrivateProfileInt("Engine", "Width", 1280, kSettingsFile.c_str());
		int fsHeight = GetPrivateProfileInt("Engine", "Height", 720, kSettingsFile.c_str());

		GLFWvidmode fullscreenMode;

		if (fullscreenModeSupported(fsWidth, fsHeight))
		{
			fullscreenMode.Width = fsWidth;
			fullscreenMode.Height = fsHeight;

			m_SelectedDisplayMode = fullscreenMode;
		}
		else
		{
			// if not, reverse the list and get the first element, it's the highest resolution we can select
			reverse(m_SupportedFullscreenModes.begin(), m_SupportedFullscreenModes.end());
			GLFWvidmode fullscreenMode = m_SupportedFullscreenModes[0];

			m_SelectedDisplayMode = fullscreenMode;
		}
	}
	else
	{
		// setup a 16/9 display mode based on the desktop resolution
		findWindowedMode (&m_SelectedDisplayMode);
	}

	if (m_SelectedDisplayMode.Width == 0 || m_SelectedDisplayMode.Height == 0)
		throw Exception("Display mode is not set!");

	if ( !glfwOpenWindow( m_SelectedDisplayMode.Width,m_SelectedDisplayMode.Height,8,8,8,0,24,0,windowMode ) )
		throw Exception("GLFW cannot open the rendering window!");

	const GLubyte* GLVersion = glGetString(GL_VERSION);

	cout << "Using OpenGL Version " << GLVersion << endl;
	cout << "-------------------------------------------------------------------------" << endl;

	glfwSetWindowTitle ("Pantelis Lekakis - Dissertation Software");

	// Glew
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
		throw Exception("GLEW initialisation failed!");

	glfwDisable(GLFW_MOUSE_CURSOR);

	// IL
	ilInit();
	iluInit();
	ilOriginFunc(IL_ORIGIN_UPPER_LEFT);
    ilEnable(IL_ORIGIN_SET);   
	ilutEnable(ILUT_OPENGL_CONV);
	ilutRenderer(ILUT_OPENGL);

	m_Running = true;
}

void Application::initialiseGLState()
{
	glViewport(0,0,m_SelectedDisplayMode.Width, m_SelectedDisplayMode.Height);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}

void Application::initialiseSubsystems()
{
	// resources
	gResources = new Resources();
	gResources->Initialise();

	// renderer
	gRenderer = new Renderer();
	gRenderer->Initialise();

	// post-process
	gPostProcess = new PostProcess();
	gPostProcess->Initialise();

	// scene
	gScene = new Scene();
	gScene->Initialise();

	// input
	gInput = new Input();
	gInput->Initialise();

	m_Quad = new FSQ();
}

void Application::findWindowedMode (GLFWvidmode *displayMode)
{
	GLFWvidmode desktopDisplayMode;
    glfwGetDesktopMode (&desktopDisplayMode);

	float candidateWidth0 = (float)(3 * desktopDisplayMode.Width / 4);
	float candidateHeight0 = (float)(9 * candidateWidth0 / 16);
    
	float candidateHeight1 = (float)(3 * desktopDisplayMode.Height / 4);
	float candidateWidth1 = (float)(16 * candidateHeight1 / 9);
    
    if (candidateWidth0 < candidateWidth1)
	{
		(*displayMode).Width = (int)candidateWidth0;
		(*displayMode).Height = (int)candidateHeight0;
	}
	else
	{
		(*displayMode).Width = (int)candidateWidth1;
		(*displayMode).Height = (int)candidateHeight1;
	}
}

void Application::enumerateFullscreenModes(vector<GLFWvidmode> *supportedModes)
{
	GLFWvidmode glfwModes[500];
    int modeCount = glfwGetVideoModes (glfwModes, 500);
    
    for (int i=0; i<modeCount; i++) {
        GLFWvidmode glfwMode = glfwModes[i];
        
        // ignore really low resolutions
        if (glfwMode.Width < 640)
            continue;
		// and also ignore really high resolutions, see macbook retina display. too much memory and performance loss there
		if (glfwMode.Width > 1680)
			continue;
        
		// also, only support common wide formats
		if (glfwMode.Height != 720 && glfwMode.Height != 900 && glfwMode.Height != 1080 && glfwMode.Height != 1050)
			continue;

        // only allow for 32bit display modes (24 + 8 for alpha)
        int displayModeBit = glfwMode.RedBits + glfwMode.GreenBits + glfwMode.BlueBits;
        
        if (displayModeBit < 24)
            continue;
        
		(*supportedModes).push_back (glfwMode);
    }
}

void Application::createLights()
{
	// DYNAMIC, MOVING LIGHTS
	/*
	Central floating air spot light
	*/
	Object *middleAirSpot1 = gScene->AddObject("middleAirSpot1");
	middleAirSpot1->SetLight(new Light(LIGHT_SPOT, vec3(1,0.5f,0.2f), 1.0f, true, 1024));
	middleAirSpot1->SetTranslation(vec3(0,600,0));
	middleAirSpot1->GetLight()->SetSpotAngle(DEG2RAD(45.0f));
	middleAirSpot1->GetLight()->SetSpotHeight(3500.0f);
	middleAirSpot1->GetLight()->SetDepthBias(0.005f);
	middleAirSpot1->GetLight()->SetCookie(gResources->LoadTexture("Data/Textures/spotCookie.png", false));

	Object *middleAirSpot2 = gScene->AddObject("middleAirSpot2");
	middleAirSpot2->SetLight(new Light(LIGHT_SPOT, vec3(0.15f,0.7f,0.2f), 1.0f, true, 1024));
	middleAirSpot2->SetTranslation(vec3(0,700,0));
	middleAirSpot2->GetLight()->SetSpotAngle(DEG2RAD(45.0f));
	middleAirSpot2->GetLight()->SetSpotHeight(3500.0f);
	middleAirSpot2->GetLight()->SetDepthBias(0.005f);
	middleAirSpot2->GetLight()->SetCookie(gResources->LoadTexture("Data/Textures/spotCookie.png", false));

	/*
	Central point light ring
	*/
	for (int i=0; i<kNumRingLights; i++)
	{
		string lightName = kRingLightPrefix + StrOp::ToString<int>(i);
		
		Object *ringLight = gScene->AddObject(lightName);
		ringLight->SetLight(new Light(LIGHT_POINT, vec3(Math::RandomF(0,1),Math::RandomF(0,1),Math::RandomF(0,1)), 1.0f, false, 512));
		ringLight->SetTranslation(vec3(0,10,0));
		ringLight->GetLight()->SetPointRadius(kRingLightRadius);
	}

	/*
	Wall spot lights
	*/
	for (int i=0; i<kNumWallSpotLights; i++)
	{
		string lightName = kWallSpotLightPrefix + StrOp::ToString<int>(i);

		Object *wallLight = gScene->AddObject(lightName);
		wallLight->SetLight(new Light(LIGHT_SPOT, kWallSpotLightColor[i], 1.0f, true, 1024));
		wallLight->SetTranslation(kWallSpotLightPos[i]);
		wallLight->GetLight()->SetSpotAngle(DEG2RAD(kWallSpotLightFOV[i]));
		wallLight->GetLight()->SetSpotHeight(3000.0f);
		wallLight->GetLight()->SetCookie(gResources->LoadTexture("Data/Textures/spotCookie.png", false));
		wallLight->SetOrientation(quat(kWallSpotLightRot[i]));
		wallLight->SetTranslation(kWallSpotLightPos[i]);
		wallLight->GetLight()->SetDepthBias(0.002f);
	}

	/*
	Corridor point lights
	*/
	for (int i=0; i<kNumCorridorLights; i++)
	{
		string lightName = kCorridorLightPrefix + StrOp::ToString<int>(i);
		
		Object *corridorLight = gScene->AddObject(lightName);
		corridorLight->SetLight(new Light(LIGHT_POINT, kCorridorLightColor[i], 1.0f, true, 1024));
		corridorLight->SetTranslation(kCorridorLightPos[i]);
		corridorLight->GetLight()->SetPointRadius(kCorridorLightRadius[i]);
		corridorLight->GetLight()->SetDepthBias(0.005f);

		string cookie = kCorridorLightCookie[i];
		if (cookie != "")
		{
			corridorLight->GetLight()->SetCookie(gResources->LoadTexture(cookie, true));
		}
	}

	/*
	Cookie corridor point lights (they dont cast shadow for now)
	*/
	for (int i=0; i<kNumCookieCorridorLights; i++)
	{
		string lightName = kCookieCorridorLightPrefix + StrOp::ToString<int>(i);
		
		Object *corridorLight = gScene->AddObject(lightName);
		corridorLight->SetLight(new Light(LIGHT_POINT, kCookieCorridorLightColor[i], 1.0f, false, 512));
		corridorLight->SetTranslation(kCookieCorridorLightPos[i]);
		corridorLight->GetLight()->SetPointRadius(kCookieCorridorLightRadius[i]);
		corridorLight->GetLight()->SetCookie(gResources->LoadTexture( kCookieCorridorLightCookie[i], true));
	}

	/*
	Curtain lights. the ones that lie on the curtains.. duh :) no shadows again, they are small surface lights
	*/
	for (int i=0; i<kNumCurtainLights; i++)
	{
		string lightName = kCurtainLightPrefix + StrOp::ToString<int>(i);
		
		Object *curtainLight = gScene->AddObject(lightName);
		curtainLight->SetLight(new Light(LIGHT_POINT, kCurtainLightColor[i], 1.0f, false, 512));
		curtainLight->SetTranslation(kCurtainLightPos[i]);
		curtainLight->GetLight()->SetPointRadius(kCurtainLightRadius[i]);
	}

	// STATIC LIGHTS
	// the light in the center of the ring
	Object *ringCenterLight = gScene->AddObject("ringCenterLight");
	ringCenterLight->SetLight(new Light(LIGHT_POINT, vec3(1,1,1), 1.0f, false, 512));
	ringCenterLight->GetLight()->SetCookie(gResources->LoadTexture("Data/Textures/PointCookie/ueaCookie.cube", true));
	ringCenterLight->SetTranslation(vec3(0,20,0));
	ringCenterLight->GetLight()->SetPointRadius(100);

	// the light in the 1st story, pointing towards the curtain
	Object *curtainSpot1 = gScene->AddObject("curtainSpot1");
	curtainSpot1->SetLight(new Light(LIGHT_SPOT, vec3(0.7f,0.7f,1), 1.0f, true, 1024));
	curtainSpot1->SetTranslation(vec3(500,1000,0));
	curtainSpot1->GetLight()->SetSpotAngle(DEG2RAD(60.0f));
	curtainSpot1->GetLight()->SetSpotHeight(3500.0f);
	curtainSpot1->SetOrientation(quat(vec3(-PI/3,0,0)));
	curtainSpot1->GetLight()->SetDepthBias(0.005f);
	curtainSpot1->GetLight()->SetCookie(gResources->LoadTexture("Data/Textures/spotCookie.png", true));

	// the light in the 1st story, behind the curtain near the orange light :)
	Object *curtainSpot2 = gScene->AddObject("curtainSpot2");
	curtainSpot2->SetLight(new Light(LIGHT_SPOT, vec3(0.7f,0.4f,1), 1.0f, true, 1024));
	curtainSpot2->SetTranslation(vec3(1206,172,476));
	curtainSpot2->GetLight()->SetSpotAngle(DEG2RAD(60.0f));
	curtainSpot2->GetLight()->SetSpotHeight(3500.0f);
	curtainSpot2->SetOrientation(quat(vec3(DEG2RAD(-25), DEG2RAD(55), DEG2RAD(-1.4f))));
	curtainSpot2->GetLight()->SetDepthBias(0.005f);
	curtainSpot2->GetLight()->SetCookie(gResources->LoadTexture("Data/Textures/spotCookie.png", true));

}

void Application::loadData()
{
	Shader *shader = gResources->LoadShader("Data/Shaders/ScreenAlignedQuad.shader");


	Texture *loadingTexture = gResources->LoadTexture("Data/Textures/loading.png", false, true);
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLuint loadingTexId = loadingTexture->GetID();

	shader->Set();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, loadingTexId);
	
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );

	shader->Uniform1("texture", 1, &loadingTexId);

	m_Quad->Render();
	
	glBindTexture(GL_TEXTURE_2D, 0);

	glfwSwapBuffers();

	gPostProcess->AddEffect(new DeferredCompositionEffect("deferredComposition"));
	gPostProcess->AddEffect(new SMAAEffect("smaa"));
	
	Object* obj = gScene->AddObject("camera");
	obj->SetCamera(new Camera(45.0f, (float)m_SelectedDisplayMode.Width/(float)m_SelectedDisplayMode.Height, 0.1f, 5000.0f));

	gScene->SetCamera(obj);


	Object* mesh = gScene->AddObject("scene");
	mesh->SetMesh(gResources->LoadMesh("Data/Models/Sponza/sponza_noplants.obj"));

	gResources->LoadFont("Data/Textures/font1.bmp", 16);

	createLights();
}

void Application::displayBufferView(eDeferredCompositionStep target)
{
	const RenderTarget* const gbuffer = gRenderer->GetGBuffer();
	GLuint texture = 0;
	Shader *shader = gResources->GetShader("Data/Shaders/ScreenAlignedQuad.shader");

	switch (target)
	{
	case DEFERRED_INVALID:
		return;

	case DEFERRED_ALBEDO:
		texture = gbuffer->GetTexture("albedo");
		break;

	case DEFERRED_NORMALS:
		texture = gbuffer->GetTexture("normal");
		break;

	case DEFERRED_DEPTH:
		texture = gbuffer->GetTexture("depth");
		break;

	case DEFERRED_LIGHTMAP:
		texture = gRenderer->GetLightmap()->GetTexture("lightmap");
		break;

	default:
		return;
	}

	shader->Set();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	shader->Uniform1("texture", 1, &texture);

	m_Quad->Render();
	
	glBindTexture(GL_TEXTURE_2D, 0);
}