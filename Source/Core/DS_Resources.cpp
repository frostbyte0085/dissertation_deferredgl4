#include <Core/DS_Resources.h>
#include <Tools/DS_StrOp.h>

#include <Graphics/DS_Mesh.h>
#include <Graphics/DS_Shader.h>
#include <Graphics/DS_Texture.h>
#include <Graphics/DS_TextureFont.h>

namespace DS
{
	Resources* gResources = NULL;
}

using namespace DS;

Resources::Resources()
{
}

Resources::~Resources()
{
	// delete meshes
	MeshMap::iterator itMeshes;
	for (itMeshes = m_Meshes.begin(); itMeshes != m_Meshes.end(); itMeshes++)
	{
		Mesh *mesh = itMeshes->second;
		if (mesh != NULL)
			delete mesh;
		mesh = NULL;
	}
	m_Meshes.clear();

	// delete shaders
	ShaderMap::iterator itShaders;
	for (itShaders = m_Shaders.begin(); itShaders != m_Shaders.end(); itShaders++)
	{
		Shader *shader = itShaders->second;
		if (shader != NULL)
			delete shader;
		shader = NULL;
	}
	m_Shaders.clear();

	// delete textures
	TextureMap::iterator itTextures;
	for (itTextures= m_Textures.begin(); itTextures != m_Textures.end(); itTextures++)
	{
		Texture *texture = itTextures->second;
		if (texture != NULL)
			delete texture;
		texture = NULL;
	}
	m_Textures.clear();

	// delete texture fonts
	TextureFontMap::iterator itTextureFonts;
	for (itTextureFonts= m_TextureFonts.begin(); itTextureFonts != m_TextureFonts.end(); itTextureFonts++)
	{
		TextureFont *font = itTextureFonts->second;
		if (font != NULL)
			delete font;
		font = NULL;
	}
	m_TextureFonts.clear();

}

void Resources::Initialise()
{
	
}

// MESH MANAGEMENT
//
Mesh* Resources::LoadMesh(const string& filename)
{
	string filenameLowered = StrOp::ToLower(filename);

	Mesh *mesh = GetMesh(filenameLowered);
	if (mesh == NULL)
	{
		mesh = new Mesh(filename);
		mesh->create();
		m_Meshes[filenameLowered] = mesh;
	}
	return mesh;
}

Mesh* Resources::GetMesh(const string& filename)
{
	string filenameLowered = StrOp::ToLower(filename);

	Mesh *mesh = NULL;
	
	MeshMap::iterator it = m_Meshes.find(filenameLowered);
	if (it != m_Meshes.end())
	{
		mesh = it->second;
	}
	return mesh;
}

void Resources::RemoveMesh(const string &filename)
{
	string filenameLowered = StrOp::ToLower(filename);

	Mesh* mesh = GetMesh(filenameLowered);
	if (mesh != NULL)
	{
		m_Meshes.erase(filenameLowered);
		delete mesh;
		mesh = NULL;
	}
}
//

// SHADER MANAGEMENT
void Resources::ReloadShaders()
{
	ShaderMap::iterator it;
	for (it=m_Shaders.begin(); it!=m_Shaders.end(); it++)
	{
		Shader* shader = it->second;
		if (shader != NULL)
		{
			shader->Recompile();
		}
	}
}

Shader* Resources::LoadShader(const string& filename)
{
	string filenameLowered = StrOp::ToLower(filename);

	Shader *shader = GetShader(filenameLowered);
	if (shader == NULL)
	{
		shader = new Shader(filename);
		shader->create();
		m_Shaders[filenameLowered] = shader;
	}	
	return shader;
}

Shader* Resources::GetShader(const string& filename)
{
	string filenameLowered = StrOp::ToLower(filename);

	Shader *shader = NULL;
	
	ShaderMap::iterator it = m_Shaders.find(filenameLowered);
	if (it != m_Shaders.end())
	{
		shader = it->second;
	}
	return shader;
}

void Resources::RemoveShader(const string &filename)
{
	string filenameLowered = StrOp::ToLower(filename);

	Shader* shader = GetShader(filenameLowered);
	if (shader != NULL)
	{
		m_Shaders.erase(filenameLowered);
		delete shader;
		shader = NULL;
	}
}
//

// TEXTURE MANAGEMENT
Texture* Resources::LoadTexture(const string& filename, bool mipmaps, bool filter, bool clamp, GLenum format, GLenum internalFormat, int width, int height)
{
	string filenameLowered = StrOp::ToLower(filename);

	Texture *texture = GetTexture(filenameLowered);
	if (texture == NULL)
	{
		texture = new Texture(filename, mipmaps, format, internalFormat, filter, clamp, width, height);
		if (StrOp::EndsWith(filenameLowered, ".cube"))
			texture->createCube();
		else
			texture->create2D();

		m_Textures[filenameLowered] = texture;
	}	
	return texture;
}

Texture* Resources::GetTexture(const string& filename)
{
	string filenameLowered = StrOp::ToLower(filename);

	Texture *texture = NULL;
	
	TextureMap::iterator it = m_Textures.find(filenameLowered);
	if (it != m_Textures.end())
	{
		texture = it->second;
	}
	return texture;
}

void Resources::RemoveTexture(const string &filename)
{
	string filenameLowered = StrOp::ToLower(filename);

	Texture* texture = GetTexture(filenameLowered);
	if (texture != NULL)
	{
		m_Textures.erase(filenameLowered);
		delete texture;
		texture = NULL;
	}
}
//

// TEXTURE FONT MANAGEMENT
TextureFont* Resources::LoadFont(const string& filename, float fontSize)
{
	string filenameLowered = StrOp::ToLower(filename);

	TextureFont *font = GetFont(filenameLowered);
	if (font == NULL)
	{
		font = new TextureFont(filename, fontSize);
		font->create();

		m_TextureFonts[filenameLowered] = font;
	}	
	return font;
}

TextureFont* Resources::GetFont(const string& filename)
{
	string filenameLowered = StrOp::ToLower(filename);

	TextureFont *font = NULL;
	
	TextureFontMap::iterator it = m_TextureFonts.find(filenameLowered);
	if (it != m_TextureFonts.end())
	{
		font = it->second;
	}
	return font;
}

void Resources::RemoveFont(const string &filename)
{
	string filenameLowered = StrOp::ToLower(filename);

	TextureFont* font = GetFont(filenameLowered);
	if (font != NULL)
	{
		m_TextureFonts.erase(filenameLowered);
		delete font;
		font = NULL;
	}
}
//