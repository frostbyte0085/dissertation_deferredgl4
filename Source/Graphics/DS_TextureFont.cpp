#include "DS_TextureFont.h"
#include "DS_Shader.h"
#include "DS_Texture.h"
#include <Core/DS_Resources.h>

using namespace DS;

TextureFont::TextureFont(const string &filename, float fontSize) : m_Texture(NULL), m_VAOID(0), m_PositionVBO(0), m_TexCoordVBO(0), m_Shader(NULL)
{
	m_Filename = filename;
	m_Size = fontSize;
}

TextureFont::~TextureFont()
{
	if (m_VAOID > 0)
		glDeleteVertexArrays(1, &m_VAOID);
	if (m_PositionVBO > 0)
		glDeleteBuffers(1, &m_PositionVBO);
	if (m_TexCoordVBO > 0)
		glDeleteBuffers(1, &m_TexCoordVBO);
}

void TextureFont::create()
{
	m_Texture = gResources->LoadTexture(m_Filename, false);
	m_Shader = gResources->LoadShader("Data/Shaders/Font.shader");

	float vertices [] = {
	    0.0f, 0.0f,
	    m_Size, 0.0f,
	    m_Size, m_Size,
	    0.0f, m_Size
	};

	glGenVertexArrays(1, &m_VAOID);
	glBindVertexArray(m_VAOID);

	glGenBuffers(1, &m_PositionVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_PositionVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 8, &vertices[0], GL_STATIC_DRAW);

	m_Shader->EnableVertexAttribArray("in_Position");
	m_Shader->VertexAttribPointer("in_Position", 2, GL_FLOAT, GL_FALSE, 0, 0);
	
	//Just initialize with something for now, the tex coords are updated
	//for each character printed
	float texCoords [] = {
	    0.0f, 0.0f,
	    0.0f, 0.0f,
	    0.0f, 0.0f,
	    0.0f, 0.0f
	};

	glGenBuffers(1, &m_TexCoordVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_TexCoordVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 8, &texCoords[0], GL_DYNAMIC_DRAW);

	m_Shader->EnableVertexAttribArray("in_TexCoord");
	m_Shader->VertexAttribPointer("in_TexCoord", 2, GL_FLOAT, GL_FALSE, 0, 0);

	float vp[4];
	glGetFloatv(GL_VIEWPORT, vp);

	m_Ortho2D = ortho<float> (0.0f, vp[2], 0.0f, vp[3], -1.0f, 1.0f);
}

void TextureFont::RenderString(const string &str, float x, float y)
{

	float vp[4];
	glGetFloatv(GL_VIEWPORT, vp);

	y = vp[3] - y;

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glDisable(GL_DEPTH_TEST);

	m_Shader->Set();
	float texCoords[8];
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Texture->GetID());

	float currentX = x;

	glBindVertexArray(m_VAOID);

	m_Shader->UniformMatrix4("projection", 1, GL_FALSE, value_ptr(m_Ortho2D));
	m_Shader->UniformMatrix4("view", 1, GL_FALSE, value_ptr(mat4(1)));

	for (string::size_type i=0; i<str.size(); ++i)
	{
		const float oneOverSixteen = 1.0f / 16.0f;

        int ch = int(str[i]);
        float xPos = float(ch % 16) * oneOverSixteen;
        float yPos = float(ch / 16) * oneOverSixteen;

        texCoords[0] = xPos;
        texCoords[1] = 1.0f - yPos - oneOverSixteen;

        texCoords[2] = xPos + oneOverSixteen;
        texCoords[3] = 1.0f - yPos - oneOverSixteen;

        texCoords[4] = xPos + oneOverSixteen;
        texCoords[5] = 1.0f - yPos - 0.001f;

        texCoords[6] = xPos;
        texCoords[7] = 1.0f - yPos - 0.001f;

        glBindBuffer(GL_ARRAY_BUFFER, m_TexCoordVBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 8, &texCoords[0]);

		mat4 world = translate(currentX,y,0.0f);

		m_Shader->UniformMatrix4("world", 1, GL_FALSE, value_ptr(world));

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

		currentX += m_Size * 0.8f;
	}


	glBindVertexArray(0);
	Shader::Unset();

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}