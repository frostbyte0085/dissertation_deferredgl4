#pragma once

#include <DS_Include.h>

// many thanks to
//http://www.spacesimulator.net/wiki/index.php?title=Tutorials:Bitmapped_Fonts_(OpenGL3.3)
namespace DS
{
	class Shader;
	class Texture;

	class TextureFont
	{
		friend class Resources;
	public:
		void RenderString(const string& str, float x, float y);
	private:
		void create();

		TextureFont(const string& filename, float fontSize);
		~TextureFont();

		Texture *m_Texture;
		GLuint m_VAOID;

		GLuint m_TexCoordVBO;
		GLuint m_PositionVBO;

		float m_Size;
		string m_Filename;
		Shader *m_Shader;

		mat4 m_Ortho2D;
	};
}