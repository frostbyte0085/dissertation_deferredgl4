#include "DS_Shader.h"

#include <Tools/DS_Exception.h>

using namespace DS;

Shader* Shader::s_BoundShader = NULL;

Shader::Shader(const string & filename) : m_Filename(""), m_ShaderID(0), m_VP(0), m_FP(0), m_TSCP(0), m_TSEP(0), m_GP(0)
{
	m_Filename = filename;
}

Shader::~Shader()
{
	Destroy();
}

void Shader::Destroy()
{
	glUseProgram(0);
	Shader::s_BoundShader = NULL;

	if (m_TSCP > 0)
	{
		glDetachShader(m_ShaderID, m_TSCP);
		glDeleteShader(m_TSCP);
	}

	if (m_TSEP > 0)
	{
		glDetachShader(m_ShaderID, m_TSEP);
		glDeleteShader(m_TSEP);
	}

	if (m_GP > 0)
	{
		glDetachShader(m_ShaderID, m_GP);
		glDeleteShader(m_GP);
	}

	glDetachShader(m_ShaderID, m_FP);
	glDeleteShader(m_FP);

    glDetachShader(m_ShaderID, m_VP);
	glDeleteShader(m_VP);

    glDeleteProgram(m_ShaderID);
}

void Shader::Unset()
{
	glUseProgram(0);
	Shader::s_BoundShader = NULL;
}

void Shader::create()
{
	int readWhat = -1; // 0 is vs, 1 is ps

	string line;
	string vsSource;
	string fpSource;
	string tscpSource;
	string tsepSource;
	string gpSource;

	// parse common file, used for common function definitions
	string commonSource;
	ifstream commonFile("Data/Shaders/common.h");
	if (commonFile.is_open())
	{
		while (commonFile.good())
		{
			getline(commonFile, line);
			commonSource += line + "\n";
		}
	}
	commonFile.close();

	ifstream shaderFile (m_Filename);
	if (shaderFile.is_open())
	{
		while (shaderFile.good())
		{
			getline(shaderFile, line);
			if (line == "@VS")
			{
				readWhat = 0;
			}
			else if (line == "@PS")
			{
				readWhat = 1;
			}
			else if (line == "@TSC")
			{
				readWhat = 2;
			}
			else if (line == "@TSE")
			{
				readWhat = 3;
			}
			else if (line == "@GS")
			{
				readWhat = 4;
			}
			else
			{
				if (line == "#include common")
				{
					
					line = commonSource;
				}

				// read vs
				if (readWhat == 0)
				{
					vsSource += line + "\n";
				}
				else if (readWhat == 1)
				{
					fpSource += line + "\n";
				}
				else if (readWhat == 2)
				{
					tscpSource += line + "\n";
				}
				else if (readWhat == 3)
				{
					tsepSource += line + "\n";
				}
				else if (readWhat == 4)
				{
					gpSource += line + "\n";
				}
			}
		}

		shaderFile.close();
	}		

	createVertexShader(vsSource);
	createFragmentShader(fpSource);
	if (tscpSource.size() > 0)
	{
		createTesselationControlShader(tscpSource);
	}

	if (tsepSource.size() > 0)
	{
		createTesselationEvalShader(tsepSource);
	}

	if (gpSource.size() > 0)
	{
		createGeometryShader(gpSource);
	}

	if (!link())
	{
		throw Exception("Cannot link shader " + m_Filename + ",  view console for details");
	}
}

bool Shader::createGeometryShader(const string &programSource)
{
    m_GP = glCreateShader(GL_GEOMETRY_SHADER);

    const char* source = programSource.c_str();

    glShaderSource(m_GP, 1, &source, 0);
    glCompileShader(m_GP);
    validateShader(m_GP);

    return true;
}

bool Shader::createTesselationControlShader(const string &programSource)
{
    m_TSCP = glCreateShader(GL_TESS_CONTROL_SHADER);

    const char* source = programSource.c_str();

    glShaderSource(m_TSCP, 1, &source, 0);
    glCompileShader(m_TSCP);
    validateShader(m_TSCP);

    return true;
}

bool Shader::createTesselationEvalShader(const string &programSource)
{
    m_TSEP = glCreateShader(GL_TESS_EVALUATION_SHADER);

    const char* source = programSource.c_str();

    glShaderSource(m_TSEP, 1, &source, 0);
    glCompileShader(m_TSEP);
    validateShader(m_TSEP);

    return true;
}

bool Shader::createVertexShader(const string &programSource)
{
    m_VP = glCreateShader(GL_VERTEX_SHADER);

    const char* source = programSource.c_str();

    glShaderSource(m_VP, 1, &source, 0);
    glCompileShader(m_VP);
    validateShader(m_VP);

    return true;
}

bool Shader::createFragmentShader(const string &programSource)
{ 
    m_FP = glCreateShader(GL_FRAGMENT_SHADER);
    
    const char* source = programSource.c_str();

    glShaderSource(m_FP, 1, &source, 0);
    glCompileShader(m_FP);
    validateShader(m_FP);
    
    return true;
}

bool Shader::link() {
    m_ShaderID = glCreateProgram();

    glAttachShader(m_ShaderID, m_VP);
    glAttachShader(m_ShaderID, m_FP);
	if (m_TSCP > 0)
	{
		glAttachShader(m_ShaderID, m_TSCP);
	}
	if (m_TSEP > 0)
	{
		glAttachShader(m_ShaderID, m_TSEP);
	}
	if (m_GP > 0)
	{
		glAttachShader(m_ShaderID, m_GP);
	}


    glLinkProgram(m_ShaderID);
  
    if (!validateProgram(m_ShaderID))
		return false;
    
    return true;
}

bool Shader::validateShader(GLuint shader) {
    
    const unsigned int bufferSize = 512;
    char buffer[bufferSize];
    memset(buffer, 0, bufferSize);
    GLsizei length = 0;
    
    glGetShaderInfoLog(shader, bufferSize, &length, buffer);
	
    if (length > 0)
	{
		cout << "(" << m_Filename << ") " <<  buffer << endl;
	}
    return true;
}

bool Shader::validateProgram(GLuint program) {
    
    const unsigned int bufferSize = 512;
    char buffer[bufferSize];
    memset(buffer, 0, bufferSize);
    GLsizei length = 0;
    
    memset(buffer, 0, bufferSize);
    glGetProgramInfoLog(program, bufferSize, &length, buffer);

    glValidateProgram(program);
    GLint status;
    glGetProgramiv(program, GL_VALIDATE_STATUS, &status);
    if (length > 0)
	{
		cout << "(" << m_Filename << ") " << buffer << endl;
	}
	if (status == GL_FALSE)
	{
		return false;
	}
    
    return true;
}

void Shader::Set()
{
	glUseProgram(m_ShaderID);
	Shader::s_BoundShader = this;
}

void Shader::Recompile()
{
	create();
}

void Shader::Relink()
{
	glLinkProgram(m_ShaderID);
}

void Shader::BindFragDataLocation(const string& name, GLuint loc)
{
	glBindFragDataLocation(m_ShaderID, loc, name.c_str());
}

void Shader::BindAttribLocation(const string& name, GLuint loc)
{
	glBindAttribLocation(m_ShaderID, loc, name.c_str());
}

void Shader::EnableVertexAttribArray(const string& name)
{
	glEnableVertexAttribArray(getAttribLocation(name));
}

void Shader::DisableVertexAttribArray(const string& name)
{
	glDisableVertexAttribArray(getAttribLocation(name));
}

// UNIFORM & VERTEXATTRIB METHODS
//
GLint Shader::getUniformLocation(const string& name)
{
	GLint location = glGetUniformLocation(m_ShaderID, name.c_str());

	if (location == -1)
	{
		std::string::size_type index = name.find("gl_");
		
		switch (index)
		{
		default:
			//cout << "Name " + name + " does not correspond to an active uniform in shader " + m_Filename + "!" << endl;
			return -1;

		case 0:
			break;
		}
	}

	return location;
}

GLint Shader::getAttribLocation(const string& name)
{
	GLint location = glGetAttribLocation(m_ShaderID, name.c_str());

	if (location == -1)
	{
		std::string::size_type index = name.find("gl_");

		switch (index)
		{
		default:
			cout << "Name " + name + " does not correspond to an active attribute in shader " + m_Filename + "!" << endl;
			return -1;

		case 0:
			break;
		}
	}

	return location;
}

//-----------------------------------------------------------------------------
// glUniform
//-----------------------------------------------------------------------------
void Shader::Uniform1(const string& name, GLfloat v0)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform1f(loc, v0);
}

void Shader::Uniform1(const string& name, GLint v0)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform1i(loc, v0);
}

void Shader::Uniform1(const string& name, GLuint v0)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform1ui(loc, v0);
}

void Shader::Uniform1(const string& name, GLsizei count, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform1fv(loc, count, value);
}

void Shader::Uniform1(const string& name, GLsizei count, const GLint *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform1iv(loc, count, value);
}

void Shader::Uniform1(const string& name, GLsizei count, const GLuint *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform1uiv(loc, count, value);
}

void Shader::Uniform2(const string& name, GLfloat v0, GLfloat v1)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform2f(loc, v0, v1);
}

void Shader::Uniform2(const string& name, GLint v0, GLint v1)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform2i(loc, v0, v1);
}

void Shader::Uniform2(const string& name, GLuint v0, GLuint v1)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform2ui(loc, v0, v1);
}

void Shader::Uniform2(const string& name, GLsizei count, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform2fv(loc, count, value);
}

void Shader::Uniform2(const string& name, GLsizei count, const GLint *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform2iv(loc, count, value);
}

void Shader::Uniform2(const string& name, GLsizei count, const GLuint *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform2uiv(loc, count, value);
}

void Shader::Uniform3(const string& name, GLfloat v0, GLfloat v1, GLfloat v2)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform3f(loc, v0, v1, v2);
}

void Shader::Uniform3(const string& name, GLint v0, GLint v1, GLint v2)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform3i(loc, v0, v1, v2);
}

void Shader::Uniform3(const string& name, GLuint v0, GLuint v1, GLuint v2)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform3ui(loc, v0, v1, v2);
}

void Shader::Uniform3(const string& name, GLsizei count, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform3fv(loc, count, value);
}

void Shader::Uniform3(const string& name, GLsizei count, const GLint *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform3iv(loc, count, value);
}

void Shader::Uniform3(const string& name, GLsizei count, const GLuint *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform3uiv(loc, count, value);
}

void Shader::Uniform4(const string& name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform4f(loc, v0, v1, v2, v3);
}

void Shader::Uniform4(const string& name, GLint v0, GLint v1, GLint v2, GLint v3)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform4i(loc, v0, v1, v2, v3);
}

void Shader::Uniform4(const string& name, GLuint v0, GLuint v1, GLuint v2, GLuint v3)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform4ui(loc, v0, v1, v2, v3);
}

void Shader::Uniform4(const string& name, GLsizei count, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform4fv(loc, count, value);
}

void Shader::Uniform4(const string& name, GLsizei count, const GLint *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform4iv(loc, count, value);
}

void Shader::Uniform4(const string& name, GLsizei count, const GLuint *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniform4uiv(loc, count, value);
}

void Shader::UniformMatrix2(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniformMatrix2fv(loc, count, transpose, value);
}

void Shader::UniformMatrix2x3(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniformMatrix2x3fv(loc, count, transpose, value);
}

void Shader::UniformMatrix2x4(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniformMatrix2x4fv(loc, count, transpose, value);
}

void Shader::UniformMatrix3(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniformMatrix3fv(loc, count, transpose, value);
}

void Shader::UniformMatrix3x2(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniformMatrix3x2fv(loc, count, transpose, value);
}

void Shader::UniformMatrix3x4(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniformMatrix3x4fv(loc, count, transpose, value);
}

void Shader::UniformMatrix4(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniformMatrix4fv(loc, count, transpose, value);
}

void Shader::UniformMatrix4x2(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniformMatrix4x2fv(loc, count, transpose, value);
}

void Shader::UniformMatrix4x3(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value)
{
	GLint loc = getUniformLocation(name);
	if (loc < 0)
		return;

	glUniformMatrix4x3fv(loc, count, transpose, value);
}

//-----------------------------------------------------------------------------
// glVertexAttrib
//-----------------------------------------------------------------------------
void Shader::VertexAttrib1(const string& name, GLdouble x)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;

	glVertexAttrib1d(loc, x);
}

void Shader::VertexAttrib1(const string& name, GLfloat x)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib1f(loc, x);
}

void Shader::VertexAttrib1(const string& name, GLshort x)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib1s(loc, x);
}

void Shader::VertexAttrib1(const string& name, const GLdouble *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib1dv(loc, v);
}

void Shader::VertexAttrib1(const string& name, const GLfloat *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib1fv(loc, v);
}

void Shader::VertexAttrib1(const string& name, const GLshort *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib1sv(loc, v);
}

void Shader::VertexAttrib2(const string& name, GLdouble x, GLdouble y)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib2d(loc, x, y);
}

void Shader::VertexAttrib2(const string& name, GLfloat x, GLfloat y)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib2f(loc, x, y);
}

void Shader::VertexAttrib2(const string& name, GLshort x, GLshort y)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib2s(loc, x, y);
}

void Shader::VertexAttrib2(const string& name, const GLdouble *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib2dv(loc, v);
}

void Shader::VertexAttrib2(const string& name, const GLfloat *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib2fv(loc, v);
}

void Shader::VertexAttrib2(const string& name, const GLshort *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib2sv(loc, v);
}

void Shader::VertexAttrib3(const string& name, GLdouble x, GLdouble y, GLdouble z)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib3d(loc, x, y, z);
}

void Shader::VertexAttrib3(const string& name, GLfloat x, GLfloat y, GLfloat z)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib3f(loc, x, y, z);
}

void Shader::VertexAttrib3(const string& name, GLshort x, GLshort y, GLshort z)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib3s(loc, x, y, z);
}

void Shader::VertexAttrib3(const string& name, const GLdouble *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib3dv(loc, v);
}

void Shader::VertexAttrib3(const string& name, const GLfloat *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib3fv(loc, v);
}

void Shader::VertexAttrib3(const string& name, const GLshort *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib3sv(loc, v);
}

void Shader::VertexAttrib4(const string& name, GLdouble x, GLdouble y, GLdouble z, GLdouble w)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4d(loc, x, y, z, w);
}

void Shader::VertexAttrib4(const string& name, GLfloat x, GLfloat y, GLfloat z, GLfloat w)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4f(loc, x, y, z, w);
}

void Shader::VertexAttrib4(const string& name, GLshort x, GLshort y, GLshort z, GLshort w)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4s(loc, x, y, z, w);
}

void Shader::VertexAttrib4(const string& name, const GLbyte *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4bv(loc, v);
}

void Shader::VertexAttrib4(const string& name, const GLdouble *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4dv(loc, v);
}

void Shader::VertexAttrib4(const string& name, const GLfloat *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4fv(loc, v);
}

void Shader::VertexAttrib4(const string& name, const GLint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4iv(loc, v);
}

void Shader::VertexAttrib4(const string& name, const GLshort *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4sv(loc, v);
}

void Shader::VertexAttrib4(const string& name, const GLubyte *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4ubv(loc, v);
}

void Shader::VertexAttrib4(const string& name, const GLuint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4uiv(loc, v);
}

void Shader::VertexAttrib4(const string& name, const GLushort *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4usv(loc, v);
}

void Shader::VertexAttrib4N(const string& name, GLubyte x, GLubyte y, GLubyte z, GLubyte w)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4Nub(loc, x, y, z, w);
}

void Shader::VertexAttrib4N(const string& name, const GLbyte *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4Nbv(loc, v);
}

void Shader::VertexAttrib4N(const string& name, const GLint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4Niv(loc, v);
}

void Shader::VertexAttrib4N(const string& name, const GLshort *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4Nsv(loc, v);
}

void Shader::VertexAttrib4N(const string& name, const GLubyte *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4Nubv(loc, v);
}

void Shader::VertexAttrib4N(const string& name, const GLuint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4Nuiv(loc, v);
}

void Shader::VertexAttrib4N(const string& name, const GLushort *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttrib4Nusv(loc, v);
}

void Shader::VertexAttribI1(const string& name, GLint x)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI1i(loc, x);
}

void Shader::VertexAttribI1(const string& name, GLuint x)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI1ui(loc, x);
}

void Shader::VertexAttribI1(const string& name, const GLint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI1iv(loc, v);
}

void Shader::VertexAttribI1(const string& name, const GLuint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI1uiv(loc, v);
}

void Shader::VertexAttribI2(const string& name, GLint x, GLint y)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI2i(loc, x, y);
}

void Shader::VertexAttribI2(const string& name, GLuint x, GLuint y)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI2ui(loc, x, y);
}

void Shader::VertexAttribI2(const string& name, const GLint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI2iv(loc, v);
}

void Shader::VertexAttribI2(const string& name, const GLuint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI2uiv(loc, v);
}

void Shader::VertexAttribI3(const string& name, GLint x, GLint y, GLint z)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI3i(loc, x, y, z);
}

void Shader::VertexAttribI3(const string& name, GLuint x, GLuint y, GLuint z)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI3ui(loc, x, y, z);
}

void Shader::VertexAttribI3(const string& name, const GLint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI3iv(loc, v);
}

void Shader::VertexAttribI3(const string& name, const GLuint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI3uiv(loc, v);
}

void Shader::VertexAttribI4(const string& name, GLint x, GLint y, GLint z, GLint w)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI4i(loc, x, y, z, w);
}

void Shader::VertexAttribI4(const string& name, GLuint x, GLuint y, GLuint z, GLuint w)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI4ui(loc, x, y, z, w);
}

void Shader::VertexAttribI4(const string& name, const GLbyte *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI4bv(loc, v);
}

void Shader::VertexAttribI4(const string& name, const GLint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI4iv(loc, v);
}

void Shader::VertexAttribI4(const string& name, const GLshort *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI4sv(loc, v);
}

void Shader::VertexAttribI4(const string& name, const GLubyte *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI4ubv(loc, v);
}

void Shader::VertexAttribI4(const string& name, const GLuint *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI4uiv(loc, v);
}

void Shader::VertexAttribI4(const string& name, const GLushort *v)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribI4usv(loc, v);
}

void Shader::VertexAttribIPointer(const string& name, GLint size, GLenum type, GLsizei stride, const GLvoid *pointer)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribIPointer(loc, size, type, stride, pointer);
}

void Shader::VertexAttribP1(const string& name, GLenum type, GLboolean normalized, GLuint value)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribP1ui(loc, type, normalized, value);
}

void Shader::VertexAttribP1(const string& name, GLenum type, GLboolean normalized, const GLuint *value)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribP1uiv(loc, type, normalized, value);
}

void Shader::VertexAttribP2(const string& name, GLenum type, GLboolean normalized, GLuint value)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribP2ui(loc, type, normalized, value);
}

void Shader::VertexAttribP2(const string& name, GLenum type, GLboolean normalized, const GLuint *value)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribP2uiv(loc, type, normalized, value);
}

void Shader::VertexAttribP3(const string& name, GLenum type, GLboolean normalized, GLuint value)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribP3ui(loc, type, normalized, value);
}

void Shader::VertexAttribP3(const string& name, GLenum type, GLboolean normalized, const GLuint *value)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribP3uiv(loc, type, normalized, value);
}

void Shader::VertexAttribP4(const string& name, GLenum type, GLboolean normalized, GLuint value)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribP4ui(loc, type, normalized, value);
}

void Shader::VertexAttribP4(const string& name, GLenum type, GLboolean normalized, const GLuint *value)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribP4uiv(loc, type, normalized, value);
}

void Shader::VertexAttribPointer(const string& name, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer)
{
	GLint loc = getAttribLocation(name);
	if (loc < 0)
		return;
	glVertexAttribPointer(loc, size, type, normalized, stride, pointer);
}

//