#include "DS_Camera.h"
#include <Math/DS_Math.h>

using namespace DS;

Camera::Camera(float fov, float aspect, float nearPlane, float farPlane)
{
	SetFOV(fov);
	SetAspectRatio(aspect);
	SetClipPlanes(nearPlane, farPlane);
}

Camera::~Camera()
{
}

void Camera::Update()
{
	if (m_NeedsUpdate)
	{
		m_ProjectionMatrix = perspective(m_FOV, m_AspectRatio, m_Near, m_Far);
		m_NeedsUpdate = false;
	}
}

void Camera::SetFOV(float fov)
{
	if (!Math::FloatEquals(fov, m_FOV))
	{
		m_FOV = fov;
		m_NeedsUpdate = true;
	}
}

void Camera::SetAspectRatio(float aspect)
{
	if (!Math::FloatEquals(aspect, m_AspectRatio))
	{
		m_AspectRatio = aspect;
		m_NeedsUpdate = true;
	}
}

void Camera::SetClipPlanes(float nearPlane, float farPlane)
{
	if (!Math::FloatEquals(nearPlane, m_Near))
	{
		m_Near = nearPlane;
		m_NeedsUpdate = true;
	}

	if (!Math::FloatEquals(farPlane, m_Far))
	{
		m_Far = farPlane;
		m_NeedsUpdate = true;
	}
}