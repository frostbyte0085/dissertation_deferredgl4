#pragma once

#include <DS_Include.h>

namespace DS
{
	class Sprite
	{
	public:
		Sprite();
		~Sprite();

		void Render(float x1, float y1, float x2, float y2);

	private:

	};
}