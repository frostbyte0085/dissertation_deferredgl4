#pragma once

#include <DS_Include.h>

namespace DS
{
	class Camera
	{
	public:
		Camera(float fov, float aspect, float nearPlane, float farPlane);
		~Camera();

		void SetFOV(float f);
		float GetFOV() const { return m_FOV; }

		void SetAspectRatio(float aspect);
		float GetAspectRatio() const { return m_AspectRatio; }

		void SetClipPlanes(float nearPlane, float farPlane);
		float GetNearClipPlane() const { return m_Near; }
		float GetFarClipPlane() const { return m_Far; }

		void Update();
		const mat4& GetProjectionMatrix() const { return m_ProjectionMatrix; }

	private:
		bool m_NeedsUpdate;
		float m_AspectRatio;
		float m_FOV;
		float m_Near;
		float m_Far;

		mat4 m_ProjectionMatrix;
	};
}