#include "DS_Renderer.h"
#include <Math/DS_Math.h>
#include <Math/DS_Sphere.h>
#include <Core/DS_Resources.h>
#include "DS_Shader.h"
#include "DS_Texture.h"
#include "DS_Camera.h"
#include <Scene/DS_Scene.h>
#include <Scene/DS_Object.h>
#include "DS_FSQ.h"
#include "DS_RenderTarget.h"
#include "DS_Light.h"

using namespace DS;

Renderer::LightGeometry::LightGeometry() : m_SphereVBO(0), m_SphereIBO(0), m_SphereVAO(0), m_ConeVBO(0), m_ConeIBO(0), m_ConeVAO(0),
		m_PointLightShader(NULL), m_DirectionalLightShader(NULL), m_SpotLightShader(NULL), m_SphereIndexCount(0), m_PoissonDisk(NULL)
{
}

void Renderer::LightGeometry::Create()
{
	m_PointLightShader = gResources->LoadShader("Data/Shaders/Deferred/GBufferPointLight.shader");
	m_DirectionalLightShader = gResources->LoadShader("Data/Shaders/Deferred/GBufferDirLight.shader");
	m_SpotLightShader = gResources->LoadShader("Data/Shaders/Deferred/GBufferSpotLight.shader");

	// the poisson disk used for shadow map filtering
	m_PoissonDisk = gResources->LoadTexture("Data/Textures/poisson.png", false, false);

	createSphere();
	createCone();

	m_Quad = new FSQ();
}

void Renderer::LightGeometry::Destroy()
{
	if (m_SphereVAO > 0)
		glDeleteVertexArrays(1, &m_SphereVAO);

	if (m_SphereVBO > 0)
		glDeleteBuffers(1, &m_SphereVBO);

	if (m_SphereIBO > 0)
		glDeleteBuffers(1, &m_SphereIBO);

	if (m_ConeVAO > 0)
		glDeleteVertexArrays(1, &m_ConeVAO);

	if (m_ConeVBO > 0)
		glDeleteBuffers(1, &m_ConeVBO);

	if (m_ConeIBO > 0)
		glDeleteBuffers(1, &m_ConeIBO);
}

void Renderer::LightGeometry::createSphere()
{
	vector<vec3> vertices;
	vector<unsigned short> indices;
	
	int rings = 14;
	int sectors = 14;

	float segmentAngle = 2 * PI / rings;

	float radius = 1.0f / cosf(segmentAngle / 2.0f);
	m_PointRadiusOnSegment = radius;

	float const R = 1.0f/(float)(rings-1);
    float const S = 1.0f/(float)(sectors-1);
    int r, s;

    for(r = 0; r < rings; r++)
	{
		for(s = 0; s < sectors; s++)
		{
			float const y = radius * sinf( -PI/2 + PI * r * R );
			float const x = radius * cosf(2*PI * s * S) * sin( PI * r * R );
			float const z = radius * sinf(2*PI * s * S) * sin( PI * r * R );

			vertices.push_back(vec3(x,y,z));
		}
    }

    for(r = 0; r < rings; r++)
	{
		for(s = 0; s < sectors; s++)
		{
			indices.push_back( s + sectors * r);
			indices.push_back( s + sectors + sectors * r);
			indices.push_back( s + 1 + sectors * r);
			

			indices.push_back( s + sectors + 1 + sectors * r);
			indices.push_back( s + 1 + sectors * r);
			indices.push_back( s + sectors + sectors * r);
			
		}
    }

	glGenVertexArrays(1, &m_SphereVAO);
	glBindVertexArray(m_SphereVAO);

	glGenBuffers(1, &m_SphereVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_SphereVBO);
	glBufferData (GL_ARRAY_BUFFER, vertices.size() * 3 * sizeof(float), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &m_SphereIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_SphereIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	m_SphereIndexCount = indices.size();

	vertices.clear();
	indices.clear();
}

void Renderer::LightGeometry::createCone()
{
	// make a down facing cone.
	vector<vec3> vertices;
	vector<unsigned short> indices;

	int segments = 16;
	float segmentAngle = 2.0f * PI / segments;

	float height = 1.0f;
	float radius = 1.0f / cosf(segmentAngle / 2.0f);
	m_SpotRadiusOnSegment = radius;

	for (int i=0; i<segments; i++)
	{
		float angle = (float)i * 2.0f * PI / (float)segments;
		float x = cosf(angle) * radius;
		float y = sinf(angle) * radius;
		
		vertices.push_back(vec3(x,y,-height));
	}
	
	vertices.push_back(vec3(0,0,-height));
	int bottomVertexIndex = vertices.size()-1;

	vertices.push_back(vec3(0,0,0));
	int topVertexIndex = vertices.size()-1;
	
	for (int i=0; i<segments; i++)
	{
		
		indices.push_back(i);
		indices.push_back(bottomVertexIndex);
		indices.push_back((i+1) % segments);
	}
	

	for (int i=0; i<segments; i++)
	{
		indices.push_back(topVertexIndex);
		indices.push_back(i);
		indices.push_back((i+1) % segments);
	}
	

	glGenVertexArrays(1, &m_ConeVAO);
	glBindVertexArray(m_ConeVAO);

	glGenBuffers(1, &m_ConeVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_ConeVBO);
	glBufferData (GL_ARRAY_BUFFER, vertices.size() * 3 * sizeof(float), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &m_ConeIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ConeIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	m_ConeIndexCount = indices.size();

	vertices.clear();
	indices.clear();
}

void Renderer::LightGeometry::RenderSphere(const S_PointRendererData& pointData)
{
	Object *c = gScene->GetCamera();
	
	glEnable(GL_CULL_FACE);

	if (pointData.m_CameraToLight <= pointData.m_Radius * m_PointRadiusOnSegment)
		glCullFace(GL_FRONT);
	else
		glCullFace(GL_BACK);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_CUBE_MAP, pointData.m_Cookie);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, pointData.m_DPFront);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, pointData.m_DPBack);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, m_PoissonDisk->GetID());

	m_PointLightShader->Set();

	m_PointLightShader->Uniform1("hasShadows", pointData.m_DPFront && (RenderSettings::ShadowsEnabled == true) ? 1 : 0);
	m_PointLightShader->Uniform1("hasFilter", (RenderSettings::ShadowFilter == true) ? 1 : 0);
	m_PointLightShader->Uniform2("shadowMapSize", 1, value_ptr(pointData.m_ShadowMapSize));
	m_PointLightShader->Uniform1("depthBias", pointData.m_DepthBias);
	m_PointLightShader->Uniform1("hasCookie", pointData.m_Cookie > 0 ? 1 : 0);
	m_PointLightShader->UniformMatrix4("world", 1, GL_FALSE, value_ptr(pointData.m_WorldMatrix));
	m_PointLightShader->Uniform3("color", 1, value_ptr(pointData.m_Color));
	m_PointLightShader->Uniform1("intensity", pointData.m_Intensity);
	m_PointLightShader->Uniform3("position", 1, value_ptr(pointData.m_Position));
	m_PointLightShader->Uniform1("radius", pointData.m_Radius);

	m_PointLightShader->UniformMatrix4("lightView", 1, GL_FALSE, value_ptr(pointData.m_ViewMatrix));

	m_PointLightShader->Uniform1("lightFarPlane", pointData.m_Radius);
	m_PointLightShader->Uniform1("lightNearPlane", pointData.m_NearClipPlane);

	m_PointLightShader->Uniform1("linearDepthConstant", 1.0f / (pointData.m_Radius - pointData.m_NearClipPlane));

	glBindVertexArray(m_SphereVAO);
	
	glBindBuffer(GL_ARRAY_BUFFER, m_SphereVBO);

	m_PointLightShader->VertexAttribPointer ("in_Position", 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	m_PointLightShader->EnableVertexAttribArray("in_Position");

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_SphereIBO);
	glDrawElements(GL_TRIANGLES, m_SphereIndexCount, GL_UNSIGNED_SHORT, 0);

	m_PointLightShader->DisableVertexAttribArray("in_Position");
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	Shader::Unset();

	glCullFace(GL_BACK);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void Renderer::LightGeometry::RenderCone(const S_SpotRendererData& spotData)
{
	Object *c = gScene->GetCamera();
	
	glEnable(GL_CULL_FACE);
	//Check if SL is within the LightAngle, if so then draw the BackFaces, if not then draw the FrontFaces
	if (spotData.m_LdotD >= spotData.m_AngleCos)
		glCullFace(GL_FRONT);
	else
		glCullFace(GL_BACK);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, spotData.m_Cookie);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, spotData.m_ShadowMap);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_PoissonDisk->GetID());

	m_SpotLightShader->Set();
	m_SpotLightShader->Uniform1("hasShadows", spotData.m_ShadowMap && (RenderSettings::ShadowsEnabled == true) ? 1 : 0);
	m_SpotLightShader->Uniform1("hasFilter", (RenderSettings::ShadowFilter == true) ? 1 : 0);
	m_SpotLightShader->Uniform2("shadowMapSize", 1, value_ptr(spotData.m_ShadowMapSize));
	m_SpotLightShader->Uniform1("depthBias", spotData.m_DepthBias);
	m_SpotLightShader->Uniform1("hasCookie", spotData.m_Cookie > 0 ? 1 : 0);
	m_SpotLightShader->UniformMatrix4("world", 1, GL_FALSE, value_ptr(spotData.m_ConeWorldMatrix));
	m_SpotLightShader->Uniform3("color", 1, value_ptr(spotData.m_Color));
	m_SpotLightShader->Uniform1("intensity", spotData.m_Intensity);
	m_SpotLightShader->Uniform3("position", 1, value_ptr(spotData.m_Position));
	m_SpotLightShader->Uniform3("direction", 1, value_ptr(spotData.m_Direction));
	m_SpotLightShader->Uniform1("angleCos", spotData.m_AngleDiv2Cos);
	m_SpotLightShader->Uniform1("height", spotData.m_Height);
	m_SpotLightShader->Uniform1("radius", spotData.m_Radius);

	m_SpotLightShader->Uniform1("lightFarPlane", spotData.m_Height);
	m_SpotLightShader->Uniform1("lightNearPlane", spotData.m_NearClipPlane);

	m_SpotLightShader->Uniform1("linearDepthConstant", 1.0f / (spotData.m_Height - spotData.m_NearClipPlane));
	m_SpotLightShader->UniformMatrix4("lightMatrix", 1, GL_FALSE, value_ptr(spotData.m_ProjectionMatrix * spotData.m_ViewMatrix));

	glBindVertexArray(m_ConeVAO);
	
	glBindBuffer(GL_ARRAY_BUFFER, m_ConeVBO);

	m_SpotLightShader->VertexAttribPointer ("in_Position", 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	m_SpotLightShader->EnableVertexAttribArray("in_Position");

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ConeIBO);
	glDrawElements(GL_TRIANGLES, m_ConeIndexCount, GL_UNSIGNED_SHORT, 0);

	m_SpotLightShader->DisableVertexAttribArray("in_Position");
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	Shader::Unset();

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);

	glCullFace(GL_BACK);
}

void Renderer::LightGeometry::RenderFSQ(const S_DirectionalRendererData& dirData)
{
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, dirData.m_Cookie);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, dirData.m_ShadowMap);

	m_DirectionalLightShader->Set();
	m_DirectionalLightShader->Uniform1("hasShadows", dirData.m_ShadowMap && (RenderSettings::ShadowsEnabled == true) ? 1 : 0);
	m_DirectionalLightShader->Uniform2("shadowMapSize", 1, value_ptr(dirData.m_ShadowMapSize));
	m_DirectionalLightShader->Uniform1("depthBias", dirData.m_DepthBias);
	m_DirectionalLightShader->Uniform1("hasCookie", dirData.m_Cookie > 0 ? 1 : 0);
	m_DirectionalLightShader->Uniform3("color", 1, value_ptr(dirData.m_Color));
	m_DirectionalLightShader->Uniform1("intensity", dirData.m_Intensity);
	m_DirectionalLightShader->Uniform3("direction", 1, value_ptr(dirData.m_Direction));
	m_DirectionalLightShader->Uniform2("projectionScale", 1, value_ptr(dirData.m_CookieProjectionScale));

	/*
	Object *camObj = gScene->GetCamera();
	Camera *cam = camObj->GetCamera();

	// make the directional light matrix
	const vector<vec3>& corners = gScene->GetFrustumCorners();

	Sphere frustumSphere = Sphere(corners);
	vec3 center = frustumSphere.GetCenter();
	float radius = frustumSphere.GetRadius();

	vec3 lightViewPos = center - direction * radius;
	vec3 lightViewTarget = center;

	mat4 projection = ortho<float>(-1,1,-1,1,-1,1);
	mat4 view = lookAt(vec3(0), direction, vec3(0,1,0));
	//mat4 view = lookAt(lightViewPos, lightViewTarget, vec3(0,1,0));
	//mat4 projection = ortho<float>(-radius,radius,-radius,radius,-radius,radius);
	*/
	m_DirectionalLightShader->UniformMatrix4("cookieLightMatrix", 1, GL_FALSE, value_ptr(dirData.m_CookieProjectionMatrix * dirData.m_CookieViewMatrix));
	m_DirectionalLightShader->UniformMatrix4("shadowLightMatrix", 1, GL_FALSE, value_ptr(dirData.m_ShadowProjectionMatrix * dirData.m_ShadowViewMatrix));

	m_Quad->Render();

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer::LightGeometry::PrepareDirectionalLight()
{
	Object* cameraObj = gScene->GetCamera();
	mat4 view = inverse(cameraObj->GetWorldMatrix());
	mat4 projection = cameraObj->GetCamera()->GetProjectionMatrix();

	m_DirectionalLightShader->Set();
	m_DirectionalLightShader->Uniform3("cameraPosition", 1, value_ptr(cameraObj->GetTranslation()));
	m_DirectionalLightShader->UniformMatrix4("inverseProjection", 1, GL_FALSE, value_ptr(inverse(projection)));
	m_DirectionalLightShader->Uniform2("gbufferSize", 1, value_ptr(vec2(gRenderer->GetGBuffer()->GetWidth(), gRenderer->GetGBuffer()->GetHeight())));
	m_DirectionalLightShader->UniformMatrix4("invView", 1, GL_FALSE, value_ptr(inverse(view)));
	m_DirectionalLightShader->Uniform1("camFarClipPlane", cameraObj->GetCamera()->GetFarClipPlane());

	Shader::Unset();
}

void Renderer::LightGeometry::PreparePointLight()
{
	Object* cameraObj = gScene->GetCamera();
	mat4 view = inverse(cameraObj->GetWorldMatrix());
	mat4 projection = cameraObj->GetCamera()->GetProjectionMatrix();

	m_PointLightShader->Set();
	m_PointLightShader->UniformMatrix4("projection", 1, GL_FALSE, value_ptr(projection));
	m_PointLightShader->UniformMatrix4("view", 1, GL_FALSE, value_ptr(view));
	m_PointLightShader->Uniform3("cameraPosition", 1, value_ptr(cameraObj->GetTranslation()));
	m_PointLightShader->UniformMatrix4("invView", 1, GL_FALSE, value_ptr(inverse(view)));
	m_PointLightShader->Uniform1("camFarClipPlane", cameraObj->GetCamera()->GetFarClipPlane());
	m_PointLightShader->Uniform2("gbufferSize", 1, value_ptr(vec2(gRenderer->GetGBuffer()->GetWidth(), gRenderer->GetGBuffer()->GetHeight())));

	Shader::Unset();
}

void Renderer::LightGeometry::PrepareSpotLight()
{
	Object* cameraObj = gScene->GetCamera();
	mat4 view = inverse(cameraObj->GetWorldMatrix());
	mat4 projection = cameraObj->GetCamera()->GetProjectionMatrix();

	m_SpotLightShader->Set();
	m_SpotLightShader->UniformMatrix4("projection", 1, GL_FALSE, value_ptr(projection));
	m_SpotLightShader->UniformMatrix4("view", 1, GL_FALSE, value_ptr(view));
	m_SpotLightShader->Uniform3("cameraPosition", 1, value_ptr(cameraObj->GetTranslation()));
	m_SpotLightShader->UniformMatrix4("invView", 1, GL_FALSE, value_ptr(inverse(view)));
	m_SpotLightShader->Uniform1("camFarClipPlane", cameraObj->GetCamera()->GetFarClipPlane());
	m_SpotLightShader->Uniform2("gbufferSize", 1, value_ptr(vec2(gRenderer->GetGBuffer()->GetWidth(), gRenderer->GetGBuffer()->GetHeight())));

	Shader::Unset();
}