#pragma once

#include <DS_Include.h>

namespace DS
{
	class Sphere;
	class Object;
	class RenderTarget;
	class Shader;
	class FSQ;
	class Texture;

	enum eDeferredCompositionStep
	{
		DEFERRED_ALBEDO,
		DEFERRED_NORMALS,
		DEFERRED_DEPTH,
		DEFERRED_LIGHTMAP,
		DEFERRED_INVALID
	};

	struct S_SpotRendererData;
	struct S_DirectionalRendererData;
	struct S_PointRendererData;

	class RenderSettings
	{
	public:
		static vec3 AmbientColor;
		static bool ShadowsEnabled;
		static bool ShadowFilter;
		static bool SMAA;
		static bool SSAO;
	};

	class Renderer
	{
	public:
		Renderer();
		~Renderer();

		void Initialise();
		void Render();
		void Composite();

		RenderTarget* GetGBuffer() const { return m_GBuffer; }
		RenderTarget* GetLightmap() const { return m_Lightmap; }

	private:
		class LightGeometry
		{
		public:
			LightGeometry();

			void Create();
			void Destroy();

			void PrepareDirectionalLight();
			void PreparePointLight();
			void PrepareSpotLight();


			void RenderSphere(const S_PointRendererData& pointData);
			void RenderFSQ(const S_DirectionalRendererData& dirData);
			void RenderCone(const S_SpotRendererData& spotData);

		private:
			void createSphere();
			void createCone();

			GLuint m_SphereVBO;
			GLuint m_SphereIBO;
			GLuint m_SphereVAO;
			unsigned short m_SphereIndexCount;
			
			GLuint m_ConeVBO;
			GLuint m_ConeIBO;
			GLuint m_ConeVAO;
			unsigned short m_ConeIndexCount;

			Shader *m_PointLightShader;
			Shader *m_DirectionalLightShader;
			Shader *m_SpotLightShader;

			Texture *m_PoissonDisk;

			float m_PointRadiusOnSegment;
			float m_SpotRadiusOnSegment;


			FSQ *m_Quad;
		};
		LightGeometry m_LightGeometry;

		void fetch();

		bool m_ShadowsEnabled;

		// the renderables for this frame
		vector<Object*> m_Renderables;
		vector<Object*> m_Lights;
		
		// gbuffer rendertargets
		RenderTarget *m_GBuffer;

		// lightmap
		RenderTarget *m_Lightmap;

		// gbuffer shaders
		Shader *m_ClearGBuffer;
		Shader *m_RenderGBuffer;
		Shader *m_ShadowMapPass;
		Shader *m_ShadowMapPassDP;

		FSQ *m_Quad;

		void deferred_ClearGBufferPass();
		void deferred_GBufferPass();
		void deferred_LightingPass();

		void shadowMapPass();
		void render_ShadowMap(const mat4& view, const mat4& projection, float nearClipPlane, float farClipPlane);
		void render_ShadowMapDP(const mat4& view, float nearClipPlane, float farClipPlane, float zDir, const Sphere& lightBoundingSphere);

		vec3 m_AmbientColor;
	};

	extern Renderer *gRenderer;
}