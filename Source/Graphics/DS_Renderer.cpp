#include "DS_Renderer.h"
#include <Scene/DS_Scene.h>
#include <Scene/DS_Object.h>
#include "DS_RenderTarget.h"
#include "DS_Shader.h"
#include "DS_Texture.h"
#include <Core/DS_Resources.h>
#include "DS_FSQ.h"
#include "DS_Mesh.h"
#include "DS_Camera.h"
#include <Math/DS_Math.h>
#include <Math/DS_Sphere.h>
#include <Math/DS_AABB.h>

namespace DS
{
	Renderer *gRenderer = NULL;
}

using namespace DS;

vec3 RenderSettings::AmbientColor = vec3(0.25f);
bool RenderSettings::ShadowsEnabled = true;
bool RenderSettings::ShadowFilter = true;
bool RenderSettings::SSAO = true;
bool RenderSettings::SMAA = false;

Renderer::Renderer() : m_GBuffer(NULL), m_ClearGBuffer(NULL), m_RenderGBuffer(NULL),m_Quad(NULL), m_Lightmap(NULL), m_ShadowMapPass(NULL)
{
}

Renderer::~Renderer()
{
	m_LightGeometry.Destroy();

	if (m_GBuffer)
		delete m_GBuffer;
	m_GBuffer = NULL;

	if (m_Lightmap)
		delete m_Lightmap;
	m_Lightmap = NULL;

	if (m_Quad)
		delete m_Quad;
	m_Quad = NULL;
}

void Renderer::Initialise()
{
	// create our GBuffer rendertargets
	int vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);

	// make sure the gbuffer is all 64bits, otherwise some drivers won't allow it
	m_GBuffer = new RenderTarget(vp[2], vp[3]);
	m_GBuffer->BeginAdd();
	m_GBuffer->AddComponent("albedo", GL_RGBA16F, GL_RGBA, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_COLOR_ATTACHMENT0);
	m_GBuffer->AddComponent("normal", GL_RGBA16F, GL_RGBA, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_COLOR_ATTACHMENT1);
	m_GBuffer->AddComponent("depth", GL_LUMINANCE32F_ARB, GL_RED, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_COLOR_ATTACHMENT2);
	m_GBuffer->EndAdd();

	// create the lightmap rendertarget
	m_Lightmap = new RenderTarget(vp[2], vp[3]);
	m_Lightmap->BeginAdd(false); // no depth needed for this one
	m_Lightmap->AddComponent("lightmap", GL_RGBA8, GL_RGBA, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_COLOR_ATTACHMENT0);
	m_Lightmap->EndAdd();

	// create the various GBuffer shaders
	m_ClearGBuffer = gResources->LoadShader("Data/Shaders/Deferred/ClearGBuffer.shader");
	m_RenderGBuffer = gResources->LoadShader("Data/Shaders/Deferred/RenderGBuffer.shader");

	// and the shadow map pass
	m_ShadowMapPass = gResources->LoadShader("Data/Shaders/ShadowMapPass.shader");
	m_ShadowMapPassDP = gResources->LoadShader("Data/Shaders/ShadowMapPass_DP.shader");

	// the fullscreen rendering quad
	m_Quad = new FSQ();

	// create the light geometries
	m_LightGeometry.Create();
}

void Renderer::deferred_LightingPass()
{
	const vector<Plane>& frustum = gScene->GetFrustumPlanes();

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	m_Lightmap->Set();
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_GBuffer->GetTexture("normal"));

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_GBuffer->GetTexture("depth"));
	
	// set some common uniforms for all three shaders:
	m_LightGeometry.PrepareDirectionalLight();
	m_LightGeometry.PrepareSpotLight();
	m_LightGeometry.PreparePointLight();
	//

	vector<Object*>::const_iterator it;
	for (it=m_Lights.begin(); it!=m_Lights.end(); it++)
	{
		Object* obj = *it;
		Light* light = obj->GetLight();
		GLuint lightCookie = light->GetCookie() != NULL ? light->GetCookie()->GetID() : 0;

		const S_PointRendererData &pointData = light->GetPointRendererData();

		switch (light->GetType())
		{
			default:break;

		case LIGHT_DIRECTIONAL:

			m_LightGeometry.RenderFSQ(light->GetDirectionalRendererData());
			break;

		case LIGHT_POINT:
			if (Math::SphereInFrustum (pointData.m_BoundingSphere, frustum))
			{
				m_LightGeometry.RenderSphere(light->GetPointRendererData());
			}
			break;

		case LIGHT_SPOT:
			m_LightGeometry.RenderCone(light->GetSpotRendererData());
			break;
		}
	}

	RenderTarget::Resolve();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);

	glDisable(GL_BLEND);
}

void Renderer::deferred_GBufferPass()
{
	m_GBuffer->Set();
	m_RenderGBuffer->Set();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	Camera* camera = gScene->GetCamera()->GetCamera();

	m_RenderGBuffer->Uniform1("nearClipPlane", camera->GetNearClipPlane());
	m_RenderGBuffer->Uniform1("farClipPlane", camera->GetFarClipPlane());

	mat4 projectionMatrix = camera->GetProjectionMatrix();
	mat4 viewMatrix = inverse(gScene->GetCamera()->GetWorldMatrix());

	m_RenderGBuffer->UniformMatrix4("projection", 1, GL_FALSE, value_ptr(projectionMatrix));
	m_RenderGBuffer->UniformMatrix4("view", 1, GL_FALSE, value_ptr(viewMatrix));

	vector<Object*>::const_iterator it;
	for (it=m_Renderables.begin(); it!=m_Renderables.end(); it++)
	{
		Object *obj = *it;
		Mesh *mesh = obj->GetMesh();
		
		glBindVertexArray (mesh->m_VBO.m_VAO);

		glBindBuffer(GL_ARRAY_BUFFER, mesh->m_VBO.m_MainVBO);

		m_RenderGBuffer->VertexAttribPointer ("in_Position", 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)0);
		m_RenderGBuffer->EnableVertexAttribArray("in_Position");

		m_RenderGBuffer->VertexAttribPointer("in_TexCoord", 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)(sizeof(GLfloat)*3));
		m_RenderGBuffer->EnableVertexAttribArray("in_TexCoord");

		m_RenderGBuffer->VertexAttribPointer("in_Normal", 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)(sizeof(GLfloat)*6));
		m_RenderGBuffer->EnableVertexAttribArray("in_Normal");

		mat4 worldMatrix = obj->GetWorldMatrix();
		mat3 normalMatrix = transpose(inverse(mat3(worldMatrix)));

		m_RenderGBuffer->UniformMatrix4("world", 1, GL_FALSE, value_ptr(worldMatrix));
		m_RenderGBuffer->UniformMatrix3("normalMatrix", 1, GL_FALSE, value_ptr(normalMatrix));

		
		for (int i = 0; i<(int)mesh->m_MeshParts.size(); i++)
		{
			S_MeshPart *part = mesh->m_MeshParts[i];
			int materialId = part->m_MaterialID;

			// check if this part is culled
			AABB worldBox = AABB(*part->m_LocalAABB);
			worldBox.Transform(worldMatrix);
			if (!Math::AABBInFrustum(worldBox, gScene->GetFrustumPlanes()))
			{
				continue;
			}

			S_MaterialData *material = mesh->m_Materials[materialId];
			m_RenderGBuffer->Uniform1("hasDiffuseTexture", material->m_Diffuse > 0 ? 1 : 0);
			m_RenderGBuffer->Uniform1("hasNormalTexture", material->m_Normals > 0 ? 1 : 0);
			m_RenderGBuffer->Uniform1("hasSpecularTexture", material->m_Specular > 0 ? 1 : 0);
			
			// according to our shader layout bindings:
			// sampler 0 is diffuse
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, material->m_Diffuse);
			// sampler 1 is normal
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, material->m_Normals);
			// sampler 2 is specular
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, material->m_Specular);
			
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, part->m_VBO);

			glDrawElements (GL_TRIANGLES, part->m_FaceCount * 3, GL_UNSIGNED_INT, 0);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}

		m_RenderGBuffer->DisableVertexAttribArray("in_Position");
		m_RenderGBuffer->DisableVertexAttribArray("in_TexCoord");
		m_RenderGBuffer->DisableVertexAttribArray("in_Normal");
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture( GL_TEXTURE_2D, 0 );

		glActiveTexture(GL_TEXTURE1);
		glBindTexture( GL_TEXTURE_2D, 0 );

		glActiveTexture(GL_TEXTURE2);
		glBindTexture( GL_TEXTURE_2D, 0 );

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	Shader::Unset();

	RenderTarget::Resolve();
}

void Renderer::shadowMapPass()
{
	int vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	Camera *cam = gScene->GetCamera()->GetCamera();

	vector<Object*>::const_iterator it;
	for (it=m_Lights.begin(); it!=m_Lights.end(); it++)
	{
		Object* lightObj = *it;
		Light* light = (*it)->GetLight();
		if (!light->GetCastShadows())
			continue;

		const S_DirectionalRendererData &dirData = light->GetDirectionalRendererData();
		const S_SpotRendererData &spotData = light->GetSpotRendererData();
		const S_PointRendererData &pointData = light->GetPointRendererData();
		
		switch (light->GetType())
		{
			default:break;

		case LIGHT_DIRECTIONAL:
			m_ShadowMapPass->Set();
			m_ShadowMapPass->Uniform1("isDirectional", 1);
			m_ShadowMapPass->Uniform1("depthModulation", 1.0f / (dirData.m_FarPlane - dirData.m_NearPlane));
			
			light->GetShadowMaps()[0]->Set();
			glViewport(0,0,light->GetShadowMapSize(),light->GetShadowMapSize());
		
			render_ShadowMap(dirData.m_ShadowViewMatrix, dirData.m_ShadowProjectionMatrix, 0.1f, 10000.0f );
			break;

		case LIGHT_SPOT:
			m_ShadowMapPass->Set();
			m_ShadowMapPass->Uniform1("isDirectional", 0);
			m_ShadowMapPass->Uniform1("depthModulation", spotData.m_Height - spotData.m_NearClipPlane);
			m_ShadowMapPass->Uniform3("lightPosition", 1, value_ptr(spotData.m_Position));

			light->GetShadowMaps()[0]->Set();
			glViewport(0,0,light->GetShadowMapSize(),light->GetShadowMapSize());
			
			render_ShadowMap(spotData.m_ViewMatrix, spotData.m_ProjectionMatrix, spotData.m_NearClipPlane, spotData.m_Height);
			break;

		case LIGHT_POINT:			
			//glEnable(GL_CULL_FACE);
			glDisable(GL_CULL_FACE);

			m_ShadowMapPassDP->Set();
			m_ShadowMapPassDP->Uniform1("depthModulation", (pointData.m_Radius - pointData.m_NearClipPlane));
			m_ShadowMapPassDP->Uniform3("lightPosition", 1, value_ptr(pointData.m_Position));

			// front dual paraboloid
			light->GetShadowMaps()[0]->Set();
			
			glCullFace(GL_FRONT);

			glViewport(0,0,pointData.m_ShadowMapSize.x, pointData.m_ShadowMapSize.y);
			render_ShadowMapDP(pointData.m_ViewMatrix, pointData.m_NearClipPlane, cam->GetFarClipPlane(), 1.0f, pointData.m_BoundingSphere);

			// back dual paraboloid
			light->GetShadowMaps()[1]->Set();

			glCullFace(GL_BACK);

			glViewport(0,0,pointData.m_ShadowMapSize.x, pointData.m_ShadowMapSize.y);
			render_ShadowMapDP(pointData.m_ViewMatrix, pointData.m_NearClipPlane, cam->GetFarClipPlane(), -1.0f, pointData.m_BoundingSphere);
			
			break;
		}

		light->PostUpdate();
	}	
	
	glViewport(vp[0], vp[1], vp[2], vp[3]);

	glCullFace(GL_BACK);
	Shader::Unset();
}

void Renderer::render_ShadowMap(const mat4& view, const mat4& projection, float nearClipPlane, float farClipPlane)
{
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_ShadowMapPass->UniformMatrix4("view", 1, GL_FALSE, value_ptr(view));
	m_ShadowMapPass->UniformMatrix4("projection", 1, GL_FALSE, value_ptr(projection));

	vector<Plane> frustum = Math::CalculateViewFrustum(view, projection);

	vector<Object*>::const_iterator it;
	for (it=m_Renderables.begin(); it!=m_Renderables.end(); it++)
	{
		Object* obj = *it;
		if (!obj || !obj->GetMesh())
			continue;

		Mesh *mesh = obj->GetMesh();
		mat4 worldMatrix = obj->GetWorldMatrix();

		glBindVertexArray (mesh->m_VBO.m_VAO);

		glBindBuffer(GL_ARRAY_BUFFER, mesh->m_VBO.m_MainVBO);

		m_ShadowMapPass->VertexAttribPointer ("in_Position", 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)0);
		m_ShadowMapPass->EnableVertexAttribArray("in_Position");

		m_ShadowMapPass->UniformMatrix4("world", 1, GL_FALSE, value_ptr(worldMatrix));

		for (int i = 0; i<(int)mesh->m_MeshParts.size(); i++)
		{
			S_MeshPart *part = mesh->m_MeshParts[i];
			// check if this part is culled
			AABB worldBox = AABB(*part->m_LocalAABB);
			worldBox.Transform(worldMatrix);
			if (!Math::AABBInFrustum(worldBox, frustum))
			{
				continue;
			}
			
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, part->m_VBO);

			glDrawElements (GL_TRIANGLES, part->m_FaceCount * 3, GL_UNSIGNED_INT, 0);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}

		m_ShadowMapPass->DisableVertexAttribArray("in_Position");
	}
	RenderTarget::Resolve();
}

void Renderer::render_ShadowMapDP(const mat4& view, float nearClipPlane, float farClipPlane, float zDir, const Sphere& lightBoundingSphere)
{
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_ShadowMapPassDP->UniformMatrix4("view", 1, GL_FALSE, value_ptr(view));
	m_ShadowMapPassDP->Uniform1("nearClipPlane", nearClipPlane);
	m_ShadowMapPassDP->Uniform1("farClipPlane", farClipPlane);
	m_ShadowMapPassDP->Uniform1("direction", zDir);

	mat4 projection = perspective(180.0f, 1.0f, nearClipPlane, farClipPlane);
	vector<Plane> frustum = Math::CalculateViewFrustum(view, projection);

	vector<Object*>::const_iterator it;
	for (it=m_Renderables.begin(); it!=m_Renderables.end(); it++)
	{
		Object* obj = *it;
		if (!obj || !obj->GetMesh())
			continue;

		Mesh *mesh = obj->GetMesh();
		mat4 worldMatrix = obj->GetWorldMatrix();

		glBindVertexArray (mesh->m_VBO.m_VAO);

		glBindBuffer(GL_ARRAY_BUFFER, mesh->m_VBO.m_MainVBO);

		m_ShadowMapPassDP->VertexAttribPointer ("in_Position", 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)0);
		m_ShadowMapPassDP->EnableVertexAttribArray("in_Position");

		m_ShadowMapPassDP->UniformMatrix4("world", 1, GL_FALSE, value_ptr(worldMatrix));

		for (int i = 0; i<(int)mesh->m_MeshParts.size(); i++)
		{
			S_MeshPart *part = mesh->m_MeshParts[i];
			// check if this part is culled
			AABB worldBox = AABB(*part->m_LocalAABB);
			worldBox.Transform(worldMatrix);

			// if the object is not in the frustum or not reached by the light, ignore it
			if (!Math::AABBInFrustum(worldBox, frustum) || !Math::Intersects(worldBox, lightBoundingSphere))
			{
				continue;
			}
			
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, part->m_VBO);

			glDrawElements (GL_TRIANGLES, part->m_FaceCount * 3, GL_UNSIGNED_INT, 0);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}

		m_ShadowMapPassDP->DisableVertexAttribArray("in_Position");
	}
	RenderTarget::Resolve();
}

void Renderer::Render()
{
	fetch();

	if (RenderSettings::ShadowsEnabled)
	{
		shadowMapPass();
	}

	deferred_ClearGBufferPass();
	deferred_GBufferPass();
	deferred_LightingPass();
}

void Renderer::deferred_ClearGBufferPass()
{
	m_GBuffer->Set();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_ClearGBuffer->Set();
	m_Quad->Render();

	RenderTarget::Resolve();
}

void Renderer::fetch()
{
	m_Renderables.clear();
	m_Lights.clear();

	Scene::ObjectMap::const_iterator it;
	for (it = gScene->m_Objects.begin(); it != gScene->m_Objects.end(); it++)
	{
		Object *obj = it->second;
		if (!obj)
			continue;
		
		if (obj->GetMesh())
		{
			m_Renderables.push_back(it->second);
		}

		if (obj->GetLight())
		{
			m_Lights.push_back(it->second);
		}
	}
}