#pragma once

#include <DS_Include.h>

namespace DS
{
	class Shader;

	class FSQ
	{
	public:
		FSQ();
		~FSQ();

		void Render(bool restoreShader=true);

	private:
		void initialise();

		GLuint m_VAO;
		GLuint m_VBO;
		GLuint m_IBO;
	};
}