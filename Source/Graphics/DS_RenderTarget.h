#pragma once

#include <DS_Include.h>

namespace DS
{
	class RenderTarget
	{
	public:
		RenderTarget(int width, int height);
		~RenderTarget();

		void Clear();
		void BeginAdd(bool useDepth=true, bool cube=false);
		void AddComponent (const string& name, GLint internalFormat, GLenum sourceFormat, GLenum type, GLfloat textureAddress, GLint textureFilter, GLenum attachment);
		void EndAdd();
		void Set();
		void Set(const string& component);
		void Set(const string& component, int index);

		GLuint GetTexture(const string& component) const;
		GLenum GetAttachment(const string& component) const;
		GLuint GetAttachmentIndex(const string& component) const;

		bool IsCube() const { return m_IsCube; }
		int GetWidth() const { return m_Width; }
		int GetHeight() const { return m_Height; }

		static void Resolve();

	private:
		struct AttachmentData
		{
			AttachmentData()
			{
				m_TextureID = 0;
				m_Buffer = GL_INVALID_ENUM;
				m_Index = -1;
			}
			AttachmentData(GLuint texture, GLenum buffer, GLuint index)
			{
				m_TextureID = texture;
				m_Index = index;
				m_Buffer = buffer;
			}

			GLuint m_TextureID;
			GLenum m_Buffer;
			GLuint m_Index;
		};

		GLenum m_TextureType;

		bool m_IsCube;
		bool m_Adding;
		bool m_UseDepth;

		GLuint m_FBO;
		GLuint m_RBO;
		
		vector<GLenum> m_Buffers;
		vector<GLuint> m_TexIDs;

		typedef map<string, AttachmentData> DataMap;
		DataMap m_Data;
		
		int m_Width;
		int m_Height;

		int m_CurrentIndex;
	};
}