#include "DS_Light.h"
#include <Scene/DS_Object.h>
#include "DS_Texture.h"
#include <Scene/DS_Scene.h>
#include <Math/DS_Math.h>
#include "DS_RenderTarget.h"
#include "DS_Camera.h"
#include <Math/DS_Sphere.h>
#include <Math/DS_AABB.h>

using namespace DS;

Light::Light(eLightType type, const vec3& color, float intensity, bool castShadows, int shadowMapSize) : m_SpotAngle(0), m_SpotHeight(0), m_Cookie(NULL)
{
	m_Type = type;
	m_Intensity = intensity;
	m_Color = color;
	m_ShadowMapSize = shadowMapSize;
	SetCastShadows(castShadows, shadowMapSize);

	m_DepthBias = 0.00001f;

	m_NearClipPlane = 0.01f;

	memset(&m_SpotData, 0, sizeof(m_SpotData));
	memset(&m_PointData, 0, sizeof(m_PointData));
	memset(&m_DirectionalData, 0, sizeof(m_DirectionalData));
}

Light::~Light()
{
	for (int i=0; i<(int)m_ShadowMaps.size(); i++)
	{
		if (m_ShadowMaps[i] != NULL)
			delete m_ShadowMaps[i];
	}
	m_ShadowMaps.clear();
}

void Light::SetCastShadows(bool shadows, int shadowMapSize)
{
	m_CastShadows = shadows;
	if (m_CastShadows)
	{
		if (m_Type == LIGHT_POINT)
		{
			
			RenderTarget *dpFront = new RenderTarget(shadowMapSize, shadowMapSize);

			dpFront->BeginAdd();
				dpFront->AddComponent("depth", GL_LUMINANCE32F_ARB, GL_RED, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_COLOR_ATTACHMENT0);
			dpFront->EndAdd();

			RenderTarget *dpBack = new RenderTarget(shadowMapSize, shadowMapSize);

			dpBack->BeginAdd();
				dpBack->AddComponent("depth", GL_LUMINANCE32F_ARB, GL_RED, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_COLOR_ATTACHMENT0);
			dpBack->EndAdd();

			m_ShadowMaps.push_back(dpFront);
			m_ShadowMaps.push_back(dpBack);
		}
		else
		{
			
			RenderTarget *shadowMap = new RenderTarget(shadowMapSize, shadowMapSize);

			shadowMap->BeginAdd();
				shadowMap->AddComponent("depth", GL_LUMINANCE32F_ARB, GL_RED, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_COLOR_ATTACHMENT0);
			shadowMap->EndAdd();

			m_ShadowMaps.push_back(shadowMap);
		}
	}
	else
	{
		
		for (int i=0; i<(int)m_ShadowMaps.size(); i++)
		{
			if (m_ShadowMaps[i] != NULL)
				delete m_ShadowMaps[i];
		}
		m_ShadowMaps.clear();
	}
}

void Light::Update(Object *parentObject)
{
	if (m_Type == LIGHT_DIRECTIONAL)
		updateDirectional(parentObject);
	else if (m_Type == LIGHT_POINT)
		updatePoint(parentObject);
	else if (m_Type == LIGHT_SPOT)
		updateSpot(parentObject);
}

void Light::updateDirectional(Object *parentObject)
{
	Texture* cookie = GetCookie();

	m_DirectionalData.m_Cookie = cookie ? cookie->GetID() : 0;
	m_DirectionalData.m_Color = GetColor();
	m_DirectionalData.m_Intensity = GetIntensity();
	m_DirectionalData.m_CookieProjectionScale = GetCookieScaling();
	m_DirectionalData.m_DepthBias = GetDepthBias();
	m_DirectionalData.m_ShadowMapSize = vec2(GetShadowMapSize(), GetShadowMapSize());
	m_DirectionalData.m_Direction = parentObject->GetOrientation() * vec3(0,0,-1);

	m_DirectionalData.m_CookieProjectionMatrix = ortho<float>(-1,1,-1,1,-1,1);
	m_DirectionalData.m_CookieViewMatrix = lookAt(vec3(0), m_DirectionalData.m_Direction, vec3(0,1,0));

	vector<vec3>& corners = gScene->GetFrustumCorners();
	mat3 lightRotation = mat3(lookAt(vec3(0), m_DirectionalData.m_Direction, vec3(0,1,0)));

	for (int i=0; i<(int)corners.size(); i++)
	{
		corners[i] = lightRotation * corners[i];
	}
	
	Sphere frustumSphere = Sphere(corners);
	vec3 center = frustumSphere.GetCenter();
	float radius = frustumSphere.GetRadius();

	vec3 lightViewPos = center - m_DirectionalData.m_Direction * radius;
	vec3 lightViewTarget = center;

	m_DirectionalData.m_NearPlane = -radius;
	m_DirectionalData.m_FarPlane = radius;

	m_DirectionalData.m_ShadowProjectionMatrix = ortho<float>(-radius,radius,-radius,radius,-radius,radius);
	m_DirectionalData.m_ShadowViewMatrix = lookAt(lightViewPos, lightViewTarget, vec3(0,1,0));
}

void Light::updatePoint(Object *parentObject)
{
	Object *cameraObject = gScene->GetCamera();
	Texture* cookie = GetCookie();
		
	m_PointData.m_Cookie = cookie ? cookie->GetID() : 0;
	m_PointData.m_Intensity = GetIntensity();
	m_PointData.m_Color = GetColor();
	m_PointData.m_Position = parentObject->GetTranslation();
	m_PointData.m_Orientation = parentObject->GetOrientation();
	m_PointData.m_Radius = m_PointRadius;

	vec3 difference = cameraObject->GetTranslation() - parentObject->GetTranslation();
	m_PointData.m_CameraToLight = sqrtf(dot(difference, difference));

	m_PointData.m_WorldMatrix = Math::ComposeMatrix(m_PointData.m_Position, vec3(m_PointData.m_Radius), m_PointData.m_Orientation);
	
	m_PointData.m_ViewMatrix = lookAt(m_PointData.m_Position, m_PointData.m_Position - vec3(0,0,1), vec3(0,1,0));

	m_PointData.m_ShadowMapSize = vec2(GetShadowMapSize(), GetShadowMapSize());
	m_PointData.m_DepthBias = m_DepthBias;
	m_PointData.m_NearClipPlane = m_NearClipPlane;

	m_PointData.m_BoundingSphere = Sphere(m_PointData.m_Position, m_PointData.m_Radius);
}


void Light::updateSpot(Object *parentObject)
{
	Object *cameraObject = gScene->GetCamera();
	Texture* cookie = GetCookie();

	vec3 L = normalize(cameraObject->GetTranslation() - parentObject->GetTranslation());

	m_SpotData.m_Height = GetSpotHeight();
	m_SpotData.m_Cookie = cookie ? cookie->GetID() : 0;
	m_SpotData.m_Intensity = GetIntensity();
	m_SpotData.m_Color = GetColor();
	m_SpotData.m_Position = parentObject->GetTranslation();
	m_SpotData.m_Direction = normalize(parentObject->GetOrientation() * vec3(0,0,-1));
	m_SpotData.m_Radius = GetSpotHeight() * sinf(GetSpotAngle());
	m_SpotData.m_AngleCos = cosf(GetSpotAngle());
	m_SpotData.m_AngleDiv2Cos = cosf(GetSpotAngle()/2);
	m_SpotData.m_LdotD = fabsf(dot(-L, m_SpotData.m_Direction));

	m_SpotData.m_ConeWorldMatrix = Math::ComposeMatrix(m_SpotData.m_Position,
									vec3(m_SpotData.m_Radius, m_SpotData.m_Height, m_SpotData.m_Radius),
									parentObject->GetOrientation());
	m_SpotData.m_LightWorldMatrix = Math::ComposeMatrix(m_SpotData.m_Position,
									vec3(1,1,1),
									parentObject->GetOrientation());

	m_SpotData.m_ProjectionMatrix = perspective(RAD2DEG(GetSpotAngle()), 1.0f, m_NearClipPlane, m_SpotData.m_Height);
	m_SpotData.m_ViewMatrix = inverse(m_SpotData.m_LightWorldMatrix);
	m_SpotData.m_ShadowMapSize = vec2(GetShadowMapSize(), GetShadowMapSize());
	m_SpotData.m_DepthBias = m_DepthBias;
	m_SpotData.m_NearClipPlane = m_NearClipPlane;
}

void Light::PostUpdate()
{
	if (m_Type == LIGHT_DIRECTIONAL)
		postUpdateDirectional();
	else if (m_Type == LIGHT_POINT)
		postUpdatePoint();
	else if (m_Type == LIGHT_SPOT)
		postUpdateSpot();
}

void Light::postUpdateSpot()
{
	m_SpotData.m_ShadowMap = m_ShadowMaps.size() > 0 ? m_ShadowMaps[0]->GetTexture("depth") : 0;
}

void Light::postUpdatePoint()
{
	if (m_ShadowMaps.size() > 0)
	{
		m_PointData.m_DPFront = m_ShadowMaps[0]->GetTexture("depth");
		m_PointData.m_DPBack = m_ShadowMaps[1]->GetTexture("depth");
	}
	else
	{
		m_PointData.m_DPBack = m_PointData.m_DPFront = 0;
	}
}

void Light::postUpdateDirectional()
{
	m_DirectionalData.m_ShadowMap = m_ShadowMaps.size() > 0 ? m_ShadowMaps[0]->GetTexture("depth") : 0;
}