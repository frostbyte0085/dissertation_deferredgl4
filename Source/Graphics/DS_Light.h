#pragma once

#include <DS_Include.h>
#include <Math/DS_Sphere.h>

namespace DS
{
	enum eLightType
	{
		LIGHT_POINT,
		LIGHT_DIRECTIONAL,
		LIGHT_SPOT
	};

	class Texture;

	struct S_SpotRendererData
	{
		mat4 m_ProjectionMatrix;
		mat4 m_ViewMatrix;

		float m_AngleDiv2Cos;
		float m_AngleCos;
		float m_Radius;
		float m_Height;

		mat4 m_ConeWorldMatrix;
		mat4 m_LightWorldMatrix;

		float m_LdotD;

		float m_Intensity;
		vec3 m_Color;
		GLuint m_Cookie;

		vec3 m_Position;
		vec3 m_Direction;

		vec2 m_ShadowMapSize;
		GLuint m_ShadowMap;
		float m_DepthBias;
		float m_NearClipPlane;
	};
	
	
	//DP
	struct S_PointRendererData
	{
		mat4 m_ViewMatrix;

		quat m_Orientation;
		vec3 m_Position;
		float m_Radius;
		float m_CameraToLight;

		mat4 m_WorldMatrix;

		GLuint m_Cookie;
		vec3 m_Color;
		float m_Intensity;
		float m_DepthBias;
		
		GLuint m_DPFront;
		GLuint m_DPBack;
		
		vec2 m_ShadowMapSize;
		float m_NearClipPlane;

		Sphere m_BoundingSphere;
	};
	

	struct S_DirectionalRendererData
	{
		mat4 m_CookieViewMatrix;
		mat4 m_CookieProjectionMatrix;

		mat4 m_ShadowViewMatrix;
		mat4 m_ShadowProjectionMatrix;

		GLuint m_ShadowMap;
		GLuint m_Cookie;
		vec3 m_Color;
		vec3 m_Direction;
		float m_Intensity;
		float m_DepthBias;
		float m_FarPlane;
		float m_NearPlane;

		vec2 m_ShadowMapSize;
		vec2 m_CookieProjectionScale;
	};

	class Object;
	class RenderTarget;

	class Light
	{
	public:
		Light(eLightType type, const vec3& color, float intensity, bool castShadows, int shadowMapSize);
		~Light();

		void SetColor(const vec3& color) { m_Color = color; }
		const vec3& GetColor() const { return m_Color; }

		void SetCastShadows(bool shadows, int shadowMapSize);
		int GetShadowMapSize() const { return m_ShadowMapSize; }
		bool GetCastShadows() const { return m_CastShadows; }
		const vector<RenderTarget*>& GetShadowMaps() const { return m_ShadowMaps; }

		void SetDepthBias(float bias) { m_DepthBias = bias; }
		float GetDepthBias() const { return m_DepthBias; }

		void SetIntensity(float intensity) { m_Intensity = intensity; }
		float GetIntensity() const { return m_Intensity; }

		void SetCookie(const Texture* cookie) { m_Cookie = const_cast<Texture*>(cookie); }
		Texture* GetCookie() const { return m_Cookie; }

		void SetCookieScaling(const vec2& scale) { m_CookieScaling = scale; }
		const vec2& GetCookieScaling() const { return m_CookieScaling; }

		void SetSpotAngle(float angles) { m_SpotAngle = angles; }
		float GetSpotAngle() const { return m_SpotAngle; }

		void SetSpotHeight(float height) { m_SpotHeight = height; }
		float GetSpotHeight() const { return m_SpotHeight; }

		void SetPointRadius(float radius) { m_PointRadius = radius; }
		float GetPointRadius() const { return m_PointRadius; }

		eLightType GetType() const { return m_Type; }

		const S_SpotRendererData& GetSpotRendererData() const { return m_SpotData; }
		const S_DirectionalRendererData& GetDirectionalRendererData() const { return m_DirectionalData; }
		const S_PointRendererData& GetPointRendererData() const { return m_PointData; }

		void Update(Object* parentObject);
		void PostUpdate();
		
	private:
		float m_PointRadius;
		float m_NearClipPlane;
		Texture *m_Cookie;
		bool m_CastShadows;
		vec3 m_Color;
		float m_Intensity;
		float m_SpotAngle;
		float m_SpotHeight;
		eLightType m_Type;
		float m_DepthBias;
		vec2 m_CookieScaling;

		vector<RenderTarget*> m_ShadowMaps;
		int m_ShadowMapSize;

		void updateDirectional(Object *parentObject);
		void updateSpot(Object *parentObject);
		void updatePoint(Object *parentObject);

		void postUpdateDirectional();
		void postUpdateSpot();
		void postUpdatePoint();

		S_SpotRendererData m_SpotData;
		S_DirectionalRendererData m_DirectionalData;
		S_PointRendererData m_PointData;
	};
}