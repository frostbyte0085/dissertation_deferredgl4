#pragma once

#include <DS_Include.h>

namespace DS
{
	class Resources;

	class Shader
	{
		friend class Resources;
	public:
		static void Unset();
		static Shader*  GetBoundShader() { return s_BoundShader; }

		void Set();
		void Destroy();
		void Recompile();
		void Relink();

		void BindFragDataLocation(const string& name, GLuint loc);
		void BindAttribLocation(const string& name, GLuint loc);

		void EnableVertexAttribArray(const string& name);
		void DisableVertexAttribArray(const string& name);

		void Uniform1(const string& name, GLfloat v0);
		void Uniform1(const string& name, GLint v0);
		void Uniform1(const string& name, GLuint v0);
		void Uniform1(const string& name, GLsizei count, const GLfloat *value);
		void Uniform1(const string& name, GLsizei count, const GLint *value);
		void Uniform1(const string& name, GLsizei count, const GLuint *value);

		void Uniform2(const string& name, GLfloat v0, GLfloat v1);
		void Uniform2(const string& name, GLint v0, GLint v1);
		void Uniform2(const string& name, GLuint v0, GLuint v1);
		void Uniform2(const string& name, GLsizei count, const GLfloat *value);
		void Uniform2(const string& name, GLsizei count, const GLint *value);
		void Uniform2(const string& name, GLsizei count, const GLuint *value);
	
		void Uniform3(const string& name, GLfloat v0, GLfloat v1, GLfloat v2);
		void Uniform3(const string& name, GLint v0, GLint v1, GLint v2);
		void Uniform3(const string& name, GLuint v0, GLuint v1, GLuint v2);
		void Uniform3(const string& name, GLsizei count, const GLfloat *value);
		void Uniform3(const string& name, GLsizei count, const GLint *value);
		void Uniform3(const string& name, GLsizei count, const GLuint *value);
	
		void Uniform4(const string& name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
		void Uniform4(const string& name, GLint v0, GLint v1, GLint v2, GLint v3);
		void Uniform4(const string& name, GLuint v0, GLuint v1, GLuint v2, GLuint v3);
		void Uniform4(const string& name, GLsizei count, const GLfloat *value);
		void Uniform4(const string& name, GLsizei count, const GLint *value);
		void Uniform4(const string& name, GLsizei count, const GLuint *value);
	
		void UniformMatrix2(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value);
		void UniformMatrix2x3(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value);
		void UniformMatrix2x4(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value);
	
		void UniformMatrix3(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value);
		void UniformMatrix3x2(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value);
		void UniformMatrix3x4(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value);
	
		void UniformMatrix4(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value);
		void UniformMatrix4x2(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value);
		void UniformMatrix4x3(const string& name, GLsizei count, GLboolean transpose, const GLfloat *value);

		void VertexAttrib1(const string& name, GLdouble x);
		void VertexAttrib1(const string& name, GLfloat x);
		void VertexAttrib1(const string& name, GLshort x);
		void VertexAttrib1(const string& name, const GLdouble *v);
		void VertexAttrib1(const string& name, const GLfloat *v);
		void VertexAttrib1(const string& name, const GLshort *v);
	
		void VertexAttrib2(const string& name, GLdouble x, GLdouble y);
		void VertexAttrib2(const string& name, GLfloat x, GLfloat y);
		void VertexAttrib2(const string& name, GLshort x, GLshort y);
		void VertexAttrib2(const string& name, const GLdouble *v);
		void VertexAttrib2(const string& name, const GLfloat *v);
		void VertexAttrib2(const string& name, const GLshort *v);
	
		void VertexAttrib3(const string& name, GLdouble x, GLdouble y, GLdouble z);
		void VertexAttrib3(const string& name, GLfloat x, GLfloat y, GLfloat z);
		void VertexAttrib3(const string& name, GLshort x, GLshort y, GLshort z);
		void VertexAttrib3(const string& name, const GLdouble *v);
		void VertexAttrib3(const string& name, const GLfloat *v);
		void VertexAttrib3(const string& name, const GLshort *v);
	
		void VertexAttrib4(const string& name, GLdouble x, GLdouble y, GLdouble z, GLdouble w);
		void VertexAttrib4(const string& name, GLfloat x, GLfloat y, GLfloat z, GLfloat w);
		void VertexAttrib4(const string& name, GLshort x, GLshort y, GLshort z, GLshort w);
		void VertexAttrib4(const string& name, const GLbyte *v);
		void VertexAttrib4(const string& name, const GLdouble *v);
		void VertexAttrib4(const string& name, const GLfloat *v);
		void VertexAttrib4(const string& name, const GLint *v);
		void VertexAttrib4(const string& name, const GLshort *v);
		void VertexAttrib4(const string& name, const GLubyte *v);
		void VertexAttrib4(const string& name, const GLuint *v);
		void VertexAttrib4(const string& name, const GLushort *v);
	
		void VertexAttrib4N(const string& name, GLubyte x, GLubyte y, GLubyte z, GLubyte w);
		void VertexAttrib4N(const string& name, const GLbyte *v);
		void VertexAttrib4N(const string& name, const GLint *v);
		void VertexAttrib4N(const string& name, const GLshort *v);
		void VertexAttrib4N(const string& name, const GLubyte *v);
		void VertexAttrib4N(const string& name, const GLuint *v);
		void VertexAttrib4N(const string& name, const GLushort *v);
	
		void VertexAttribI1(const string& name, GLint x);
		void VertexAttribI1(const string& name, GLuint x);
		void VertexAttribI1(const string& name, const GLint *v);
		void VertexAttribI1(const string& name, const GLuint *v);
	
		void VertexAttribI2(const string& name, GLint x, GLint y);
		void VertexAttribI2(const string& name, GLuint x, GLuint y);
		void VertexAttribI2(const string& name, const GLint *v);
		void VertexAttribI2(const string& name, const GLuint *v);
	
		void VertexAttribI3(const string& name, GLint x, GLint y, GLint z);
		void VertexAttribI3(const string& name, GLuint x, GLuint y, GLuint z);
		void VertexAttribI3(const string& name, const GLint *v);
		void VertexAttribI3(const string& name, const GLuint *v);
	
		void VertexAttribI4(const string& name, GLint x, GLint y, GLint z, GLint w);
		void VertexAttribI4(const string& name, GLuint x, GLuint y, GLuint z, GLuint w);
		void VertexAttribI4(const string& name, const GLbyte *v);
		void VertexAttribI4(const string& name, const GLint *v);
		void VertexAttribI4(const string& name, const GLshort *v);
		void VertexAttribI4(const string& name, const GLubyte *v);
		void VertexAttribI4(const string& name, const GLuint *v);
		void VertexAttribI4(const string& name, const GLushort *v);
		void VertexAttribIPointer(const string& name, GLint size, GLenum type, GLsizei stride, const GLvoid *pointer);
	
		void VertexAttribP1(const string& name, GLenum type, GLboolean normalized, GLuint value);
		void VertexAttribP1(const string& name, GLenum type, GLboolean normalized, const GLuint *value);
		void VertexAttribP2(const string& name, GLenum type, GLboolean normalized, GLuint value);
		void VertexAttribP2(const string& name, GLenum type, GLboolean normalized, const GLuint *value);
		void VertexAttribP3(const string& name, GLenum type, GLboolean normalized, GLuint value);
		void VertexAttribP3(const string& name, GLenum type, GLboolean normalized, const GLuint *value);
		void VertexAttribP4(const string& name, GLenum type, GLboolean normalized, GLuint value);
		void VertexAttribP4(const string& name, GLenum type, GLboolean normalized, const GLuint *value);
		void VertexAttribPointer(const string& name, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer);

	private:
		Shader(const string& filename);
		~Shader();

		string m_Filename;

		void create();

		bool createVertexShader (const string &programSource);
		bool createFragmentShader (const string &programSource);
		bool createTesselationControlShader (const string &programSource);
		bool createTesselationEvalShader(const string &programSource);
		bool createGeometryShader(const string &programSource);
		
		bool link();

		bool validateShader (GLuint shader);
		bool validateProgram (GLuint program);
		unsigned int m_ShaderID, m_VP, m_FP, m_TSCP, m_TSEP, m_GP;

		GLint getAttribLocation(const string& name);
		GLint getUniformLocation(const string& name);

		static Shader *s_BoundShader;
	};
}