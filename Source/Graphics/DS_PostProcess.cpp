#include "DS_PostProcess.h"
#include "DS_RenderTarget.h"
#include "DS_Shader.h"
#include "DS_Texture.h"
#include "DS_FSQ.h"
#include <Tools/DS_StrOp.h>

#include "PostProcessEffects/DS_EmptyEffect.h"

namespace DS
{
	PostProcess *gPostProcess = NULL;
}

using namespace DS;


PostProcess::PostProcess() : m_RT1(NULL), m_RT2(NULL), m_EmptyEffect(NULL), m_Quad(NULL)
{

}

PostProcess::~PostProcess()
{
	RenderTarget::Resolve();

	if (m_RT1)
		delete m_RT1;

	if (m_RT2)
		delete m_RT2;

	for (int i=0; i<(int)m_Effects.size(); i++)
	{
		if (m_Effects[i])
			delete m_Effects[i];
	}
	m_Effects.clear();

	if (m_EmptyEffect)
		delete m_EmptyEffect;
}

void PostProcess::Initialise()
{
	int vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);

	m_RT1 = new RenderTarget(vp[2], vp[3]);
	m_RT2 = new RenderTarget(vp[2], vp[3]);

	m_RT1->BeginAdd(false);
		m_RT1->AddComponent("main", GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_COLOR_ATTACHMENT0);
	m_RT1->EndAdd();

	m_RT2->BeginAdd(false);
		m_RT2->AddComponent("main", GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_COLOR_ATTACHMENT0);
	m_RT2->EndAdd();

	m_Quad = new FSQ();

	m_EmptyEffect = new EmptyEffect();
}

void PostProcess::Begin()
{
	if (m_Effects.size() == 0)
		return;

	m_RT1->Set();
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT);
}

void PostProcess::Process()
{
	glDisable(GL_DEPTH_TEST);

	for (int i=0; i<(int)m_Effects.size(); i++)
	{
		GLuint textureRenderTo = 0;

		RenderTarget::Resolve();
		if (i % 2 == 0)
			textureRenderTo = m_RT1->GetTexture("main");
		else
			textureRenderTo = m_RT2->GetTexture("main");

		if (i == (int)m_Effects.size()-1)
		{
			m_Effects[i]->m_SceneRenderTarget = NULL;
		}
		else
		{
			if (i % 2 == 0)
			{
				m_Effects[i]->m_SceneRenderTarget = m_RT2;
			}
			else
			{
				m_Effects[i]->m_SceneRenderTarget = m_RT1;
			}
		}

		if (m_Effects[i]->m_SceneRenderTarget)
			m_Effects[i]->m_SceneRenderTarget->Set();

		glClearColor(0,0,0,0);
		glClear(GL_COLOR_BUFFER_BIT);

		if (m_Effects[i]->IsEnabled())
		{
			m_Effects[i]->Render(textureRenderTo);
		}
		else
		{
			m_EmptyEffect->Render(textureRenderTo);
		}
	}

	glEnable(GL_DEPTH_TEST);
}

void PostProcess::RenderQuad()
{
	m_Quad->Render();
}

void PostProcess::AddEffect (PostProcessEffect *effect)
{
	if (GetEffect(effect->GetName()))
		return;

	m_Effects.push_back(effect);
}

PostProcessEffect* PostProcess::GetEffect(const string &name) const
{
	string lowered = StrOp::ToLower(name);

	for (int i=0; i<(int)m_Effects.size(); i++)
	{
		if (m_Effects[i]->GetName() == lowered)
			return m_Effects[i];
	}
	return NULL;
}