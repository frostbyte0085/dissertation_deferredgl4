#include "DS_Mesh.h"
#include <Core/DS_Resources.h>
#include "DS_Texture.h"
#include <Tools/DS_Exception.h>
#include "DS_Shader.h"
#include <Core/DS_Resources.h>
#include <Math/DS_Math.h>
#include <Math/DS_Sphere.h>
#include <Math/DS_AABB.h>

using namespace DS;

Mesh::Mesh(const string& filename) : m_Filename("")
{
	m_Filename = filename;
	memset(&m_Counts, 0, sizeof(m_Counts));
	memset(&m_VBO, 0, sizeof(m_VBO));
}

Mesh::~Mesh()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glDeleteVertexArrays( 1, &m_VBO.m_VAO );
	glDeleteBuffers( 1, &m_VBO.m_MainVBO );

	for( GLuint i = 0; i < m_MeshParts.size(); i++ ) {
		glDeleteBuffers( 1, &m_MeshParts[i]->m_VBO );
	}	

	vector<S_MeshPart*>::iterator itMeshes;
	for (itMeshes = m_MeshParts.begin(); itMeshes != m_MeshParts.end(); itMeshes++)
	{
		if (*itMeshes != NULL)
			delete *itMeshes;
	}

	vector<S_MaterialData*>::iterator itMats;
	for (itMats = m_Materials.begin(); itMats != m_Materials.end(); itMats++)
	{
		if (*itMats != NULL)
			delete *itMats;
	}

	m_MeshParts.clear();
	m_Materials.clear();
	
}

void Mesh::create()
{
	FILE* file = fopen(m_Filename.c_str(), "rt");
	if (file != NULL)
	{
		readOBJ (file);
		createVBO();

		fclose(file);
		file = NULL;
	}
}

void Mesh::readOBJ(FILE *file)
{
	S_MeshPart* newMesh = new S_MeshPart;
	//to keep track of the current material.
	int CurrentMatID =0;

	char buffer[255];
	while(!feof(file)) {
		//Get first char
		char fChar = fgetc(file);
		//Switch case for each attrib, vert = default, normal = n, texture coord = t, face = f
		switch ( fChar ) {
		case 'm': { 
			readTextures( file, m_Filename.c_str() ); //Use the .mtl to read textures
			//Create meshes according to materials (mesh n = material n)
			for(int i=0; i < (int)m_Materials.size(); i++)
			{
				m_MeshParts.push_back(new S_MeshPart());
				m_MeshParts[i]->m_MaterialID = CurrentMatID;
			}
			break;
		}
		case'v':{
			//Get second char
			char sChar = fgetc(file);
			switch( sChar ) {
				case 'n': {
							vec3 tempNorms;
							fgets( buffer, 255, file );
							sscanf( buffer, "%f %f %f", &tempNorms[0], &tempNorms[1], &tempNorms[2]);
							m_Normals.push_back( tempNorms );
							m_Counts.m_Norms++;
							break;
						}

				case 't': {
							vec3 tempTex;
							fgets( buffer, 255, file );
							sscanf( buffer, "%f %f %f", &tempTex[0], &tempTex[1], &tempTex[2]);
							m_TexCoords.push_back( tempTex );
							m_Counts.m_TexCoords++;
							break;
						}

				default: {
							vec3 tempVerts;
							fgets( buffer, 255, file );
							sscanf( buffer, "%f %f %f", &tempVerts[0], &tempVerts[1], &tempVerts[2]);

							m_Vertices.push_back( tempVerts );	
							m_Counts.m_Verts++;
							break;
						    }
					}
			break;
				    }
		case 'u': {
			char sChar = fgetc( file );
			if( sChar == 's' ) {
				fgets( buffer, 255, file ); 
				char material[255]; sscanf( buffer, "%*s %s", material );
				
				m_MeshParts[CurrentMatID]->m_MaterialID = CurrentMatID;
				CurrentMatID = getMaterial( material); //Find material

				if( CurrentMatID < 0 || CurrentMatID >=  (int)m_Materials.size() ){
					throw Exception("bad material id");
				}
				break;
			}
			else { fgets( buffer, 255, file ); break; }
		}
		case 'f': {
			m_MeshParts[CurrentMatID]->m_FaceCount++;
			m_Counts.m_Faces++;

			readFace( file, m_MeshParts[CurrentMatID] );
			break;
		}
		case '#':
		case 's':
		case ' ':	fgets( buffer, 255, file ); break;
		}
		
	}

	fclose(file);

	//Find number of individual floats
	m_VBO.m_PointCount = m_VBO.m_Interleaved.size()*3;
	makeBoundingVolumes();
	//createTangents();
}

void Mesh::readTextures( FILE* file, const char* location )
{
	//Get texture file location
	char buffer[255];
	char textureLocation[255];
	fgets( buffer, 255, file );
	sscanf( buffer, "%*s %s", &textureLocation );

	//Get real file path (remove any '/')
	string filePath( location );
	int dashPos = (int)filePath.find_last_of('/');
	string actualPath = filePath.substr( 0, dashPos );
	actualPath.insert( actualPath.size(), "/" );
	string matLocation( actualPath );
	matLocation.insert( matLocation.size(), textureLocation );

	//Open file
	FILE* texFile = fopen( matLocation.c_str(), "rt" );

	//Open file and read header including textures
	if(!texFile){
		cout << "Could not open texture file: " << matLocation.c_str() << endl; 
		system("pause");
	} 
    else {
	    S_MaterialData* newMat;  //Create temporary material to store info
	    int currentMat = 0;	//Used for the material ID
	    bool isNewMat = false;	//Triggered when new material found
	    char texBuffer[255];
	    while(!feof(texFile)) {

		    char fChar = fgetc( texFile ); //Get first char
		    switch( fChar ) {
		    case 'n': {
					    //Search for 'new'
					    char sChar = fgetc( texFile );
					    char tChar = fgetc( texFile );
					    if( sChar == 'e' && tChar == 'w' ) {
						    fgets( texBuffer, 255, texFile );
						    char matName[255];
						    sscanf( texBuffer, "%*s %s", &matName ); 
						    if( currentMat >= 1 )
								m_Materials.push_back( newMat );

						    newMat = new S_MaterialData; //Create new material using same pointer
						    newMat->m_Name = matName;
						    newMat->m_MaterialID = currentMat;
						    isNewMat = true;
						    currentMat ++; //Increase material ID
						    break;
					    }
				      }
		    case 'K': {
					    //Gather all ambient, diffuse and specular details
					    char sChar = fgetc( texFile );
					    if( sChar == 'a' ) {
						    fgets( texBuffer, 255, texFile );
							sscanf( texBuffer, "%f %f %f", &newMat->m_AmbientColor[0], &newMat->m_AmbientColor[1], &newMat->m_AmbientColor[1] );
					    } 
					    else if( sChar == 'd' ) {
						    fgets( texBuffer, 255, texFile );
						    sscanf( texBuffer, "%f %f %f", &newMat->m_DiffuseColor[0], &newMat->m_DiffuseColor[1], &newMat->m_DiffuseColor[2] ); 
					    }
					    else if( sChar == 's' ) {
						    fgets( texBuffer, 255, texFile );
						    sscanf( texBuffer, "%f %f %f", &newMat->m_SpecularColor[0], &newMat->m_SpecularColor[1], &newMat->m_SpecularColor[2] ); 
					    }
					    else if( sChar == 'e' ) fgets( texBuffer, 255, texFile ); //Skip to next line
					    break;
				      }
		    case 'm': {
					    
					    fgets( texBuffer, 255, texFile );
					    char buffer[255]; //To store the found texture name
					    char sChar = fgetc( texFile ); //To store the second char
					    sscanf( texBuffer, "%*c%*c%*c%*c%c%*s", &sChar );
					    if( sChar == 'd' ) {
						    string diffPath( actualPath ); //To store the full diffuse texture path
						    sscanf( texBuffer, "%*s %s", buffer );
							Texture *diffuseTex = gResources->LoadTexture ((diffPath.insert( diffPath.length(), buffer )) );
							if (diffuseTex != NULL)
								newMat->m_Diffuse = diffuseTex->GetID();
					    }
					    else if( sChar == 's' ) {
						    string specPath( actualPath ); //To store the full spec texture path
						    sscanf( texBuffer, "%*s %s", buffer );
							Texture *specularTex = gResources->LoadTexture ((specPath.insert( specPath.length(), buffer )) );
						    if (specularTex != NULL)
								newMat->m_Specular = specularTex->GetID();
					    }
					    else if( sChar == 'u' ) {
						    string bumpPath( actualPath ); //To store the full bump texture path
						    sscanf( texBuffer, "%*s %s", buffer );
						    Texture *normalsTex = gResources->LoadTexture ((bumpPath.insert( bumpPath.length(), buffer )) );
							if (normalsTex != NULL)
								newMat->m_Normals = normalsTex->GetID();
					    }
					    break;
				      }  
		    case '#':	
		    case 'N':	
		    case 'T':	
		    case 'i':
		    case 'd':	fgets( texBuffer, 255, texFile ); //Skip to next line
		    }
	    }
	    m_Materials.push_back( newMat );//Push material on to vector if end of file is found

	    fclose( texFile );
    }
}

void Mesh::readFace(FILE *file, S_MeshPart *mesh)
{
	char buffer[255];
	fgets( buffer, 255, file );
	S_Face face; //Create a whole face
	S_FaceVTN tempFace0; //Read in first face,
	S_FaceVTN tempFace1; //Second
	S_FaceVTN tempFace2; //Third
	
	string buff( buffer ); //Check for lack of texture coordinates
	for( int i = 0; i < 3; i ++ )
	{
		int found = buff.find("//"); 
		if( found > 0 )
			buff.replace( found, 2, "/1/");
	}
	
	sscanf( buff.c_str(), " %i/%i/%i %i/%i/%i %i/%i/%i",	&tempFace0.m_V, &tempFace0.m_T, &tempFace0.m_N,
													&tempFace1.m_V, &tempFace1.m_T, &tempFace1.m_N,
													&tempFace2.m_V, &tempFace2.m_T, &tempFace2.m_N); 
	
	int texSize = m_TexCoords.size();
	
	if( (m_IndicesIt = m_IndicesMap.find( tempFace0 )) == m_IndicesMap.end() )
	{		
		//Face is not found, add to verts, tex and norms
		m_VBO.m_Interleaved.push_back(m_Vertices[tempFace0.m_V-1]);
		mesh->m_Vertices.push_back(m_Vertices[tempFace0.m_V-1]);

		if( texSize > 0 )
			m_VBO.m_Interleaved.push_back(m_TexCoords[tempFace0.m_T-1]);
		else
			m_VBO.m_Interleaved.push_back( vec3() );

		m_VBO.m_Interleaved.push_back(m_Normals[tempFace0.m_N-1]);
		
		//Point to the beginning of the tripple
		m_IndicesMap[tempFace0] = m_Counts.m_Tripples;
		face.m_V0 = m_Counts.m_Tripples;
		m_Counts.m_Tripples++;
		
		mesh->m_VertexCount++;
		mesh->m_Indices.push_back(face.m_V0); //Push ind onto mesh ind list
	} 
	else
	{
		face.m_V0 = m_IndicesIt->second;
		mesh->m_Indices.push_back(m_IndicesIt->second);
	}
	
	if( (m_IndicesIt = m_IndicesMap.find( tempFace1 )) == m_IndicesMap.end() )
	{
		//Face is not found, add to verts, tex and norms
		m_VBO.m_Interleaved.push_back(m_Vertices[tempFace1.m_V-1]);
		mesh->m_Vertices.push_back(m_Vertices[tempFace1.m_V-1]);

		if( texSize > 0 )
			m_VBO.m_Interleaved.push_back(m_TexCoords[tempFace1.m_T-1]);
		else
			m_VBO.m_Interleaved.push_back( vec3() );

		m_VBO.m_Interleaved.push_back(m_Normals[tempFace1.m_N-1]);
		
		//Point to the begenning of the tripple
		m_IndicesMap[tempFace1] = m_Counts.m_Tripples;
		face.m_V1 = m_Counts.m_Tripples;
		m_Counts.m_Tripples++;

		mesh->m_VertexCount++;
		mesh->m_Indices.push_back(face.m_V1); //Push ind onto mesh ind list
	}
	else
	{
		face.m_V1 = m_IndicesIt->second;
		mesh->m_Indices.push_back(m_IndicesIt->second);
	}
	if( (m_IndicesIt = m_IndicesMap.find( tempFace2 )) == m_IndicesMap.end() ) {
		
		//Face is not found, add to verts, tex and norms
		m_VBO.m_Interleaved.push_back(m_Vertices[tempFace2.m_V-1]);
		mesh->m_Vertices.push_back(m_Vertices[tempFace2.m_V-1]);

		if( texSize > 0 )
			m_VBO.m_Interleaved.push_back(m_TexCoords[tempFace2.m_T-1]);
		else
			m_VBO.m_Interleaved.push_back( vec3() );
		
		m_VBO.m_Interleaved.push_back(m_Normals[tempFace2.m_N-1]);
		
		//Point to the begenning of the tripple
		m_IndicesMap[tempFace2] = m_Counts.m_Tripples;
		face.m_V2 = m_Counts.m_Tripples;
		m_Counts.m_Tripples++;

		mesh->m_VertexCount++;
		mesh->m_Indices.push_back(face.m_V2); //Push ind onto mesh ind list
	}
	else
	{
		face.m_V2 = m_IndicesIt->second;
		mesh->m_Indices.push_back(m_IndicesIt->second);
	}
	m_Faces.push_back( face );
	
	m_Counts.m_Indices++;
}

void Mesh::createVBO()
{
	glGenVertexArrays(1, &m_VBO.m_VAO);
	glBindVertexArray(m_VBO.m_VAO);
		
	//Send vertex/texture/normal data as interleaved.
	glGenBuffers( 1, &m_VBO.m_MainVBO );

	glBindBuffer( GL_ARRAY_BUFFER, m_VBO.m_MainVBO );
	glBufferData( GL_ARRAY_BUFFER, m_VBO.m_PointCount * sizeof(float), &m_VBO.m_Interleaved[0], GL_STATIC_DRAW );

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	for( GLuint k = 0; k < m_MeshParts.size(); k++ ) {
		S_MeshPart *part = m_MeshParts[k];

		glGenBuffers( 1, &part->m_VBO );
		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, part->m_VBO);
		glBufferData( GL_ELEMENT_ARRAY_BUFFER, part->m_FaceCount*3*sizeof(GLuint), &(part->m_Indices[0]), GL_STATIC_DRAW );
		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	}

	glBindVertexArray(0);
	Shader::Unset();

	clearModelData();
}

GLuint Mesh::getMaterial( char* material) {
	//Search for material
	for( GLuint i = 0; i < m_Materials.size(); i ++ ) {
		if( m_Materials[i]->m_Name == material )
			return m_Materials[i]->m_MaterialID;
	}
	return 0;
}

void Mesh::clearModelData()
{
	m_IndicesMap.clear();
	m_Normals.clear();
	m_Vertices.clear();
	m_TexCoords.clear();
	m_Faces.clear();
	memset(&m_Counts, 0, sizeof(&m_Counts));
}

void Mesh::makeBoundingVolumes()
{
	for (int i=0; i<(int)m_MeshParts.size(); i++)
	{
		S_MeshPart* part = m_MeshParts[i];

		part->m_LocalBoundingSphere = new Sphere(part->m_Vertices);
		part->m_LocalAABB = new AABB(part->m_Vertices);

		part->m_Vertices.clear();
	}
}