#pragma once

#include <DS_Include.h>
#include <Tools/DS_StrOp.h>

namespace DS
{
	class RenderTarget;
	class FSQ;
	class EmptyEffect;

	class PostProcessEffect
	{
	public:
		PostProcessEffect(const string& name) : m_SceneRenderTarget(NULL)
		{
			m_Enabled = true;
			m_Name = StrOp::ToLower(name);
		}

		virtual ~PostProcessEffect()
		{
		}

		RenderTarget *m_SceneRenderTarget;

		const string& GetName() const { return m_Name; }
		bool IsEnabled() const { return m_Enabled; }
		void SetEnabled(bool v) { m_Enabled = v; }

		virtual void Render(GLuint screenTexture)=0;
	protected:
		
	private:
		string m_Name;
		bool m_Enabled;
	};

	class PostProcess
	{
	public:
		PostProcess();
		~PostProcess();

		void Initialise();
		
		void Begin();
		void Process();

		void RenderQuad();


		void AddEffect(PostProcessEffect *effect);
		PostProcessEffect* GetEffect(const string& name) const;

	private:
		RenderTarget *m_RT1;
		RenderTarget *m_RT2;

		vector<PostProcessEffect*> m_Effects;
		EmptyEffect *m_EmptyEffect;
		FSQ *m_Quad;
	};

	extern PostProcess *gPostProcess;
}
