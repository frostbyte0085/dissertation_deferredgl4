#include "DS_FSQ.h"
#include "DS_Shader.h"

using namespace DS;

FSQ::FSQ() : m_VAO(0), m_VBO(0)
{
	initialise();
}

FSQ::~FSQ()
{
	if (m_VAO)
	{
		glDeleteVertexArrays(1, &m_VAO);
		m_VAO = 0;
	}

	if (m_VBO)
	{
		glDeleteBuffers(1, &m_VBO);
		m_VBO = 0;
	}

	if (m_IBO)
	{
		glDeleteBuffers(1, &m_IBO);
		m_IBO = 0;
	}
}

void FSQ::initialise()
{
	float quad[] =
	{
		-1.0f, -1.0f,
		-1.0f, 1.0f,
		1.0f, -1.0f,
		1.0f, 1.0f
	};

	unsigned short indices[] = 
	{
		0, 1, 2, 2, 1, 3
	};

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	glGenBuffers(1, &m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);

	glGenBuffers(1, &m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
}

void FSQ::Render(bool restoreShader)
{
	glDisable(GL_CULL_FACE);

	Shader* shader = Shader::GetBoundShader();

	glBindVertexArray(m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

	shader->VertexAttribPointer("in_Position", 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	shader->EnableVertexAttribArray("in_Position");

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
	
	shader->DisableVertexAttribArray("in_Position");

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	
	if (restoreShader)
		Shader::Unset();

	glEnable(GL_CULL_FACE);
}