#include "DS_Texture.h"
#include <Tools/DS_Exception.h>
#include <Tools/DS_StrOp.h>

using namespace DS;

Texture::Texture(const string& filename, bool mipmap, GLenum format, GLenum internalFormat, bool filter, bool clamp, int width, int height) : m_TexID(0), m_Width(0), m_Height(0)
{
	m_Filename = filename;
	m_Mipmaps = mipmap;
	m_Format = format;
	m_InternalFormat = internalFormat;
	m_Filter = filter;
	m_Clamp = clamp;
	m_Width = width;
	m_Height = height;
}

Texture::~Texture()
{
	if (m_TexID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindTexture (GL_TEXTURE_CUBE_MAP, 0);

		glDeleteTextures(1, &m_TexID);
	}
}

void Texture::create2D()
{
	m_Type = TEXTURE_2D;

	void *data = NULL;
	int format = m_Format;
	int internalFormat = m_InternalFormat;
	int type = GL_UNSIGNED_BYTE;

	char *buffer = NULL;
	ILuint IL = 0;

	if (!StrOp::EndsWith(m_Filename.c_str(), ".raw"))
	{
		// load using IL
		ilGenImages(1, &IL);
		ilBindImage(IL);

		if (ilLoadImage(m_Filename.c_str()))
		{		
			ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

			if (m_Width == -1)
				m_Width = ilGetInteger(IL_IMAGE_WIDTH);
			if (m_Height == -1)
			m_Height = ilGetInteger(IL_IMAGE_HEIGHT);

			if (m_Format == 0)
				format = ilGetInteger(IL_IMAGE_FORMAT);

			if (m_InternalFormat == 0)
				internalFormat = GL_RGBA;

			type = ilGetInteger(IL_IMAGE_TYPE);
			data = ilGetData();

			ILinfo ImageInfo;
			iluGetImageInfo(&ImageInfo);
			if (ImageInfo.Origin == IL_ORIGIN_UPPER_LEFT)
			{
				iluFlipImage();
			}
		}
	}
	else
	{
		ifstream f (m_Filename.c_str(), ios::in | ios::binary | ios::ate);
		if (!f.fail())
		{
			int size = (int)f.tellg();
			buffer = new char[size];
			memset(buffer, 0, sizeof(char) * size);
			f.seekg(0, ios::beg);
			f.read(buffer, size);
			f.close();

			data = buffer;
		}
	}

	if (data != NULL)
	{
		glGenTextures(1, &m_TexID);
		glBindTexture(GL_TEXTURE_2D, m_TexID);
		
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_Clamp ? GL_CLAMP_TO_EDGE : GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_Clamp ? GL_CLAMP_TO_EDGE : GL_REPEAT );

		if (m_Filter)
		{
			float maxAnisotropy;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);
		}
		if (!m_Mipmaps)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_Filter ? GL_LINEAR : GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_Filter ? GL_LINEAR : GL_NEAREST);
        
			glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, m_Width, m_Height, 0, format, type, data);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_Filter ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_Filter ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST);
        
			gluBuild2DMipmaps( GL_TEXTURE_2D, internalFormat, m_Width, m_Height, format, type, data);
		}

		if (IL > 0)
			ilDeleteImages(1, &IL);

		glBindTexture(GL_TEXTURE_2D, 0);

		if (buffer != NULL)
			delete[] buffer;

		cout << "Created 2d texture " << m_Filename << ", location " << m_TexID << endl;
	}

}

void Texture::createCube()
{
	m_Type = TEXTURE_CUBE;

	vector<string> cubeTextureFilenames;
	string cubeLine;

	// get the data dir that this cube rests in
	int idx = m_Filename.find_last_of("/");
	string dataDir = m_Filename.substr(0, idx) + "/";

	// load the stuff
	ifstream cubeFile(m_Filename);
	if (!cubeFile.is_open())
		throw Exception("Cube texture descriptor " + m_Filename + " could not be loaded!");

	while (!cubeFile.eof())
	{
		getline(cubeFile, cubeLine);

		cubeTextureFilenames.push_back(dataDir + cubeLine);
	}
	cubeFile.close();

	vector<ILuint> IL;
	IL.resize(6);

	glGenTextures(1, &m_TexID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_TexID);
	
	for (int i=0; i<(int)cubeTextureFilenames.size(); i++)
	{
		ilGenImages(1, &IL[i]);
		ilBindImage(IL[i]);

		if (ilLoadImage(cubeTextureFilenames[i].c_str()))
		{
			ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

			m_Width = ilGetInteger(IL_IMAGE_WIDTH);
			m_Height = ilGetInteger(IL_IMAGE_HEIGHT);
			ILint format = ilGetInteger(IL_IMAGE_FORMAT);
			ILint internalFormat = GL_RGBA;
			ILint type = ilGetInteger(IL_IMAGE_TYPE);
			
			ILubyte* data = ilGetData();

			ILinfo ImageInfo;
			iluGetImageInfo(&ImageInfo);
			if (ImageInfo.Origin == IL_ORIGIN_UPPER_LEFT)
			{
				iluFlipImage();
			}

			if (m_Format != 0)
			{
				format = m_Format;
				internalFormat = m_InternalFormat;
			}

			if (m_Filter)
			{
				float maxAnisotropy;
				glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);
				glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);
			}

			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

			if (!m_Mipmaps)
			{
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, m_Filter ? GL_LINEAR : GL_NEAREST);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, m_Filter ? GL_LINEAR : GL_NEAREST);
        
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, internalFormat, m_Width, m_Height, 0, format, type, data);
			}
			else
			{
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_GENERATE_MIPMAP, GL_TRUE);

				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, m_Filter ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, m_Filter ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST);
        
				gluBuild2DMipmaps( GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, internalFormat, m_Width, m_Height, format, type, data);
			}
		}

		ilDeleteImages(1, &IL[i]);
	}
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	IL.clear();
	cubeTextureFilenames.clear();
}