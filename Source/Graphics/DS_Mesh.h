/*
Mesh loader used with permission from Sam Bailey, UEA Postgrad student
*/
#pragma once

#include <DS_Include.h>

namespace DS
{
	class Resources;
	class Texture;
	class Sphere;
	class AABB;

	struct S_NumberOf
	{
		int m_Verts;
		int m_Norms;
		int m_TexCoords;
		int m_Faces;
		int m_Tripples;
		int m_Indices;

		S_NumberOf(int verts=0, int norms=0, int texCoords=0, int faces=0, int tripples=0, int indices=0)
		{
			m_Verts=verts; m_Norms=norms; m_TexCoords=texCoords; m_Faces=faces; m_Tripples=tripples; m_Indices = indices;
		}
	};

	struct S_FaceVTN
	{
		GLuint m_V;
		GLuint m_T;
		GLuint m_N;

		S_FaceVTN( GLuint v=0, GLuint t=0, GLuint n=0 )
		{
			m_V=v; m_T=t; m_N=n;
		}
	};

	struct S_Face
	{
		GLuint m_V0;
		GLuint m_V1;
		GLuint m_V2;

		S_Face( GLuint v0=0, GLuint v1=0, GLuint v2=0 )
		{
			m_V0=v0; m_V1=v1; m_V2=v2;
		}
	};

	struct S_VBO
	{
		vector<vec3> m_Interleaved;

		GLuint m_MainVBO;

		GLuint m_VAO;
		GLuint m_PointCount;
	};

	struct S_FaceVTNComp
	{
		bool operator()( const S_FaceVTN& left, const S_FaceVTN& right )
		{
			if( left.m_V == right.m_V )
			{
				if( left.m_T == right.m_T ) 
					return (left.m_N<right.m_N);
				else 
					return (left.m_T<right.m_T);
			}
			else return (left.m_V<right.m_V);
		}
	};

	struct S_MeshPart
	{
		GLuint m_MaterialID;
		vector<GLuint> m_Indices;
		vector<vec3> m_Vertices; // these are used only for the bounding volume calculation, cleared after that.
		GLuint m_FaceCount;
		GLuint m_VertexCount;
		GLuint m_VBO;

		Sphere *m_LocalBoundingSphere;
		AABB *m_LocalAABB;

		S_MeshPart( int material=0, int mesh=0, GLuint vbo=0, int faceNum=0, int vertNum=0 )
		{
			m_LocalBoundingSphere = NULL;
			m_LocalAABB = NULL;

			m_MaterialID=material;
			m_FaceCount=faceNum;
			m_VertexCount=vertNum;
		}
	};

	struct S_MaterialData {
		vec3 m_DiffuseColor;
		vec3 m_AmbientColor;
		vec3 m_SpecularColor;
		
		GLuint m_Diffuse; 
		GLuint m_Normals; 
		GLuint m_Specular;

		int m_MaterialID; //Stores the material ID, used when rendering
		string m_Name; //Stores the name of the material as defined in the OBJ

		S_MaterialData( vec3 ambient=vec3(0,0,0),  vec3 diffuse=vec3(0,0,0),
					  vec3 specular=vec3(0,0,0), GLuint diffuseTexture=0, GLuint bumpTexture=0, GLuint specTexture=0, int matID=0, 
					  string matName="" ) {
			m_AmbientColor = ambient; m_DiffuseColor=diffuse; m_SpecularColor=specular;
			m_Diffuse=diffuseTexture; m_Normals=bumpTexture; m_Specular = specTexture; m_MaterialID=matID; m_Name=matName;
		};
	};

	class Mesh
	{
		friend class Resources;
		friend class Renderer;
	public:

	private:
		Mesh(const string& filename);
		~Mesh();
		void create();

		string m_Filename;

		S_NumberOf m_Counts;
		S_VBO m_VBO;

		vector<S_MeshPart*> m_MeshParts;
		vector<S_MaterialData*> m_Materials;

		// read data
		vector<S_Face> m_Faces;
		vector<vec3> m_Vertices;
		vector<vec3> m_Normals;
		vector<vec3> m_TexCoords;

		//Face map for sorting unique faces
		map<S_FaceVTN, unsigned int, S_FaceVTNComp> m_IndicesMap;
		map<S_FaceVTN, unsigned int, S_FaceVTNComp>::iterator m_IndicesIt;

		// reading
		void readOBJ(FILE* file);
		void readFace( FILE* file, S_MeshPart* mesh );
		void readTextures( FILE* file, const char* location );

		GLuint getMaterial( char* material);

		void createVBO();
		void makeBoundingVolumes();

		void clearModelData();
	};
}