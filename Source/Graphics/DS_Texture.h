#pragma once

#include <DS_Include.h>

namespace DS
{
	class Resouces;

	enum eTextureType
	{
		TEXTURE_2D,
		TEXTURE_CUBE
	};

	class Texture
	{
		friend class Resources;
	public:

		GLuint GetID() const { return m_TexID; }
		int GetWidth() const { return m_Width; }
		int GetHeight() const { return m_Height; }
		bool HasMipmaps() const { return m_Mipmaps; }
		eTextureType GetType() const { return m_Type; }

	private:
		Texture(const string& filename, bool mipmaps, GLenum format, GLenum internalFormat, bool filter, bool clamp, int width, int height);
		~Texture();

		void create2D();
		void createCube();

		GLenum m_Format;
		GLenum m_InternalFormat;

		string m_Filename;
		bool m_Mipmaps;
		bool m_Filter;
		bool m_Clamp;

		int m_Width;
		int m_Height;

		eTextureType m_Type;

		GLuint m_TexID;	
	};
}