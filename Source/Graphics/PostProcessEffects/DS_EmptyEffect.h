#pragma once

#include "../DS_PostProcess.h"

namespace DS
{
	class Shader;

	class EmptyEffect : public PostProcessEffect
	{
	public:
		EmptyEffect();
		~EmptyEffect();

		void Render(GLuint screenTexture);
	private:
		Shader *m_Shader;

	};
}