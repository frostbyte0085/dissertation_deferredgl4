#pragma once

#include "../DS_PostProcess.h"

namespace DS
{
	class Shader;
	class RenderTarget;
	class Texture;

	class SMAAEffect : public PostProcessEffect
	{
	public:
		SMAAEffect(const string &name);
		~SMAAEffect();

		void Render(GLuint screenTexture);
	private:
		Shader *m_Shader;

		// SMAA requirements
		RenderTarget *m_EdgesRT;
		RenderTarget *m_BlendRT;

		Shader *m_Passes[3];

		Texture *m_AreaTexture;
		Texture *m_SearchTexture;
	};
}