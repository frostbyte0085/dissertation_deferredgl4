#pragma once

#include "../DS_PostProcess.h"

namespace DS
{
	class Shader;
	class Texture;

	class DeferredCompositionEffect : public PostProcessEffect
	{
	public:
		DeferredCompositionEffect(const string &name);
		~DeferredCompositionEffect();

		void Render(GLuint screenTexture);
		bool DoSSAO;
		bool SSAOStep;
	private:
		Shader *m_Shader;
	};
}