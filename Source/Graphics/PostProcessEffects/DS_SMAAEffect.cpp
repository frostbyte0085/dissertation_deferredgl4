#include "DS_SMAAEffect.h"
#include "../DS_Shader.h"
#include "../DS_PostProcess.h"
#include <Core/DS_Resources.h>
#include "../DS_RenderTarget.h"
#include "../DS_Texture.h"

using namespace DS;

SMAAEffect::SMAAEffect(const string& name) : PostProcessEffect(name), m_Shader(NULL)
{
	m_Passes[0] = gResources->LoadShader("Data/Shaders/PostProcessEffects/PP_SMAA_EdgeDetection.shader");
	m_Passes[1] = gResources->LoadShader("Data/Shaders/PostProcessEffects/PP_SMAA_BlendWeightsCalculation.shader");
	m_Passes[2] = gResources->LoadShader("Data/Shaders/PostProcessEffects/PP_SMAA_NeighborhoodBlending.shader");

	m_Shader = gResources->LoadShader("Data/Shaders/PostProcessEffects/PP_Empty.shader");

	int vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);

	m_AreaTexture = gResources->LoadTexture("Data/Textures/SMAA/smaa_area.png", false, true, true, GL_RG, GL_RG8, 160, 560);
	m_SearchTexture = gResources->LoadTexture("Data/Textures/SMAA/smaa_search.png", false, false, true, GL_RED, GL_R8, 66, 33);

	m_EdgesRT = new RenderTarget(vp[2], vp[3]);
	m_EdgesRT->BeginAdd(false);
	m_EdgesRT->AddComponent("main", GL_RGBA8, GL_RGBA, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_LINEAR, GL_COLOR_ATTACHMENT0);
	m_EdgesRT->EndAdd();

	m_BlendRT = new RenderTarget(vp[2], vp[3]);	
	m_BlendRT->BeginAdd(false);
	m_BlendRT->AddComponent("main", GL_RGBA8, GL_RGBA, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_LINEAR, GL_COLOR_ATTACHMENT0);
	m_BlendRT->EndAdd();
}

SMAAEffect::~SMAAEffect()
{
	delete m_BlendRT;
	delete m_EdgesRT;
}

void SMAAEffect::Render(GLuint screenTexture)
{
	
	int vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);

	vec2 pixelSize = vec2(1.0f / vp[2], 1.0f / vp[3]);

	// BEGIN PASS 1
	// pass 1: Edge detection
	// pass our screen texture for the unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, screenTexture);

	m_EdgesRT->Set();
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT);

	m_Passes[0]->Set();

	m_Passes[0]->Uniform2("SMAA_PIXEL_SIZE", 1, value_ptr(pixelSize));

	gPostProcess->RenderQuad();
	
	RenderTarget::Resolve();

	// END PASS 1
	//
	
	// BEGIN PASS 2
	// pass 2: Blend weights calculation
	// pass our edges rendertarget texture for the unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_EdgesRT->GetTexture("main"));

	// pass our area texture for the unit 0
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_AreaTexture->GetID());

	// pass our search texture for the unit 0
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_SearchTexture->GetID());

	m_BlendRT->Set();
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT);

	m_Passes[1]->Set();
	m_Passes[1]->Uniform2("SMAA_PIXEL_SIZE", 1, value_ptr(pixelSize));

	gPostProcess->RenderQuad();

	RenderTarget::Resolve();
	// END PASS 2
	//

	// BEGIN PASS 3
	// pass 3: Neighborhood blending
	// pass our screen texture for the unit 0
	if (m_SceneRenderTarget != NULL)
	{
		m_SceneRenderTarget->Set();
	}
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, screenTexture);

	// pass our blend rendertarget texture for the unit 1
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_BlendRT->GetTexture("main"));

	// pass 0 for the unit 2
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);


	m_Passes[2]->Set();
	m_Passes[2]->Uniform2("SMAA_PIXEL_SIZE", 1, value_ptr(pixelSize));

	//glEnable(GL_FRAMEBUFFER_SRGB);

	gPostProcess->RenderQuad();

	//glDisable(GL_FRAMEBUFFER_SRGB);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);
	// END PASS 3
	//
}