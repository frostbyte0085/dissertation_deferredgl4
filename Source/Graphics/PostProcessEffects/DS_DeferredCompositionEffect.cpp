#include "DS_DeferredCompositionEffect.h"
#include "../DS_Shader.h"
#include "../DS_PostProcess.h"
#include <Core/DS_Resources.h>
#include "../DS_Renderer.h"
#include "../DS_RenderTarget.h"
#include "../DS_Camera.h"
#include <Scene/DS_Scene.h>
#include <Scene/DS_Object.h>
#include "../DS_Texture.h"


using namespace DS;

DeferredCompositionEffect::DeferredCompositionEffect(const string &name) : PostProcessEffect(name), m_Shader(NULL)
{
	m_Shader = gResources->LoadShader("Data/Shaders/PostProcessEffects/PP_DeferredComposition.shader");
}

DeferredCompositionEffect::~DeferredCompositionEffect()
{

}

// there is no previous postprocess step, we don't use the screenTexture
void DeferredCompositionEffect::Render(GLuint screenTexture)
{
	if (m_SceneRenderTarget != NULL)
	{
		m_SceneRenderTarget->Set();
	}
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT);

	RenderTarget *gbuffer = gRenderer->GetGBuffer();
	RenderTarget *lightmap = gRenderer->GetLightmap();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gbuffer->GetTexture("depth"));

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gbuffer->GetTexture("albedo"));

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, lightmap->GetTexture("lightmap"));


	Object *camObj = gScene->GetCamera();
	Camera *cam = camObj->GetCamera();

	m_Shader->Set();

	m_Shader->UniformMatrix4("invProjection", 1, GL_FALSE, value_ptr(inverse(cam->GetProjectionMatrix())));

	m_Shader->Uniform1("doSSAO", (DoSSAO == true) ? 1 : 0);
	m_Shader->Uniform1("SSAOStep", (SSAOStep == true) ? 1 : 0);

	m_Shader->Uniform3("ambientColor", 1, value_ptr(RenderSettings::AmbientColor));
	gPostProcess->RenderQuad();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);

	RenderTarget::Resolve();
}