#include "DS_EmptyEffect.h"
#include "../DS_Shader.h"
#include "../DS_PostProcess.h"
#include <Core/DS_Resources.h>

using namespace DS;

EmptyEffect::EmptyEffect() : PostProcessEffect("empty"), m_Shader(NULL)
{
	m_Shader = gResources->LoadShader("Data/Shaders/PostProcessEffects/PP_Empty.shader");
}

EmptyEffect::~EmptyEffect()
{

}

void EmptyEffect::Render(GLuint screenTexture)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, screenTexture);

	m_Shader->Set();
	gPostProcess->RenderQuad();

	glBindTexture(GL_TEXTURE_2D, 0);
}