#pragma once

#include <DS_Include.h>

namespace DS
{
	const string kExceptionLog = "Exception thrown: ";

	class Exception
	{
	public:
		Exception (const string &exceptionDescription) { m_Description = exceptionDescription; }
		~Exception() {}

		const string& GetDescription() const { return m_Description; }

	private:
		string m_Description;
	};
}