#pragma once

#include <DS_Include.h>
#include <iomanip>

namespace DS
{
	class StrOp
	{
	public:
		static string ToLower(const string& str)
		{
			string lowered = str;

			transform(lowered.begin(), lowered.end(),lowered.begin(), tolower );

			return lowered;
		}
		
		static bool EndsWith(const string& str, const string& ending)
		{
			if (str.length() >= ending.length())
				return (0 == str.compare (str.length() - ending.length(), ending.length(), ending));
			return false;
		}

		// use stringstream to convert value to a string
		template <class T> static string ToString(const T& value, int precision=2)
		{
			stringstream out;
			out << std::fixed << setprecision(precision) << value;
			return out.str();
		}

		// use istringstream to convert string to value
		template <class T> static T FromString(const string& s)
		{
			istringstream is(s);
			T t;
			is >> t;
			return t;
		}
	};
}