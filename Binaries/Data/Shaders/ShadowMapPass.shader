@VS
#version 420

precision highp float;

in vec3 in_Position;

uniform mat4 world;
uniform mat4 view;
uniform mat4 projection;
uniform int isDirectional;

out vec4 WorldPosition;

void main()
{
	mat4 mvp = projection * view * world;
	gl_Position = mvp * vec4(in_Position, 1.0);
		
	if (isDirectional == 0)
		WorldPosition = world * vec4(in_Position, 1.0);
	else
		WorldPosition = gl_Position;
}

@PS
#version 420

precision highp float;

#define DEPTH 0

//Output targets
layout(location = DEPTH) out float Depth;

in vec4 WorldPosition;

uniform int isDirectional;
uniform vec3 lightPosition;
uniform float depthModulation;

void main()
{
	if (isDirectional == 0)
		Depth = length(lightPosition - WorldPosition.xyz) / depthModulation;
	else
	{
		Depth = WorldPosition.z / WorldPosition.w;
	}
}
