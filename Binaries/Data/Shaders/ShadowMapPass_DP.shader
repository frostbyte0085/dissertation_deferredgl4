@VS
#version 420

precision highp float;

in vec3 in_Position;

uniform mat4 world;
uniform mat4 view;

uniform float nearClipPlane;
uniform float farClipPlane;
uniform float direction;

out vec4 WorldPosition;
out float Z;

// dp render shader based on 
// http://members.gamedev.net/JasonZ/Paraboloid/DualParaboloidMappingInTheVertexShader.pdf
void main()
{
	mat4 mv = view * world;
	vec4 viewSpacePos = mv * vec4(in_Position, 1.0);
	viewSpacePos.xyz /= viewSpacePos.w;
	viewSpacePos.z *= direction;
	
	float distance = length(viewSpacePos.xyz);
	viewSpacePos /= distance;
	
	Z = viewSpacePos.z;
	
	viewSpacePos.xy /= viewSpacePos.z + 1.0;
	viewSpacePos.xy = clamp(viewSpacePos.xy, -1.0, 1.0);
	
	viewSpacePos.z = (distance - nearClipPlane) / (farClipPlane - nearClipPlane);
	viewSpacePos.w = 1.0;
		
	WorldPosition = world * vec4(in_Position, 1.0);
	gl_Position = viewSpacePos;
}

@PS
#version 420

precision highp float;

//Output targets
layout(location = 0, index = 0) out float Depth;

uniform float direction;

in vec4 WorldPosition;
in float Z;

uniform float depthModulation;
uniform vec3 lightPosition;

void main()
{
	if (Z < -0.3) discard;
	
	Depth = length(lightPosition - WorldPosition.xyz) / depthModulation;
}
