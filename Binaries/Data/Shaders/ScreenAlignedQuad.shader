@VS
#version 420 core

precision highp float;

in vec2 in_Position;

out vec2 ex_TexCoord;

void main()
{
	// PositionNDC is in normalized device coordinates (NDC).
	// The (x, y) coordinates are in the range [-1, 1].
	
	gl_Position = vec4(in_Position, 0.0, 1.0);

	// To generate the texture coordinates for the resulting full screen
	// quad we need to transform the vertex position's coordinates from the
	// [-1, 1] range to the [0, 1] range. This is achieved with a scale and
	// a bias.

	ex_TexCoord = in_Position * 0.5 + 0.5;
}

@PS
#version 420 core

precision highp float;

uniform sampler2D tex;

in vec2 ex_TexCoord;

out vec4 ex_FragColor;

void main()
{
	vec4 texel;
	
	texel = texture(tex, ex_TexCoord.xy);
		
	ex_FragColor = vec4(1.0, 1.0, 1.0, 1.0) * texel;
}