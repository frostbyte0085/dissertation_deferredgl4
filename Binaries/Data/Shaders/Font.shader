@VS
#version 420

precision highp float;

in vec2 in_Position;
in vec2 in_TexCoord;

uniform mat4 world;
uniform mat4 view;
uniform mat4 projection;

out block
{
	vec2 TexCoord;
}OUT;

void main()
{
	mat4 mvp = projection * view * world;
	OUT.TexCoord = in_TexCoord;
	gl_Position = mvp * vec4(in_Position, 0.0, 1.0);
}

@PS
#version 420

precision highp float;

layout (binding=0) uniform sampler2D tex;

in block
{
	vec2 TexCoord;
}IN;

//Output targets
out vec4 colourOut;

void main()
{
	colourOut = texture(tex, IN.TexCoord);
}
