@VS
#version 420 core

precision highp float;

uniform mat4 world;
uniform mat4 view;
uniform mat4 projection;

in vec3 in_Position;
in vec3 in_TexCoord;
in vec3 in_Normal;

out block
{
	vec3 PositionVS;
	vec2 TexCoord;
	vec3 Normal;
	vec3 ViewDir;
}OUT;

void main()
{
	mat4 pmv = projection * view * world;
	mat4 mv = view * world;
	
	OUT.TexCoord = in_TexCoord.xy;
	OUT.PositionVS = (mv * vec4(in_Position, 1)).xyz;
	OUT.ViewDir = normalize(-OUT.PositionVS);
	
	OUT.Normal = normalize(in_Normal);
	
	gl_Position = pmv * vec4(in_Position, 1.0);
}

@PS
#version 420 core

#include common

#define ALBEDO 0
#define NORMAL 1
#define DEPTH 2

precision highp float;

uniform mat3 normalMatrix;
uniform float nearClipPlane;
uniform float farClipPlane;

layout(binding=0) uniform sampler2D diffuseTexture;
uniform int hasDiffuseTexture;

layout(binding=1) uniform sampler2D normalTexture;
uniform int hasNormalTexture;

layout(binding=2) uniform sampler2D specularTexture;
uniform int hasSpecularTexture;

in block
{
	vec3 PositionVS;
	vec2 TexCoord;
	vec3 Normal;
	vec3 ViewDir;
}IN;

layout(location = ALBEDO) out vec4 Albedo;
layout(location = NORMAL) out vec4 Normal;
layout(location = DEPTH) out vec2 Depth;


mat3 makeTBN(vec3 normal, vec3 position, vec2 texcoord)
{
	vec3 dpx = dFdx(position);
    vec3 dpy = dFdy(position);
    vec2 dtx = dFdx(texcoord);
    vec2 dty = dFdy(texcoord);
    
	vec3 tangent = normalize(dpx * dty.t - dpy * dtx.t);
	vec3 bitangent = cross(tangent, normal);
   
    return normalMatrix * mat3(tangent, bitangent, normal);
}

void main()
{
	vec2 uv = IN.TexCoord;
	vec4 albedo = vec4(1,1,1,1);
	float specular_i = 0;
	float specular_e = 0;
	
	vec3 normal = IN.Normal;
	vec3 viewDir = IN.ViewDir;
	
	if (hasDiffuseTexture > 0)
	{
		albedo = texture(diffuseTexture, uv);
	}
	
	if (hasNormalTexture > 0)
	{
		mat3 tbn = makeTBN(normal, viewDir, uv);
		
		normal = texture(normalTexture, uv).xyz * 2.0 - 1.0;
		normal = normalize(tbn * normal);
	}
	
	if (hasSpecularTexture > 0)
	{
		vec2 specular = texture(specularTexture, uv).xy;
		specular_i = specular.x;
		specular_e = specular.y;
	}
	
	// albedo holds the diffuse colour plus currently empty for alpha
	Albedo = vec4(albedo.rgb, 1);
	
	// pack normals as spherical coordinates and store the in the Normal RT's xy component
	// store specular intensity and power in the zw component of the Normal RT.
	vec2 packedNormals = CartesianToSpherical(normal);
	Normal = vec4(packedNormals, pack3to1(vec3(specular_i, specular_e,0)), 0);

	// linear view-space depth
	// inspired by CryENGINE 3.
	Depth.r = IN.PositionVS.z / farClipPlane;
}