@VS
#version 420 core

precision highp float;

uniform mat4 world;
uniform mat4 projection;
uniform mat4 view;

in vec3 in_Position;

out block
{
	vec4 Position;
	vec4 PositionVS;
}OUT;

void main()
{
	mat4 pmv = projection * view * world;
	mat4 mv = view * world;
	
	gl_Position = pmv * vec4(in_Position, 1);
	
	OUT.Position = gl_Position;
	OUT.PositionVS = mv * vec4(in_Position,1);
}

@PS
#version 420 core

precision highp float;

#include common

uniform mat4 world;

in block
{
	vec4 Position;
	vec4 PositionVS;
}IN;

layout (binding=0) uniform sampler2D normalTexture;
layout (binding=1) uniform sampler2D depthTexture;
layout (binding=2) uniform sampler2D cookieTexture;
layout (binding=3) uniform sampler2D shadowMapTexture;
layout (binding=4) uniform sampler2D poissonDiskTexture;

uniform float depthBias;
uniform int hasShadows;
uniform int hasFilter;
uniform vec2 shadowMapSize;
uniform int hasCookie;
uniform mat4 invView;
uniform vec2 gbufferSize;
uniform float camFarClipPlane;
uniform vec3 cameraPosition;
uniform vec3 position;
uniform vec3 direction;
uniform float height;
uniform float intensity;
uniform vec3 color;
uniform float angleCos;
uniform float radius;

uniform float lightNearPlane;
uniform float lightFarPlane;

uniform mat4 lightMatrix;
uniform float linearDepthConstant;

out vec4 ex_color;


float attenuateFromCookie(sampler2D cookieSampler, vec4 fragmentWorldPos, mat4 lightMatrix)
{
	vec4 lightSpacePos3d = lightMatrix * fragmentWorldPos;
	vec2 projCoords = lightSpacePos3d.xy / lightSpacePos3d.w;
	projCoords.xy =  0.5 * projCoords + 0.5;
	
	return texture(cookieSampler, projCoords).r;
}

float calculateShadowFactor(sampler2D shadowSampler, vec4 fragmentWorldPos, mat4 lightMatrix, float distance)
{
	vec4 lightSpacePos3d = lightMatrix * fragmentWorldPos;
	
	if (lightSpacePos3d.w <= 0.0)
		return 1.0;
	
	vec2 projCoords = lightSpacePos3d.xy / lightSpacePos3d.w;
	projCoords =  0.5 * projCoords + 0.5;
	
	float lit = 0.0;

	if (hasFilter == 1)
	{
		lit = filterPoisson(shadowSampler, poissonDiskTexture, distance * linearDepthConstant, depthBias, shadowMapSize, projCoords);
	}
	else
	{
		float shadowDepth = texture(shadowSampler, projCoords).r;
		if (distance * linearDepthConstant <= shadowDepth + depthBias)
			lit = 1.0;
	}
	return lit;
}

vec4 Phong(vec3 position0, vec3 normal, float specularIntensity, float specularPower)
{	
	vec3 L = position - position0;
	vec3 D = normalize(direction);
	
	float dist = length(L);
	
	float heightAttenuation = 1.0 - smoothstep(height * 0.75, height, dist);
	if (heightAttenuation == 0.0) discard;
	
	L = normalize(L);
	float DL = dot(D, -L);
	
	if (DL <= angleCos) discard;
	
	float radialAttenuation = 1.0;
	
	// make our radial attenuation
	if (hasCookie == 1)
	{
		radialAttenuation = attenuateFromCookie(cookieTexture, vec4(position0, 1.0), lightMatrix);
	}
	else
	{
		radialAttenuation = pow(DL, 17);
	}

	float attenuation = heightAttenuation * radialAttenuation;
	if (attenuation == 0.0) discard;
		
	vec3 N = normalize(normal);
	
	float NL = dot(N, L);
	if (NL <= 0.0) discard;
	
	vec3 R = normalize(reflect(-L, N));
	
	vec3 E = normalize(cameraPosition - position0);
	
	vec3 diffuse = NL * color;
	float specular = specularIntensity * pow(clamp(dot(R,E),0,1), specularPower);
	
	float shadow = 1.0;
	if (hasShadows == 1)
	{
		shadow = calculateShadowFactor(shadowMapTexture, vec4(position0, 1.0), lightMatrix, dist);
	}
	return shadow * attenuation * intensity * vec4(diffuse, specular);
}

void main()
{
	vec2 screenPos = IN.Position.xy / IN.Position.w;
	vec2 uv = 0.5 * screenPos + 0.5;
	
	vec4 normal = texture(normalTexture, uv);
	// unpack the normal and specular components
	// convert normal from spherical coordinates to cartesian
	vec3 unpackedNormal = SphericalToCartesian(normal.xy);
	
	// unpack specular
	vec3 unpackedSpecular = unpack1to3(normal.z);
	float specularIntensity = unpackedSpecular.x;
	float specularPower = unpackedSpecular.y * 255;
	
	ex_color = Phong((invView * PositionFromLinearZ(depthTexture, uv, IN.PositionVS, camFarClipPlane)).xyz,
					unpackedNormal, specularIntensity, specularPower);
}