@VS
#version 420 core

precision highp float;

uniform mat4 inverseProjection;
uniform vec2 gbufferSize;

in vec2 in_Position;

out block
{
	vec3 PositionVS;
	vec2 TexCoord;
}OUT;

void main()
{
	OUT.TexCoord = (in_Position * 0.5 + 0.5);
	OUT.PositionVS = (inverseProjection * vec4(in_Position, 0, 1)).xyz;
	
	gl_Position = vec4(in_Position, 0.0, 1.0);
}

@PS
#version 420 core

precision highp float;

#include common

in block
{
	vec3 PositionVS;
	vec2 TexCoord;
}IN;

layout (binding=0) uniform sampler2D normalTexture;
layout (binding=1) uniform sampler2D depthTexture;
layout (binding=2) uniform sampler2D cookieTexture;
layout (binding=3) uniform sampler2D shadowMapTexture;

uniform mat4 invView;
uniform mat4 inverseProjection;

uniform float depthBias;
uniform int hasShadows;
uniform vec2 shadowMapSize;
uniform vec2 gbufferSize;
uniform int hasCookie;
uniform vec3 color;
uniform vec3 cameraPosition;
uniform vec3 direction;
uniform float intensity;
uniform vec2 projectionScale;
uniform float camFarClipPlane;

uniform mat4 cookieLightMatrix;
uniform mat4 shadowLightMatrix;

out vec4 ex_color;

float attenuateFromCookie(sampler2D cookieSampler, vec3 fragmentPos, mat4 lightMatrix)
{
	vec4 lightSpacePos = lightMatrix * vec4(fragmentPos,1.0);
	
	vec2 projCoords = lightSpacePos.xy / lightSpacePos.w;
	
	
	return texture(cookieSampler, projCoords * projectionScale * 1000).r;
}

float calculateShadowFactor(sampler2D shadowSampler, vec3 fragmentPos, mat4 lightMatrix)
{
	vec4 lightSpacePos = lightMatrix * vec4(fragmentPos, 1.0);
	
	vec2 projCoords = lightSpacePos.xy / lightSpacePos.w;
	projCoords =  0.5 * projCoords + 0.5;
	
	float shadowZ = texture(shadowSampler, projCoords).r;
	float depth = lightSpacePos.z / lightSpacePos.w;
	
	float lit = 1.0;
	if (depth > shadowZ + 0.0001)
		lit = 0.0;
		
	return lit;	
}

vec4 Phong(vec3 position, vec3 normal, float specularIntensity, float specularPower)
{
	float attenuation = 1.0;
	if (hasCookie == 1)
	{
		attenuation = attenuateFromCookie(cookieTexture, position, cookieLightMatrix);
	}
	if (attenuation == 0.0)
		discard;
		
	float shadowFactor = 1.0;
	if (hasShadows == 1)
	{
		shadowFactor = calculateShadowFactor(shadowMapTexture, position, shadowLightMatrix);
	}
	if (shadowFactor == 0.0)
		discard;
	
	vec3 R = normalize(reflect(direction, normal));
	vec3 E = normalize(cameraPosition - position);
	float NL = dot(normal, -direction);
	vec3 diffuse = NL * color.xyz;
	float specular = specularIntensity * pow(clamp(dot(R, E), 0, 1), specularPower);

	return shadowFactor * attenuation * intensity * vec4(diffuse.rgb, specular);
}

void main()
{	
	vec4 normal = texture(normalTexture, IN.TexCoord);
	// unpack the normal and specular components
	// convert normal from spherical coordinates to cartesian
	vec3 unpackedNormal = SphericalToCartesian(normal.xy);
	// unpack specular
	vec3 unpackedSpecular = unpack1to3(normal.z);
	float specularIntensity = unpackedSpecular.x;
	float specularPower = unpackedSpecular.y * 255;
		
	ex_color = Phong((invView * PositionFromLinearZ(depthTexture, IN.TexCoord, vec4(IN.PositionVS,1), camFarClipPlane)).xyz,
			unpackedNormal, specularIntensity, specularPower);
}
