@VS
#version 420 core

precision highp float;

uniform mat4 world;
uniform mat4 projection;
uniform mat4 view;

in vec3 in_Position;

out block
{
	vec4 Position;
	vec4 PositionVS;
}OUT;

void main()
{
	mat4 pmv = projection * view * world;
	mat4 mv = view * world;
	
	gl_Position = pmv * vec4(in_Position, 1);
	
	OUT.Position = gl_Position;
	OUT.PositionVS = mv * vec4(in_Position,1);
}

@PS
#version 420 core

precision highp float;

#include common

in block
{
	vec4 Position;
	vec4 PositionVS;
}IN;


layout (binding=0) uniform sampler2D normalTexture;
layout (binding=1) uniform sampler2D depthTexture;
layout (binding=2) uniform samplerCube cookieTexture;
layout (binding=3) uniform sampler2D dpFront;
layout (binding=4) uniform sampler2D dpBack;
layout (binding=5) uniform sampler2D poissonDiskTexture;

uniform float depthBias;
uniform int hasShadows;
uniform int hasFilter;
uniform vec2 shadowMapSize;
uniform int hasCookie;
uniform vec2 gbufferSize;
uniform float camFarClipPlane;
uniform vec3 cameraPosition;
uniform vec3 position;
uniform float intensity;
uniform vec3 color;
uniform float radius;

uniform mat4 invView;
uniform mat4 world;

uniform float lightNearPlane;
uniform float lightFarPlane;
uniform float linearDepthConstant;

uniform mat4 lightView;

out vec4 ex_color;

float attenuateFromCookie(samplerCube cookieSampler, vec3 texcoord)
{
	return texture(cookieSampler, texcoord).r;
}

float calculateShadowFactor(float distance, vec3 fragPos)
{
	vec4 fragPosLight = lightView * vec4(fragPos,1);
	fragPosLight.xyz /= fragPosLight.w;
	
	float L = length(fragPosLight.xyz);
	vec3 pos = fragPosLight.xyz / L;

	float lit = 0.0;
	vec2 coord;
	float shadowZ = 0.0;
	
	if (pos.z >= 0.0)
	{
		coord.x = (pos.x / (1.0 + pos.z)) * 0.5 + 0.5;
		coord.y = (pos.y / (1.0 + pos.z)) * 0.5 + 0.5;

		if (hasFilter == 1)
			lit = filterPoisson(dpFront, poissonDiskTexture, distance * linearDepthConstant, depthBias, shadowMapSize, coord);
		else
		{
			float shadowDepth = texture(dpFront, coord).r;
			if (distance * linearDepthConstant <= shadowDepth + depthBias)
				lit = 1.0;
		}
	}
	else
	{
		coord.x = (pos.x / (1.0 - pos.z)) * 0.5 + 0.5;
		coord.y = (pos.y / (1.0 - pos.z)) * 0.5 + 0.5;
			
		if (hasFilter == 1)
		{
			lit = filterPoisson(dpBack, poissonDiskTexture, distance * linearDepthConstant, depthBias, shadowMapSize, coord);
		}
		else
		{
			float shadowDepth = texture(dpBack, coord).r;
			if (distance * linearDepthConstant <= shadowDepth + depthBias)
				lit = 1.0;
		}
	}
	
	return lit;
}

vec4 Phong(vec3 position0, vec3 normal, float specularIntensity, float specularPower)
{
	vec3 L = position - position0;
	float dist = length(L);
	L = normalize(L);
	
	// simple linear attenuation
	float factor = max(.01f, dist) / radius;
	float radialAttenuation = clamp(1 - factor, 0, 1);
	float cookieAttenuation = 1.0;
	
	// make our radial attenuation
	if (hasCookie == 1)
	{
		cookieAttenuation = attenuateFromCookie(cookieTexture, mat3(world) * vec3(-L.x, L.y, L.z));
	}
	float attenuation = cookieAttenuation * radialAttenuation;
	if (attenuation == 0.0)
		discard;

	float shadow = 1.0;
	if (hasShadows == 1)
	{
		shadow = calculateShadowFactor(dist, position0);
	}
	if (shadow == 0.0)
		discard;
	
	vec3 R = normalize(reflect(-L, normal));
	vec3 E = normalize(cameraPosition - position0);
	
	float NL = dot(normal, L);
	vec3 diffuse = NL * color;
	float specular = specularIntensity * pow(clamp(dot(R, E), 0, 1), specularPower);

	return shadow * attenuation * intensity * vec4(diffuse.rgb, specular);
}

void main()
{
	vec2 screenPos = IN.Position.xy / IN.Position.w;
	vec2 uv = 0.5 * screenPos + 0.5;
	
	// discard fragments outside the light radius
	vec4 pos3d = invView * PositionFromLinearZ(depthTexture, uv, IN.PositionVS, camFarClipPlane);
	
	float dlight = length(pos3d.xyz - position);
	if (dlight > radius) discard;	
	
	vec4 normal = texture(normalTexture, uv);
	// unpack the normal and specular components
	// convert normal from spherical coordinates to cartesian
	vec3 unpackedNormal = SphericalToCartesian(normal.xy);
	// unpack specular
	vec3 unpackedSpecular = unpack1to3(normal.z);
	float specularIntensity = unpackedSpecular.x;
	float specularPower = unpackedSpecular.y * 255;
		
	ex_color = Phong(pos3d.xyz, unpackedNormal, specularIntensity, specularPower);
}
