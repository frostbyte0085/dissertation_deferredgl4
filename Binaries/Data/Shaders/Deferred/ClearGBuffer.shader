@VS
#version 420 core

precision highp float;

in vec2 in_Position;

void main()
{
	gl_Position = vec4(in_Position, 0.0, 1.0);
}

@PS
#version 420 core

precision highp float;

#define ALBEDO 0
#define NORMAL 1
#define DEPTH 2

layout(location = ALBEDO) out vec4 Albedo;
layout(location = NORMAL) out vec4 Normal;
layout(location = DEPTH) out float Depth;

void main()
{
	Albedo = vec4(0,0,0,1);
	Normal = vec4(0,0,0,1);
	Depth = 1.0;
}