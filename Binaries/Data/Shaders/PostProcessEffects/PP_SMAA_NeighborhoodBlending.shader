@VS
#version 420 core

#define SMAA_ONLY_COMPILE_VS 1
#define NO_PS
#include common

precision highp float;

out vec2 texcoord;
out vec4 offset[2];

in vec2 in_Position;

void main()
{
	gl_Position = vec4(in_Position, 0.0, 1.0);

	texcoord = in_Position * 0.5 + 0.5;

	SMAANeighborhoodBlendingVS(texcoord, offset);
}

@PS
#version 420 core

#define SMAA_ONLY_COMPILE_PS 1
#include common

precision highp float;

layout (binding=0) uniform sampler2D colorTex;
layout (binding=1) uniform sampler2D blendTex;

in vec2 texcoord;
in vec4 offset[2];

out vec4 ex_FragColor;

void main()
{
	ex_FragColor = SMAANeighborhoodBlendingPS(texcoord, offset, colorTex, blendTex);
}