@VS
#version 420 core

#define SMAA_ONLY_COMPILE_VS 1
#define NO_PS
#include common

precision highp float;

in vec2 in_Position;

out vec2 texcoord;
out vec2 pixcoord;
out vec4 offset[3];

void main()
{
	gl_Position = vec4(in_Position, 0.0, 1.0);

	texcoord = in_Position * 0.5 + 0.5;
	
	SMAABlendingWeightCalculationVS(texcoord, pixcoord, offset);
}

@PS
#version 420 core

#define SMAA_ONLY_COMPILE_PS 1
#include common

in vec2 texcoord;
in vec2 pixcoord;
in vec4 offset[3];

precision highp float;

layout (binding=0) uniform sampler2D edgesTex;
layout (binding=1) uniform sampler2D areaTex;
layout (binding=2) uniform sampler2D searchTex;

out vec4 ex_FragColor;

void main()
{
	ex_FragColor = SMAABlendingWeightCalculationPS(texcoord, pixcoord, offset, edgesTex, areaTex, searchTex, ivec4(0));
}