@VS
#version 420 core

precision highp float;

in vec2 in_Position;

uniform mat4 invProjection;

out block
{
	vec3 PositionVS;
	vec2 TexCoord;
}OUT;

void main()
{
	gl_Position = vec4(in_Position, 0.0, 1.0);
	
	OUT.PositionVS = (invProjection * vec4(in_Position, 0, 1)).xyz;
	
	OUT.TexCoord = 0.5 * gl_Position.xy + 0.5;
}

@PS
#version 420 core

precision highp float;

#include common

layout (binding=0) uniform sampler2D depthTexture;
layout (binding=1) uniform sampler2D albedoTexture;
layout (binding=2) uniform sampler2D lightmapTexture;

in block
{
	vec3 PositionVS;
	vec2 TexCoord;
}IN;

uniform vec3 ambientColor;

uniform mat4 invProjection;

uniform int doSSAO;
uniform int SSAOStep;

out vec4 ex_FragColor;


vec3 getPositionSS(vec2 uv)
{
	float depth = texture(depthTexture, uv).r;
	
    vec4 Position = vec4(uv * 2 - 1, depth, 1.0);
     
    Position  = invProjection * Position;
    Position /= Position.w;
     
    return Position.xyz;
}
 
float depthCompare(float depth1, float depth2, inout bool extrapolate) 
{  
	float strength = 225;
	float depthDifference = (depth1 - depth2) * strength;
	float mean = 0.1;
	float variance = 0.5;
	
	if (depthDifference < mean)
		variance = mean;
	else
		extrapolate = true;
	
    return pow(G_E, -2 * (depthDifference - mean) * (depthDifference - mean) / (variance * variance));
}  

float doOcclusion(float depth, float dw, float dh, vec2 uv)
{	
	float dwd = dw / depth;
	float dhd = dh / depth;
	
	vec2 coord = vec2(uv.x + dwd, uv.y + dhd);
	
	bool extrapolate = false;
	float occlusion = depthCompare(depth, getPositionSS(coord).z, extrapolate);
	
	if (extrapolate)
	{
		vec2 coord2 = vec2(uv.x - dwd, uv.y - dhd);
		
		float extrapolatedTest = depthCompare(getPositionSS(coord2).z, depth, extrapolate);
		occlusion += (1.0 - occlusion) * extrapolatedTest;
	}	
	return occlusion;  
} 
   
void main()
{
	vec2 uv = IN.TexCoord;
	
	float ao = 1.0;
	if (doSSAO == 1)
	{
		ivec2 screenSize = textureSize(albedoTexture, 0);
		
		vec2 random = rand(uv);
		float depth = getPositionSS(uv).z;
		ao = 0.0f;
		
		float pw = 1.0 / screenSize.x * 0.5;
		float ph = 1.0 / screenSize.y * 0.5;
		
		int numSamples = 4;
		for (int i=0; i<numSamples; i++)
		{
			ao += doOcclusion(depth,  pw, ph, uv);
			ao += doOcclusion(depth,  pw, -ph, uv);
			ao += doOcclusion(depth,  -pw, ph, uv);
			ao += doOcclusion(depth,  -pw, -ph, uv);

			float pw2 = pw * 1.2;
			float ph2 = ph * 1.2;
			
			ao += doOcclusion(depth,  pw2, 0, uv);
			ao += doOcclusion(depth,  -pw2, 0, uv);
			ao += doOcclusion(depth,  0, ph2, uv);
			ao += doOcclusion(depth,  0, -ph2, uv);

			// randomise the samples and expand them
			pw += random.x * 0.05;
			pw *= 1.5;
			
			ph += random.y * 0.05;
			ph *= 1.5;
		}
		ao = 1.0 + ( ambientColor.r - ao / (8 * numSamples) );
	}
	
	vec4 albedoColor = texture(albedoTexture, IN.TexCoord);
	vec4 litColor = texture(lightmapTexture, IN.TexCoord);
	
	vec3 finalColor;

	finalColor = ambientColor * ao * albedoColor.rgb + albedoColor.rgb * litColor.rgb + litColor.a;
	if (doSSAO == 1 && SSAOStep == 1)
	{
		finalColor = vec3(ao);
	}
	
	ex_FragColor = vec4(finalColor,1.0);
}
