@VS
#version 420 core

precision highp float;

in vec2 in_Position;

out vec2 ex_TexCoord;

void main()
{
	gl_Position = vec4(in_Position, 0.0, 1.0);

	ex_TexCoord = in_Position * 0.5 + 0.5;
}

@PS
#version 420 core

precision highp float;

layout (binding=0) uniform sampler2D tex;

in vec2 ex_TexCoord;

out vec4 ex_FragColor;

void main()
{
	ex_FragColor = texture(tex, ex_TexCoord.xy);
}